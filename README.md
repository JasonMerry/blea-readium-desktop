# Blackwell's Readium Desktop

## Prerequisites

1. https://brew.sh/ - Homebrew (check if you have it already with `brew`)
2. https://nodejs.org NodeJS >= 6, NPM >= 3 (check with `node --version` and `npm --version` from the command line)
    * With `brew` installed you can install node `brew install node`
    * You may also need `nvm` if you are working on projects that require a different version of npm/node [install nvm](https://github.com/creationix/nvm/blob/master/README.markdown)
    * You do not have to use `brew` for installing node but you do need it for `yarn` but it helps to do both this way
3. https://yarnpkg.com Yarn >= 0.21 (check with `yarn --version` from the command line)
    * `brew install yarn`
4. Swat up on the *Technologies* list ;)

## Technologies

* typescript
* electron
* reactjs
* redux
* i18next
* ~Material-UI~ Materialize-css - can choose your own markup; no inline styling; uses jquery (not necessarily a good thing)
* sass for css


## Getting started and ready to build/play
Other than being in your project directory, you do not have to follow these steps first. You can skip straight to Cleaning if you like as that single command does it all :)

Command line steps:

1. `cd blea-readium-desktop`
2. `npm install` (initialize local `node_modules` packages from dependencies declared in `package.json`)
3. `yarn upgrade` (sync local packages)
4. `yarn run lint` (code linting : not always needed but good to know or do when you update your code)
5. time to play :)


## Running the app

### First run
Before being able to use any of this you need to have built Readium from a git repo. To do this you need to
```
yarn run readium-js
```
This is the last of the many scripts that run when doing a fresh clean install. Nothing stopping you running yourself if you want/need to.

### Cleaning

To give you a completely clean repo everytime you pull it's best to remove node_modules, the dist folder and clean up the npm cache. It's recommended to do this before anything else:

```
yarn run clean
```

### Development

```
yarn run start:dev
```

This environment provides a hot loader. So if you made changes in your code, electron will automatically reload your app.
Linting runs so you can guarantee your code is following our standards.

The css file is generated as it would if the app was distributed. If you are going to be developing with css and need it watched for changes then run the watcher:

```
yarn run start:dev:watch
```

### Production

```
yarn start
```

## Developing the app

### High level React components
One thing that React wants a single surrounding element which is generally a single `<div>` around every component.
As a result the css can get a bit tricky when dealing with higher level components due to us requiring `flex` rules in the css.

If you need a high level component, put this around it `<div className="rs">` (rs = React Surround). The hope is that this will not break the flexible layout.
If it becomes apparent this isn't fully working then another solution needs to be found and this section edited.

### Body ID
Each section has it's own body id to help specify certain aspects of each page. This achieved by adding some JavaScript to the `contstuctor()`:
```
document.body.id = "bodyId";
```

## Building the app

### App logo
The app needs an icon(logo) for each OS we develop for and this is created using the original icon file `src/assets/img/icon.png`.
It is transparent, 1024x1024 and made from the original `src/assets/psd/icon.psd` so you will need someone who has Photoshop or another program that can read/edit and save 'psd' files.

To create the icons just use the script
```
yarn run icons
```
ALl relevant icons will be put into the `build` directory ready for your build.

### Distributions
