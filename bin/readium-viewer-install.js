// DEP - Check if a directory exists, and create it if it doesn't
var fs = require('fs');

// DEP - Output option to the user to determine what to do with the build
var stdio = require('stdio');

// DEP - to copy files
var cpx = require('cpx');

// Varrying Readium directories
var readiumDir = "./readium-js-viewer";
var readiumArchiveDir = "./src/webview/archive-readium-js-viewer";
var bleadiumDir = "./src/webview/blea-readium-js-viewer";
var templateDir = "/src/templates";
var pluginDir = "/readium-js/readium-shared-js/plugins";

var readiumTemplates = readiumDir + templateDir;
var readiumArchiveTemplates = readiumArchiveDir + templateDir;
var readiumPlugins = readiumDir + pluginDir;
var readiumArchivePlugins = readiumArchiveDir + pluginDir;
var bleadiumTemplates = bleadiumDir + templateDir;

// Set up the ability to write out cmd line scripts from JS statements
var child_process = require('child_process');
// example:
// child_process.execSync("yarn run clean",{stdio:[0,1,2]});

// Child process for bash output
function bashScript(script) {
    child_process.execSync(script,{stdio:[0,1,2]});
};

// Git - Checkout of readium repository
function gitCheckout() {
    console.log("- - - - - - - - - - - - - - Master and Submodules checking out");
    bashScript("cd " + readiumDir + " && git checkout master && git submodule foreach --recursive \"git checkout master\"");
    console.log("- - - - - - - - - - -  DONE Master and Submodules checking out");
};

// Git - submodule update
function gitSubmodule() {
    return new Promise(function(resolve) {
        console.log("- - - - - - - - - - - - - - Submodule update");
        bashScript("cd " + readiumDir + " && pwd && git submodule update --init --recursive && git checkout master && git submodule foreach --recursive \"git checkout master\"");
        console.log("- - - - - - - - - - -  DONE Submodule update");
        resolve();
    })
};

// Archive original Readium templates/css/other files
function archiveReadium() {
    console.log("- - - - - - - - - - - - - - Copying Readium files to archive");
    // templates for viewer
    bashScript("cpx '" + readiumTemplates + "/*.html' " + readiumArchiveTemplates);
    console.log("- - - - - - - - - - -  DONE Copying Readium files to archive");
};

// Restore original Readium files
function restoreReadium() {
    // templates for viewer
    console.log("- - - - - - - - - - - - - - Restore archive files back to Readium");
    bashScript("cpx '" + readiumArchiveTemplates + "/*' " + readiumTemplates);
    // file for plugins
    bashScript("cpx '" + readiumArchivePlugins + "/plugins-override.cson' " + readiumPlugins);
    console.log("- - - - - - - - - - -  DONE Restore archive files back to Readium");
}

// Replace Readium
function replaceReadium() {
    console.log("- - - - - - - - - - - - - - Replacing our files to Readium");
    // "bleadium:replace": "cpx './src/webview/blea-readium-js-viewer/templates/*' ./readium-js-viewer/src/templates",
    bashScript("cpx '" + bleadiumTemplates + "/*' " + readiumTemplates);
    console.log("- - - - - - - - - - -  DONE Replacing our files to Readium");
}

// Prepare Readium
function prepareReadium() {
    console.log("- - - - - - - - - - - - - - Preparing/installing Readium");
    bashScript("cd " + readiumDir + " && pwd && yarn run prepare:yarn:all");
    console.log("- - - - - - - - - - -  DONE Preparing/installing Readium");
};

function buildReadium() {
    console.log("- - - - - - - - - - - - - - Building Readium");
    bashScript("yarn run bleadium");
    console.log("- - - - - - - - - - -  DONE Building Readium");
};

// Checking our readiumDir exists locally with a promise
function checkDir(dir) {
    return new Promise(function(resolve, reject) {
        if(fs.existsSync(dir)) {
            resolve();
        } else {
            reject();
        };
    });
};

// The function to run it all
function installReadium(readiumDir) {
    return new Promise(function(resolve) {
        // check the directory exists
        checkDir(readiumDir).then(function () {
            console.log("- - - - - - - - - - - - - - Directory '" + readiumDir + "' already exists going to check git status");
        }).catch(function() {
            console.log("- - - - - - - - - - - - - - Making directory:'", readiumDir, "'");
            bashScript("mkdir " + readiumDir);
            console.log("- - - - - - - - - - -  DONE Making directory");
            console.log("- - - - - - - - - - - - - - Cloning Readium");
            bashScript("git clone --recursive -b master https://github.com/readium/readium-js-viewer.git " + readiumDir);
            console.log("- - - - - - - - - - -  DONE Cloning Readium");
            gitSubmodule()
            .then(gitCheckout())
            .then(archiveReadium())
            .then(prepareReadium());
        }).then(function() {
        // copy back original files before checking repo
            restoreReadium();
        // git status for changes to html files
            bashScript("cd " + readiumDir + " && pwd && git status");
            stdio.question('??? - - - - - - - - - - - - Do you need to pull the repo based on the status?',['y','n'], function (err, pull) {
                if(pull == 'y') {
                // updating repo
                    console.log("- - - - - - - - - - - - - - Updating repo");
                    bashScript("cd " + readiumDir + " && pwd && git clean -xfdf && git submodule foreach --recursive git clean -xfdf && git reset --hard && git submodule foreach --recursive git reset --hard");
                    console.log("- - - - - - - - - - -  DONE Updating repo");
                    gitSubmodule();
                    archiveReadium();
                // if files have changed, get user to choose to stop or continue npm
                    stdio.question('??? - - - - - - - - - - - Do you need to make local changes because of the updated files?',['y','n'], function (err, local) {
                        if(local == 'y') {
                            console.log("- - - - - - - - - - - - - - npm will now exit with error code 1 so you can make the necessary changes before running this again");
                            process.exit(1);
                        } else {
                            resolve();
                        };
                    });
                } else {
                    resolve();
                };
            });
        });
    });
};

module.exports.installReadium = function() {
    installReadium(readiumDir).then(function() {
        prepareReadium();
        buildReadium();
    });
};

module.exports.replaceReadium = function () {
    replaceReadium();
};
