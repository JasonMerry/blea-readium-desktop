import test from "ava";
import {actionTest} from "redux-ava";

import {
    fetchBookshelf
} from "blea/actions/bookshelf";

import {
    changeForm,
    clearError,
    loginRequest,
    logoutRequest,
    registerDevice,
    requestError,
    sendingRequest,
    setAuthState,
} from "blea/actions/auth";

let formState = {
    password: "password",
    username: "stephen",
};

let errorStatus = 200;
let errorMessage = "Some error message";

// bookshelf action creators
test(
    "fetchBookshelf action",
    actionTest(
        fetchBookshelf, null, {type: "FETCH_BOOKSHELF"}
    )
);

// auth action creators
test(
    "changeForm action",
    actionTest(
        changeForm, formState, {type: "CHANGE_FORM", newFormState: formState}
    )
);

test(
    "setAuthState action",
    actionTest(
        setAuthState, {loggedIn: true}, {type: "SET_AUTH", newAuthState: {loggedIn: true}}
    )
);

test(
    "sendingRequest action",
    actionTest(
        sendingRequest, true, {type: "SENDING_REQUEST", sending: true}
    )
);

test(
    "loginRequest action",
    actionTest(
        loginRequest, formState, {type: "LOGIN_REQUEST", data: formState}
    )
);

test(
    "logout action",
    actionTest(
        logoutRequest, null, {type: "LOGOUT"}
    )
);

test(
    "request error action",
    actionTest(
        requestError, errorStatus, errorMessage, {type: "REQUEST_ERROR", errorStatus, errorMessage}
    )
);

test(
    "clear error action",
    actionTest(
        clearError, null, {type: "CLEAR_ERROR"}
    )
);

test(
    "registerDevice error",
    actionTest(
        registerDevice, null, {type: "REGISTER_DEVICE"}
    )
);
