import { Action } from "redux";

import { AuthLogin, UserAuthState } from "blea/entities/auth";

export const CHANGE_FORM = "CHANGE_FORM";
export const SET_AUTH = "SET_AUTH";
export const SENDING_REQUEST = "SENDING_REQUEST";
export const LOGIN_REQUEST = "LOGIN_REQUEST";
export const LOGOUT = "LOGOUT";
export const REQUEST_ERROR = "REQUEST_ERROR";
export const CLEAR_ERROR = "CLEAR_ERROR";
export const REGISTER_DEVICE = "REGISTER_DEVICE";
export const LOGIN_REQUEST_DEBUG = "LOGIN_REQUEST_DEBUG";

export interface IChangeFormAction extends Action {
    newFormState: AuthLogin;
}

export interface ILoginRequestAction extends Action {
    data: AuthLogin;
}

export interface ISendingRequestAction extends Action {
    sending: boolean;
}

export interface ISetAuthAction extends Action {
    newAuthState: UserAuthState;
}

export interface IRequestErrorAction extends Action {
    errorMessage: string;
    errorStatus: number;
}

export interface IRegisterDeviceAction extends Action {

}

// Sets the form state
// @param  {object} newFormState          The new state of the form
// @param  {string} newFormState.username The new text of the username input field of the form
// @param  {string} newFormState.password The new text of the password input field of the form
export function changeForm (newFormState: AuthLogin): IChangeFormAction  {
    return {type: CHANGE_FORM, newFormState};
}

// Sets the authentication state of the application
// @param  {boolean} newAuthState True means a user is logged in, false means no user is logged in
export function setAuthState (newAuthState: UserAuthState): ISetAuthAction {
    return {type: SET_AUTH, newAuthState};
}

// Sets the `currentlySending` state, which displays a loading indicator during requests
// @param  {boolean} sending True means we're sending a request, false means we're not
export function sendingRequest (sending: boolean): ISendingRequestAction {
    return {type: SENDING_REQUEST, sending};
}

// Tells the app we want to log in a user
// @param  {object} data          The data we're sending for log in
// @param  {string} data.username The username of the user to log in
// @param  {string} data.password The password of the user to log in
export function loginRequest (data: AuthLogin): ILoginRequestAction {
    return {type: LOGIN_REQUEST, data};
}

export function loginRequestDebug(): Action {
    return {
        type: LOGIN_REQUEST_DEBUG,
    };
}

// Tells the app we want to log out a user
export function logoutRequest (): Action {
    return {type: LOGOUT};
}

// Sets the `error` state to the error received
// @param  {number} errorStatus The status number we got when trying to make the request
// @param  {string} errorMessage The message text we get from the server when trying to make the request
export function requestError (errorStatus: number, errorMessage: string): IRequestErrorAction {
    return {type: REQUEST_ERROR, errorStatus, errorMessage};
}

// Sets the `error` state as empty
export function clearError (): Action {
    return {type: CLEAR_ERROR};
}

// Tells the app we want to register a device
export function registerDevice (): IRegisterDeviceAction {
    return {type: REGISTER_DEVICE };
}
