import { Action } from "redux";

import { Book } from "blea/entities/book";

import { Download } from "blea/entities/download";

export const BOOK_DOWNLOAD_ADD = "BOOK_DOWNLOAD_ADD";
export const BOOK_DOWNLOAD_START = "BOOK_DOWNLOAD_START";
export const BOOK_DOWNLOAD_PROGRESS = "BOOK_DOWNLOAD_PROGRESS";
export const BOOK_DOWNLOAD_FINISH = "BOOK_DOWNLOAD_FINISH";
export const BOOK_DOWNLOAD_CANCEL = "BOOK_DOWNLOAD_CANCEL";
export const BOOK_DOWNLOAD_INDEX = "BOOK_DOWNLOAD_INDEX";

export interface BookDownloadAction extends Action {
    book: Book;
    downloads?: Download[];
    progress?: number;
    contentType?: string;
}

export function add(book: Book): BookDownloadAction {
    return {
        type: BOOK_DOWNLOAD_ADD,
        book,
    };
}

export function start(book: Book, downloads: Download[]): BookDownloadAction {
    return {
        type: BOOK_DOWNLOAD_START,
        book,
        downloads,
    };
}

export function progress(book: Book, progress: number): BookDownloadAction {
    return {
        type: BOOK_DOWNLOAD_PROGRESS,
        book,
        progress,
    };
}

export function finish(book: Book, contentType: string): BookDownloadAction {
    return {
        type: BOOK_DOWNLOAD_FINISH,
        book,
        contentType
    };
}

export function cancel(book: Book): BookDownloadAction {
    return {
        type: BOOK_DOWNLOAD_CANCEL,
        book,
    };
}

export function index(book: Book): BookDownloadAction {
    return {
        type: BOOK_DOWNLOAD_INDEX,
        book,
    };
}
