import { Action } from "redux";

import { Book, Bookmark } from "blea/entities/book";

export const BOOK_UPDATE = "BOOK_UPDATE";
export const BOOK_LAST_PAGE = "BOOK_LAST_PAGE";
export const BOOK_LAST_WORD_ID = "BOOK_LAST_WORD_ID";
export const BOOK_ADD_HIGHLIGHT = "BOOK_ADD_HIGHLIGHT";
export const BOOK_DELETE_HIGHLIGHT = "BOOK_DELETE_HIGHLIGHT";
export const BOOK_GET_HIGHLIGHTS = "BOOK_GET_HIGHLIGHTS";
export const BOOK_SET_HIGHLIGHTS = "BOOK_SET_HIGHLIGHTS";
export const BOOK_ADD_BOOKMARK = "BOOK_ADD_BOOKMARK";
export const BOOK_DELETE_BOOKMARK = "BOOK_DELETE_BOOKMARK";
export const BOOK_UPDATE_BOOKMARKS = "BOOK_UPDATE_BOOKMARKS";
export const BOOK_BOOKMARKS_GET = "BOOK_BOOKMARKS_GET";
export const BOOK_SET_CURRENT_PAGE = "BOOK_SET_CURRENT_PAGE";
export const BOOK_SET_NUMBER_PAGES = "BOOK_SET_NUMBER_PAGES";
export const BOOK_SEARCH_REQUEST = "BOOK_SEARCH_REQUEST";
export const BOOK_SEARCH_UPDATE = "BOOK_SEARCH_UPDATE";

export interface BookAction extends Action {
    book: Book;
    wordId?: number;
    spineBrickMap?: any[];
    highlight?: any;
    highlights?: any[];
    bookmark?: Bookmark;
    bookmarks?: Bookmark[];
    bookmarkId?: number;
    highlightId?: number;
    page?: number;
    pdfpages?: number;
}

export interface BookSearchAction extends Action {
    bookshelfPk: number;
    query?: string;
    result?: any;
}

export function lastPage(book: Book): BookAction {
    return {
        type: BOOK_LAST_PAGE,
        book,
    };
}

export function setCurrentPage(book: Book, page: number): BookAction {
    return {
        type: BOOK_SET_CURRENT_PAGE,
        book,
        page,
    };
}

export function setNumberPages(book: Book, pdfpages: number): BookAction {
    return {
        type: BOOK_SET_NUMBER_PAGES,
        book,
        pdfpages,
    };
}

export function setLastWordId(book: Book, wordId: number): BookAction {
    return {
        type: BOOK_LAST_WORD_ID,
        book,
        wordId,
    };
}

export function getBookHighlights(book: Book): BookAction {
    return {
        type: BOOK_GET_HIGHLIGHTS,
        book,
    };
}

export function setBookHighlights(book: Book, highlights: any): BookAction {
    return {
        type: BOOK_SET_HIGHLIGHTS,
        book,
        highlights,
    };
}

export function addBookHighlight(book: Book, highlight: any): BookAction {
    return {
        type: BOOK_ADD_HIGHLIGHT,
        book,
        highlight,
    };
}

export function deleteBookHighlight(book: Book, highlightId: number): BookAction {
    return {
        type: BOOK_DELETE_HIGHLIGHT,
        book,
        highlightId,
    };
}

export function addBookmark(book: Book, bookmark: Bookmark): BookAction {
    return {
        type: BOOK_ADD_BOOKMARK,
        book,
        bookmark,
    };
}

export function deleteBookmark(book: Book, bookmarkId: number): BookAction {
    return {
        type: BOOK_DELETE_BOOKMARK,
        book,
        bookmarkId,
    };
}

export function updateBookmarks(book: Book, bookmarks: Bookmark[]): BookAction {
    return {
        type: BOOK_UPDATE_BOOKMARKS,
        book,
        bookmarks,
    };
}

export function getBookmarks(book: Book): BookAction {
    return {
        type: BOOK_BOOKMARKS_GET,
        book,
    };
}

export function getSearchResult(bookshelfPk: number, query: string): BookSearchAction {
    return {
        type: BOOK_SEARCH_REQUEST,
        bookshelfPk,
        query,
    };
}

export function updateSearchResults(bookshelfPk: number, result: any): BookSearchAction {
    return {
        type: BOOK_SEARCH_UPDATE,
        bookshelfPk,
        result,
    };
}
