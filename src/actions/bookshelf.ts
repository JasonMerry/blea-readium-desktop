import { Action } from "redux";

import { Book, SpineBrickMap } from "blea/entities/book";
import { Bookshelf } from "blea/entities/bookshelf";

import { BookAction } from "blea/actions/book";

export const FETCH_BOOKSHELF = "FETCH_BOOKSHELF";
export const FETCH_BOOKSHELF_SUCCESS = "FETCH_BOOKSHELF_SUCCESS";
export const FETCH_BOOKSHELF_FAILURE = "FETCH_BOOKSHELF_FAILURE";
export const REMOVE_BOOKSHELF = "REMOVE_BOOKSHELF";
export const BOOKSHELF_UPDATED = "BOOKSHELF_UPDATED";
export const SET_BOOKSHELF = "SET_BOOKSHELF";
export const UPDATED_BOOKSHELF = "UPDATED_BOOKSHELF";
export const SYNC_BOOKSHELF = "SYNC_BOOKSHELF";

export const DOWNLOAD_CANCEL = "DOWNLOAD_CANCEL";
export const DOWNLOAD_FAIL = "DOWNLOAD_FAIL";
export const DOWNLOAD_REMOVE = "DOWNLOAD_REMOVE";
export const DOWNLOAD_PROGRSS = "DOWNLOAD_PROGRESS";

// Book action types
export const BOOK_ADD = "BOOK_ADD";
export const BOOK_UPDATE = "BOOK_UPDATE";
export const BOOK_REMOVE = "BOOK_REMOVE";
export const BOOK_SET_SPINE_MAP = "BOOK_SET_SPINE_MAP";

export interface BookshelfAction extends Action {
    bookshelf?: Bookshelf;
}

// action creators
export function fetchBookshelf(): Action {
    return {
        type: FETCH_BOOKSHELF,
    };
}

export function syncBookshelf(): Action {
    return {
        type: SYNC_BOOKSHELF,
    };
}

export function removeBookshelf(): Action {
    return {
        type: REMOVE_BOOKSHELF,
    };
}

export function updatedBookshelf(): Action {
    return {
        type: UPDATED_BOOKSHELF,
    };
}

export function setBookshelf(bookshelf: Bookshelf): BookshelfAction {
    return {
        type: SET_BOOKSHELF,
        bookshelf
    };
}

export function updateBook(book: Book): BookAction {
    return {
        type: BOOK_UPDATE,
        book,
    };
}

export function updateBookSpineMap(book: Book, spineBrickMap: SpineBrickMap[]): BookAction {
    return {
        type: BOOK_SET_SPINE_MAP,
        book,
        spineBrickMap,
    };
}
