import { Action } from "redux";

export const SET_LAYOUT =  "SET_LAYOUT";

export interface ILayoutAction extends Action {
    layout: string;
}

// action creator
export function setLayout(layout: boolean): ILayoutAction {
    return {
        layout: layout ? "grid" : "list",
        type: SET_LAYOUT,
    };
}
