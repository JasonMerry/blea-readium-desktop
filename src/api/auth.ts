
export interface IAuthApi {
    // loggedIn(token: string): any;

    login(username: string, password: string): any;

    logout(token: string): any;

    registerDevice(token: string, name: string): any;
}
