import { injectable } from "inversify";

import { getServerMain } from "blea/resources/server";

import { deviceId } from "blea/resources/device";
import { IAuthApi } from "./auth";

@injectable()
export class BlackwellAuthApi implements IAuthApi {

    public logout(token: string) {
        return getServerMain().axiosInstance.delete(
            "/api/account/logout/", {headers: {Authorization: "Token " + token}}
        )
            .then((response) => {
                return Promise.resolve({loggedIn: false});
            });
    }

    // Logs a user in, returning a promise with `true` when done
    // @param  {string} username The username of the user
    // @param  {string} password The password of the user
    public login(username: string, password: string) {

        return getServerMain().axiosInstance.post("/api/account/login/", {username, password})
            .then((response) => {
                return Promise.resolve({loggedIn: true, token: response.data.token});
            });
    }

    public registerDevice(token: string, name: string) {
        return getServerMain().axiosInstance.post(
            "/api/device/register/",
            {name, device_id: deviceId},
            {headers: {Authorization: "Token " + token}}
        )
            .then((response) => {
                return Promise.resolve({loggedIn: true, key: response.data.key, user: response.data.device.user});
            });
    }

}
