import { DownloadParams } from "blea/entities/download";

import { Book } from "blea/entities/book";

import { ApiItem } from "blea/entities/offline";

// gets rid of Reflect.hasOwnMetadata is not a function error
import "reflect-metadata";

export interface BookshelfApi {
    fetchBookshelf(token: string): any;
    getDownloadBookParams(token: string, download: Book): DownloadParams;
    getLastPage(bookshelfPk: number, token: string): any;
    getBookHighlights(bookshelfPk: number, token: string): any;
    addBookHighlight(bookshelfPk: number, highlight: any, token: string);
    deleteHighlight(highlightId: number, token: string);
    getApiCallDeleteHighlight(highlightId: number): any;
    getApiCallAddBookHighlight(bookshelfPk: number, highlight: any);
    addBookmark(bookshelfPk: number, wordId: number, pageNumber: number, token: string): any;
    getApiCallAddBookmark(bookshelfPk: number, wordId: number, pageNumber: number): ApiItem;
    deleteBookmark(bookmarkId: number, token: string): any;
    getApiCallDeleteBookmark(bookmarkId: number): ApiItem;
    getBookmarks(bookshelfPk: number, token: string): any;
    genericApiCall(apiItem: ApiItem, token: string): any;
    checkServerAvailable(): any;
    searchBook(bookshelfPk: number, query: string, token: string): any;
}
