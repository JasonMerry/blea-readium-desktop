import { injectable } from "inversify";

import { deviceId } from "blea/resources/device";
import { getServerMain } from "blea/resources/server";

import { BookshelfApi } from "blea/api/bookshelf";

import { DownloadParams } from "blea/entities/download";

import { Book } from "blea/entities/book";
import { ApiItem } from "blea/entities/offline";

@injectable()
export class BlackwellBookshelfApi implements BookshelfApi {

    private static _getApiCallAddBookHighlight(bookshelfPk: number, highlight: any) {
        return {
            data:
            {
                book_id: bookshelfPk,
                file: highlight.file,
                id: highlight.id,
                note: highlight.note,
                owned: true,
                page: highlight.page,
                selection: highlight.selection,
                shareAll: false,
                shareWith: false,
            },
            method: highlight.id ? "put" : "post",
            url: highlight.id ? `/api/mark/${highlight.id}/` : "/api/mark/"
        };

    }

    private static _getApiCallAddBookmark(bookshelfPk: number, wordId: number, pageNumber: number): ApiItem {
        return {
            data: {
                book_id: bookshelfPk,
                page: pageNumber,
                word_id: wordId,
            },
            method: "post",
            url: "/api/bookmark/",
        };
    }

    private static _getApiCallDeleteBookmark(bookmarkId: number): ApiItem {
        return {
            data: {},
            method: "delete",
            url: `/api/bookmark/${bookmarkId}/`,
        };
    }

    private static _getApiCallDeleteHighlight(highlightId: number): ApiItem {
        return {
            data: {},
            method: "delete",
            url: `/api/mark/${highlightId}/`,
        };
    }

    private static _genericApiCall(apiItem: ApiItem, token: string): any {
        return getServerMain().axiosInstance(
            {
                data: apiItem.data,
                headers: { Authorization: "Token " + token, },
                method: apiItem.method,
                params: apiItem.params,
                url: apiItem.url,
            },
        )
            .then((response) => {
                return Promise.resolve(response.data ? response.data : response.status);
            })
            .catch((error) => {
                return Promise.reject(error);
            });
    }

    public fetchBookshelf (token: string) {
        return getServerMain().axiosInstance.get("/api/bookshelf/", {headers: {Authorization: "Token " + token}})
            .then((response) => {
                return Promise.resolve(response.data[0]);
            })
            .catch((error) => {
                return Promise.reject(error);
            });
    }

    public getDownloadBookParams(token: string, download: Book): DownloadParams {
        return {
            headers: {Authorization: "Token " + token},
            qs: {device_id: deviceId},
            url: getServerMain().SERVER_PATH + "/api/content/" + download.bookshelfPk + "/",
        };
    }

    public getLastPage(bookshelfPk: number, token: string) {
        return getServerMain().axiosInstance.get(`/api/sync/${bookshelfPk}/`, {headers: {Authorization: "Token " + token}})
            .then((response) => {
                return Promise.resolve(response.data);
            })
            .catch((error) => {
                return Promise.reject(error);
            });
    }

    public getBookHighlights(bookshelfPk: number, token: string) {
        return getServerMain().axiosInstance.get(`/api/mark/`, {
            headers: {Authorization: "Token " + token},
            params: {book: bookshelfPk}
        })
            .then((response) => {
                return Promise.resolve(response.data);
            })
            .catch((error) => {
                return Promise.reject(error);
            });
    }

    public addBookHighlight(bookshelfPk: number, highlight: any, token: string) {
        let apiItem = BlackwellBookshelfApi._getApiCallAddBookHighlight(bookshelfPk, highlight);
        return BlackwellBookshelfApi._genericApiCall(apiItem, token);
    }

    public getApiCallAddBookHighlight(bookshelfPk: number, highlight: any) {
        return BlackwellBookshelfApi._getApiCallAddBookHighlight(bookshelfPk, highlight);
    }

    public addBookmark(bookshelfPk: number, wordId: number, pageNumber: number, token: string): any {
        let apiItem = BlackwellBookshelfApi._getApiCallAddBookmark(bookshelfPk, wordId, pageNumber);
        return BlackwellBookshelfApi._genericApiCall(apiItem, token);
    }

    // return a ApiItem for insertion into the Offline Queue
    public getApiCallAddBookmark(bookshelfPk: number, wordId: number, pageNumber: number): ApiItem {
        return BlackwellBookshelfApi._getApiCallAddBookmark(bookshelfPk, wordId, pageNumber);
    }

    public deleteBookmark(bookmarkId: number, token: string): any {
        let apiItem = BlackwellBookshelfApi._getApiCallDeleteBookmark(bookmarkId);
        return BlackwellBookshelfApi._genericApiCall(apiItem, token);
    }

    public getApiCallDeleteBookmark(bookmarkId: number): ApiItem {
        return BlackwellBookshelfApi._getApiCallDeleteBookmark(bookmarkId);
    }

    public deleteHighlight(highlightId: number, token: string): any {
        let apiItem = BlackwellBookshelfApi._getApiCallDeleteHighlight(highlightId);
        return BlackwellBookshelfApi._genericApiCall(apiItem, token);
    }

    public getApiCallDeleteHighlight(highlightId: number): ApiItem {
        return BlackwellBookshelfApi._getApiCallDeleteHighlight(highlightId);
    }

    public getBookmarks(bookshelfPk: number, token: string): any {
        return getServerMain().axiosInstance.get(
            `/api/bookmark/`,
            {
                headers: { Authorization: "Token " + token, },
                params: {book: bookshelfPk},
            },
        )
            .then((response) => {
                return Promise.resolve(response.data);
            })
            .catch((error) => {
                return Promise.reject(error);
            });
    }

    public genericApiCall(apiItem: ApiItem, token: string): any {
        return BlackwellBookshelfApi._genericApiCall(apiItem, token);
    }

    public checkServerAvailable() {
        return getServerMain().axiosInstance.get(`/check-instance/`)
            .then((response) => {
                return Promise.resolve(response.status);
            })
            .catch((error) => {
                return Promise.reject(error);
            });
    }

    public searchBook(bookshelfPk: number, query: string, token: string) {
        return getServerMain().axiosInstance.get(
            `/api/search/${bookshelfPk}/`,
            {
                headers: { Authorization: "Token " + token, },
                params: {q: query, v: "2"},
            },
        )
            .then((response) => {
                return Promise.resolve(response.data);
            })
            .catch((error) => {
                return Promise.reject(error);
            });
    }
}
