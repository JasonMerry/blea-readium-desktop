import test from "ava";

import { BookshelfApi } from "blea/api/bookshelf";
import { container } from "blea/main/di";
import TYPES from "blea/types";

test(
    "get bookshelf instance", (t) => {
        const bookshelfInst = container.get<BookshelfApi>(TYPES.BookshelfApi);
        t.truthy(bookshelfInst);
    }
);
