import * as React from "react";

import { Link, Route } from "react-router-dom";

import { Store } from "redux";

import { ConnectedRouter } from "react-router-redux";

import { lazyInject } from "blea/renderer/di";
import { IAppState } from "blea/renderer/reducers";
import { history } from "blea/renderer/store/memory";

import { setLocale } from "blea/actions/i18n";
import { Translator } from "blea/i18n/translator";

import BookView from "blea/components/BookView";
import Library from "blea/components/Library";
import Login from "blea/components/Login";

const defaultLocale = "en";

export default class App extends React.Component<undefined, App> {

    @lazyInject("store")
    private store: Store<IAppState>;

    @lazyInject(Translator)
    private translator: Translator;

    constructor() {
        super();
        let locale = this.store.getState().i18n.locale;

        if (locale == null) {
            this.store.dispatch(setLocale(defaultLocale));
        }
        this.translator.setLocale(locale);
    }

    public componentDidMount() {
        this.store.subscribe(() => {
            this.translator.setLocale(this.store.getState().i18n.locale);
        });
    }

    public render(): React.ReactElement<{}> {
        return (
            <ConnectedRouter store={this.store} history={history}>
                <div className="rs">
                    <Route exact path="/" component={Login} />
                    <Route path="/library" component={Library} />
                    <Route path="/bookview/:bookshelfPk" component={BookView} />
                    <div className="bw_dev card-panel">
                        <h6>BW DEV block</h6>
                        <ul>
                            <li><Link to="/">Login/logout</Link></li>
                            <li><Link to="/library">Bookshelf</Link></li>
                        </ul>
                        <small>
                            <em>
                            We are using:<br/>
                            node <script>document.write(process.versions.node)</script><br/>
                            Chromium <script>document.write(process.versions.chrome)</script><br/>
                            Electron <script>document.write(process.versions.electron)</script>
                            </em>
                        </small>
                    </div>
                </div>
            </ConnectedRouter>
        );
    }
}
