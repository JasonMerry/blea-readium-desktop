import * as React from "react";
import { Store } from "redux";

import { clearError, logoutRequest } from "blea/actions/auth";

import { setLocale } from "blea/actions/i18n";
import { lazyInject } from "blea/renderer/di";
import { IAppState } from "blea/renderer/reducers";

import * as $ from "materialize-css/node_modules/jquery/dist/jquery.js";

interface AppToolbarState {
    locale: string;
}

export default class AppToolbar extends React.Component<undefined, AppToolbarState> {
    public state: AppToolbarState;
    private unSubscriber;

    @lazyInject("store")
    private store: Store<IAppState>;

    // @lazyInject(Translator)
    // private translator: Translator;

    constructor() {
        super();
        this.state = {
            locale: this.store.getState().i18n.locale,
        };
        this.handleLocaleChange = this.handleLocaleChange.bind(this);
        this.handleLogoutClick = this.handleLogoutClick.bind(this);
    }

    public componentDidMount() {
        this.unSubscriber = this.store.subscribe(() => {
            this.setState({
                locale: this.store.getState().i18n.locale,
            });
        });
        $(".dropdown-button").dropdown({ hover: false });
        console.log("State: App Toolbar", this.store.getState());
    }

    public componentWillUnmount() {
        this.unSubscriber();
    }

    public render(): React.ReactElement<{}>  {
        // const __ = this.translator.translate;

        return (

            <div className="rs">
                <nav>
                    <div className="nav-wrapper indigo darken-4">
                        <ul className="left">
                            <li>
                                <a
                                    className="dropdown-button"
                                    data-activates="dropdown1"
                                >
                                    <i className="material-icons">more_vert</i>
                                </a>
                                <ul id="dropdown1" className="dropdown-content">
                                    <li><a>one</a></li>
                                    <li><a>two</a></li>
                                    <li><a>three</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul className="right">
                            <li><a onClick={this.handleLogoutClick} className="btn">Logout</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        );
    }

    private handleLocaleChange(event: any, index: any, locale: string) {
        this.store.dispatch(setLocale(locale));
    }

    private handleLogoutClick() {
        this.store.dispatch(clearError());
        this.store.dispatch(logoutRequest());
    }
}
