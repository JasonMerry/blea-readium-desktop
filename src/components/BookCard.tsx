import * as React from "react";
import { Store } from "redux";

import * as bookDownloadActions from "blea/actions/book-download";

import { BookType } from "blea/entities/book";
import { DownloadStatus } from "blea/entities/download";

import { lazyInject } from "blea/renderer/di";
import { IAppState } from "blea/renderer/reducers";
import { Link } from "react-router-dom";

import BookInsights from "blea/components/BookInsights";

interface IBookProps {
    bookshelfPk: number;
    title: string;
    author: string;
    coverUrl: string;
    downloading: DownloadStatus;
    progress: number;
    isbn13: string;
    bookType: BookType;
    pageClass1: string;
    pageClass2: string;
    coverClass: string;
    titleClass: string;
    insightsClass: string;
}

export default class BookCard extends React.Component<IBookProps, undefined> {

    @lazyInject("store")
    private store: Store<IAppState>;

    constructor(props) {
        super(props);
        this.handleBookDownloadClick = this.handleBookDownloadClick.bind(this);
    }

    public render(): React.ReactElement<{}>  {

        let downloadIcon;
        let downloadTooltip;
        let downloadClass;

        return (
                <li className={this.props.pageClass1}>
                    <div className={this.props.pageClass2}>
                        <div className="card-image">
                            <Link
                                to={`/bookview/${this.props.bookshelfPk}`}
                            >
                                <img
                                    src={this.props.coverUrl}
                                    alt={this.props.title}
                                    className={this.props.coverClass}
                                />
                                <p className={this.props.titleClass}>
                                    <strong>
                                        {this.props.title}
                                    </strong>
                                    <em>
                                        {this.props.author}
                                    </em>
                                </p>
                                {(this.props.bookType != null ? <strong className="bookType" title="{this.props.bookshelfPk}">{(this.bookTypeToText())}</strong> : "")}
                            </Link>
                            <a
                                className={this.handleDownloadClass({downloadClass})}
                                data-position="top"
                                data-tooltip={this.handleDownloadTooltip({downloadTooltip})}
                                onClick={this.handleBookDownloadClick}
                            >
                                {(this.props.progress ? <i className="downloadProgress">{this.props.progress}%</i> :
                                    <i className="material-icons">
                                        {this.handleDownloadIcon({downloadIcon})}
                                    </i>
                                )}
                            </a>
                        </div>
                        <div className={this.props.insightsClass}>
                            <BookInsights />
                        </div>
                    </div>
                </li>
        );
    }

    private handleBookDownloadClick(event) {
        event.preventDefault();
        // get book
        this.store.dispatch(bookDownloadActions.add({
            bookshelfPk: this.props.bookshelfPk,
            download: {
                progress: 0,
                status: DownloadStatus.Init,
            },
            isbn13: this.props.isbn13,
        }));
    }

    private bookTypeToText(): string {
        switch (this.props.bookType) {
        case BookType.Epub:
            return "Epub";
        case BookType.Pdf:
            return "Pdf";
        default:
            return null;
        }
    }

    private handleDownloadIcon(downloadIcon) {
        switch (this.props.downloading) {
            case 2:
                downloadIcon = "sync_disabled";
                return downloadIcon;
            case 3:
                downloadIcon = "sync_failed";
                return downloadIcon;
            case 4:
                downloadIcon = "done";
                return downloadIcon;
            default:
                downloadIcon = "file_download";
                return downloadIcon;
        }
    }

    private handleDownloadTooltip(downloadTooltip) {
        switch (this.props.downloading) {
            case 1:
                downloadTooltip = "Downloading";
                return downloadTooltip;
            case 2:
                downloadTooltip = "Download disabled";
                return downloadTooltip;
            case 3:
                downloadTooltip = "Download failed";
                return downloadTooltip;
            case 4:
                downloadTooltip = "Downloaded";
                return downloadTooltip;
            default:
                downloadTooltip = "Download";
                return downloadTooltip;
        }
    }

    private handleDownloadClass(downloadClass) {
        downloadClass = "btn-floating halfway-fab waves-effect waves-light tooltipped";
        switch (this.props.downloading) {
            case 4:
                downloadClass += " disabled";
                return downloadClass;
            default:
                return downloadClass;
        }
    }
}
