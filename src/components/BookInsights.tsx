import * as React from "react";
import { Store } from "redux";

import { lazyInject } from "blea/renderer/di";

import { IAppState } from "blea/renderer/reducers";

import { BookStats } from "blea/entities/bookshelf";

interface BookInsightsState {
    insights: BookStats;
}

export default class BookInsights extends React.Component<undefined, BookInsightsState> {
    public state: BookInsightsState;

    private unSubscriber;

    @lazyInject("store")
    private store: Store<IAppState>;

    constructor() {
        super();
        // TODO: need to get insights for a book on the bookshelf
        // this.state = {
        //     insights: this.store.getState().insights
        // };
    }

    public componentDidMount() {
        this.unSubscriber = this.store.subscribe(() => {
            // TODO: need to get insights for a book on the bookshelf
            // this.setState({
            //     insights: this.store.getState().insights
            // });
        });
    }

    public componentWillUnmount() {
        this.unSubscriber();
    }

    public render(): React.ReactElement<{}>  {

        return (
            <ul className="row">
                <li className="col s3">
                    <i
                        className="material-icons tooltipped"
                        data-position="top"
                        data-tooltip="Bookmarks"
                    >
                        bookmark
                        <span className="badge">2341</span>
                    </i>
                </li>
                <li className="col s3">
                    <i
                        className="material-icons tooltipped"
                        data-position="top"
                        data-tooltip="Notes"
                    >
                        note
                        <span className="badge">2</span>
                    </i>
                </li>
                <li className="col s3">
                    <i
                        className="material-icons tooltipped"
                        data-position="top"
                        data-tooltip="Highlights"
                    >
                        highlight
                        <span className="badge">897</span>
                    </i>
                </li>
                <li className="col s3">
                    <i
                        className="material-icons tooltipped"
                        data-position="top"
                        data-tooltip="Shares"
                    >
                        share
                        <span className="badge">23</span>
                    </i>
                </li>
            </ul>
        );
    }
}
