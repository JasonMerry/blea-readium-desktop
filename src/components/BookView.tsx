import * as path from "path";
import * as React from "react";

import { lazyInject } from "blea/renderer/di";
import { IAppState } from "blea/renderer/reducers";
import { Store } from "redux";

import { getAppFolder } from "blea/resources/device";

import * as bookDownloadActions from "blea/actions/book-download";

import * as $ from "materialize-css/node_modules/jquery/dist/jquery.js";

// allow use of webview in react
declare global {
    namespace JSX {
        interface IntrinsicElements {
            webview: any;
        }
    }
}

interface IBookViewProps {
    match: any;
}

import {
    addBookHighlight,
    addBookmark,
    deleteBookHighlight,
    deleteBookmark,
    getBookHighlights,
    getBookmarks,
    lastPage,
    setCurrentPage,
    setNumberPages,
} from "blea/actions/book";

import { Book, BookType } from "blea/entities/book";

import { Bookmarks } from "blea/components/Bookmarks";
import { Highlights } from "blea/components/Highlights";
import { Search } from "blea/components/Search";

import { Pdf } from "blea/components/Pdf";

export default class BookView extends React.Component<IBookViewProps, undefined> {

    public state;

    private reader = null;
    private pdfreader = null;

    private bookPath: string;
    private pdfFile: string;
    private book: Book;
    private unSubscriber;

    @lazyInject("store")
    private store: Store<IAppState>;

    constructor(props) {
        super(props);
        document.body.id = "bookView";
        this.bookPath = path.join(getAppFolder(), "bwl", this.props.match.params.bookshelfPk);
        this.handleTriggerClick = this.handleTriggerClick.bind(this);
        this.handleBookmarkClickEpub = this.handleBookmarkClickEpub.bind(this);
        this.handleBookmarkClickPdf = this.handleBookmarkClickPdf.bind(this);
        this.handleBookmarkDeleteClick = this.handleBookmarkDeleteClick.bind(this);
        this.handleHighlightDeleteClick = this.handleHighlightDeleteClick.bind(this);
        this.handleHighlightNoteEditClick = this.handleHighlightNoteEditClick.bind(this);
        this.pdfOnPageComplete = this.pdfOnPageComplete.bind(this);
        this.pdfOnDocumentComplete = this.pdfOnDocumentComplete.bind(this);
        this.pdfHandlePrevious = this.pdfHandlePrevious.bind(this);
        this.pdfHandleNext = this.pdfHandleNext.bind(this);
        this.pdfHandleAddBookmark = this.pdfHandleAddBookmark.bind(this);
        this.pdfTriggerSelect = this.pdfTriggerSelect.bind(this);
        this.handleSaveNoteClick = this.handleSaveNoteClick.bind(this);
        this.handleSaveNoteSubmit = this.handleSaveNoteSubmit.bind(this);
        this.handleSaveNoteChange = this.handleSaveNoteChange.bind(this);
        this.handleSaveNoteSubmitPdf = this.handleSaveNoteSubmitPdf.bind(this);

        this.state = {
            bookmarks: this.store.getState().bookshelf.books[this.props.match.params.bookshelfPk].bookmarks,
            currentPage: this.store.getState().bookshelf.books[this.props.match.params.bookshelfPk].currentPage,
            errorText: "",
            highlight: undefined,
            highlights: this.store.getState().bookshelf.books[this.props.match.params.bookshelfPk].highlights,
            note: "",
            noteId: undefined,
            pdfpages: this.store.getState().bookshelf.books[this.props.match.params.bookshelfPk].pdfpages,
            searchResult: this.store.getState().bookshelf.books[this.props.match.params.bookshelfPk].result,
        };

        this.book = this.getBookFromState();
        if (!this.book.spineBrickMap && this.book.bookType === BookType.Epub) {
            this.store.dispatch(bookDownloadActions.index(this.book));
        }
        this.store.dispatch(getBookmarks(this.book));
        this.store.dispatch(getBookHighlights(this.book));
    }

    public componentDidMount() {
        $(".modal").modal();

        const localReader = this.reader;
        let highlight;

        this.unSubscriber = this.store.subscribe(() => {
            this.setState({
                bookmarks: this.store.getState().bookshelf.books[this.props.match.params.bookshelfPk].bookmarks,
                currentPage: this.store.getState().bookshelf.books[this.props.match.params.bookshelfPk].currentPage,
                errorText: "",
                highlight: undefined,
                highlights: this.store.getState().bookshelf.books[this.props.match.params.bookshelfPk].highlights,
                note: "",
                noteId: undefined,
                pdfpages: this.store.getState().bookshelf.books[this.props.match.params.bookshelfPk].pdfpages,
                searchResult: this.store.getState().bookshelf.books[this.props.match.params.bookshelfPk].result,
            });
        });

        if (this.reader) {
            this.reader.addEventListener("dom-ready", () => {
                // Show devTools if you want
                localReader.openDevTools();
                localReader.send("load-book", {
                    path: this.bookPath
                });
            });
            this.reader.addEventListener("ipc-message", (event) => {
                const readerAction = event.channel;
                switch (readerAction.type) {
                case "bookmark":
                    // dispatch add Bookmark
                    this.store.dispatch(addBookmark(this.book, {
                        wordId: readerAction.id,
                        words: "Awaiting update.",
                    }));
                    return;
                case "saveSelection":
                    let selection = this.getSelection(readerAction);
                    // dispatch add Highlight
                    if (selection) {
                        this.store.dispatch(
                            addBookHighlight(this.book, {
                                file: this.getSpineFromBrickId(selection.start),
                                selection: {
                                    word_ids: selection.selectionArray
                                }
                            })
                        );
                    } else {
                        this.setState({errorText: "No selected text"});
                        $("#error-modal").modal("open");
                    }
                    return;
                case "checkSelection":
                    highlight = this.getSelection(readerAction);
                    this.setState({highlight});
                    return;
                case "openNoteDialog":
                    highlight = this.getSelection(readerAction);
                    this.setState({highlight});
                    if (highlight) {
                        $("#save-note-modal").modal("open");
                    } else {
                        this.setState({errorText: "No selected text"});
                        $("#error-modal").modal("open");
                    }
                    return;
                case "pageloaded":
                    this.reader.send(
                        "page-nav",
                        {
                            detail: {
                                highlights: this.state.highlights,
                                msg: "showSelection",
                            }
                        }
                    );
                default:
                    return;
                }

            });
        }
    }

    public componentWillUnmount() {
        this.unSubscriber();
    }

    public render(): React.ReactElement<{}>  {
        if (this.book.bookType === BookType.Epub) {
            return this.renderEpubView();
        } else {
            this.pdfFile = path.join(this.bookPath, "content.pdf");
            return this.renderPdfView();
        }
    }

    public componentDidUpdate() {
        $(".collapsible").collapsible();
    }

    private renderEpubView(): React.ReactElement<{}>  {
        return (
            <div className="rs">
                <p>
                    <span id="back" onClick={() => this.handleTriggerClick({ msg: "back" })}>back</span> |&nbsp;
                    <span id="forward" onClick={() => this.handleTriggerClick({ msg: "forward" })}>forward</span> |&nbsp;
                    <span id="getbookmark" onClick={() => this.handleTriggerClick({ msg: "getBookmark" })}>add bookmark</span> |&nbsp;
                    <span id="getLastPage" onClick={() => this.handleTriggerClick({ msg: "getLastPage" })}>get last page</span> |&nbsp;
                    <span id="gotoLastPage" onClick={() => this.handleTriggerClick({ msg: "gotoId" })}>go to last page</span> |&nbsp;
                <span id="showSettings" onClick={() => this.handleTriggerClick({ msg: "showSettings" })}>Show settings</span> |&nbsp;

                <span id="showTableOfContents" onClick={() => this.handleTriggerClick({ msg: "showTableOfContents" })}>Show TOC</span> |&nbsp;

                <span id="saveSelection" onClick={() => this.handleTriggerClick({ msg: "getSelection", rtn: "saveSelection"})}>Save selection</span> |&nbsp;
                <span id="saveNote" onClick={() => this.handleSaveNoteClick()}>Save note</span>

            </p>
                <webview
                    id="reader"
                    src="./readium/index.html"
                    preload="./readium/preload.js"
                    ref={(webview) => { this.reader = webview; }}>
                </webview>
                <div id="error-modal" className="modal">
                    <div className="modal-content">
                        <h4>Error!!!!</h4>
                        <div className="row">
                            <p>{this.state.errorText}</p>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <a href="#!" className="modal-action modal-close waves-effect waves-red btn-flat">OK</a>
                    </div>
                </div>
                <div id="save-note-modal" className="modal">
                    <form className="col s12" onSubmit={this.handleSaveNoteSubmit}>
                        <div className="modal-content">
                            <h4>Add a note</h4>
                            <div className="row">
                                <div className="row">
                                    <div className="input-field col s12">
                                    <textarea id="textarea1" className="materialize-textarea" onChange={this.handleSaveNoteChange} value={this.state.note}></textarea>
                                        <label htmlFor="textarea1">Note</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button className="btn waves-effect btn-full" type="submit">Save</button>
                            <a href="#!" className="modal-action modal-close waves-effect waves-red btn-flat">Cancel</a>
                        </div>
                    </form>
                </div>
                <ul className="collapsible" data-collapsible="accordion">
                    <li>
                        <div className="collapsible-header"><i className="material-icons">filter_drama</i>Bookmarks</div>
                <div className="collapsible-body" style={{position: "absolute", top: "0"}}>
                            <Bookmarks bookmarks={this.state.bookmarks} onClickBookmark={this.handleBookmarkClickEpub} onClickBookmarkDelete={this.handleBookmarkDeleteClick} bookType={this.book.bookType} />
                        </div>
                    </li>
                    <li>
                        <div className="collapsible-header"><i className="material-icons">filter_drama</i>Highlights and Notes</div>
                <div className="collapsible-body" style={{position: "absolute", top: "0"}}>
                <Highlights highlights={this.state.highlights} onClickHighlight={this.handleBookmarkClickEpub} onClickHighlightDelete={this.handleHighlightDeleteClick} onClickNoteEdit={this.handleHighlightNoteEditClick} bookType={this.book.bookType} />
                        </div>
                    </li>
                    <li>
                        <div className="collapsible-header"><i className="material-icons">filter_drama</i>Search</div>
                <div className="collapsible-body" style={{position: "absolute", top: "0"}}>
                            <Search bookshelfPk={this.book.bookshelfPk} bookType={this.book.bookType} results={this.state.searchResult} onClickSearchResult={this.handleBookmarkClickEpub} />
                        </div>
                    </li>
                </ul>
            </div >
        );
    }

    private pdfOnDocumentComplete(pdfpages) {
        this.store.dispatch(setNumberPages(this.book, pdfpages));
    }

    private pdfOnPageComplete(page) {
        if (page !== this.state.currentPage) {
            this.store.dispatch(setCurrentPage(this.book, page));
        }
    }

    private pdfHandlePrevious() {
        this.store.dispatch(setCurrentPage(this.book, this.state.currentPage - 1));
    }

    private pdfHandleNext() {
        this.store.dispatch(setCurrentPage(this.book, this.state.currentPage + 1));
    }

    private pdfHandleAddBookmark() {
        this.store.dispatch(addBookmark(this.book, {
            page: this.state.currentPage,
            words: "Awaiting update.",
        }));
    }

    private pdfTriggerSelect() {
        let selections = this.pdfreader.getHightlightCoords();
        // send to high
        this.store.dispatch(
            addBookHighlight(this.book, {
                note: "",
                page: this.state.currentPage,
                selection: {
                    rects: selections
                }
            })
        );
    }

    private renderPagination(page, pages) {
        let previousButton = <li className="previous" onClick={this.pdfHandlePrevious}><a href="#">Previous <i className="fa fa-arrow-left"></i></a></li>;
        if (page === 1) {
            previousButton = <li className="previous disabled"><a href="#">Previous <i className="fa fa-arrow-left"></i></a></li>;
        }
        let nextButton = <li className="next" onClick={this.pdfHandleNext}><a href="#">Next <i className="fa fa-arrow-right"></i></a></li>;
        if (page === pages) {
            nextButton = <li className="next disabled"><a href="#">Next <i className="fa fa-arrow-right"></i></a></li>;
        }

        let addBookmark = <li className="next" onClick={this.pdfHandleAddBookmark}><a>Add Bookmark</a></li>;
        let getSelection = <li className="next" onMouseDown={this.pdfTriggerSelect}><a>Add Highlight</a></li>;
        let addNote = <li className="next" onMouseDown={() => this.handleSaveNoteClickPdf()}><a>Add note</a></li>;

        return (
                <nav>
                <ul className="pager">
                {previousButton}
                {nextButton}
                {addBookmark}
                {getSelection}
                {addNote}
                </ul>
                </nav>
        );
    }

    private renderPdfView(): React.ReactElement<{}>  {
        let pagination = null;
        if (this.state.pdfpages) {
            pagination = this.renderPagination(this.state.pdfpage, this.state.pdfpages);
        }

        return (
            <div className="rs">
                {pagination}

                <Pdf
            file={this.pdfFile}
            onPageComplete={this.pdfOnPageComplete}
            onDocumentComplete={this.pdfOnDocumentComplete}
            ref={(Pdf) => { this.pdfreader = Pdf; }}
            page={!this.state.currentPage ? 1 : this.state.currentPage}
            highlights={this.state.highlights}
            onOutlineItemClick={this.handleBookmarkClickPdf}
                />
                <div id="error-modal-pdf" className="modal">
                    <div className="modal-content">
                        <h4>Error!!!!</h4>
                        <div className="row">
                            <p>{this.state.errorText}</p>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <a href="#!" className="modal-action modal-close waves-effect waves-red btn-flat">OK</a>
                    </div>
                </div>
                <div id="save-note-modal-pdf" className="modal">
                    <form className="col s12" onSubmit={this.handleSaveNoteSubmitPdf}>
                        <div className="modal-content">
                            <h4>Add a note</h4>
                            <div className="row">
                                <div className="row">
                                    <div className="input-field col s12">
                                    <textarea id="textarea1" className="materialize-textarea" onChange={this.handleSaveNoteChange} value={this.state.note}></textarea>
                                        <label htmlFor="textarea1">Note</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button className="btn waves-effect btn-full" type="submit">Save</button>
                            <a href="#!" className="modal-action modal-close waves-effect waves-red btn-flat">Cancel</a>
                        </div>
                    </form>
                </div>
                <ul className="collapsible" data-collapsible="accordion">
                    <li>
                        <div className="collapsible-header"><i className="material-icons">filter_drama</i>Bookmarks</div>
                        <div className="collapsible-body" style={{position: "absolute", top: "0"}}>
                            <Bookmarks bookmarks={this.state.bookmarks} onClickBookmark={this.handleBookmarkClickPdf} onClickBookmarkDelete={this.handleBookmarkDeleteClick} bookType={this.book.bookType} />
                        </div>
                    </li>
                    <li>
                        <div className="collapsible-header"><i className="material-icons">filter_drama</i>Highlights and Notes</div>
                        <div className="collapsible-body" style={{position: "absolute", top: "0"}}>
                            <Highlights highlights={this.state.highlights} onClickHighlight={this.handleBookmarkClickPdf} onClickHighlightDelete={this.handleHighlightDeleteClick} onClickNoteEdit={this.handleHighlightNoteEditClick} bookType={this.book.bookType} />
                        </div>
                    </li>
                    <li>
                        <div className="collapsible-header"><i className="material-icons">filter_drama</i>Search</div>
                        <div className="collapsible-body" style={{position: "absolute", top: "0"}}>
                            <Search bookshelfPk={this.book.bookshelfPk} bookType={this.book.bookType} results={this.state.searchResult} onClickSearchResult={this.handleBookmarkClickPdf} />
                        </div>
                    </li>
                </ul>
            </div>
        );
    }

    private handleSaveNoteClickPdf() {
        // trigger highlight check
        // if check shows highlight open dialog else error dialog
        // check if there is a new highlight
        let selections = undefined;
        try {
            selections = this.pdfreader.getHightlightCoords();
            if (Math.abs(selections[0][0].x - selections[0][1].x) < 1.0) {
                selections = undefined;
            }
        } catch (e) {
            console.debug("selection error", e);
        }
        if (selections) {
            this.setState({highlight: {rects: selections}});
            $("#save-note-modal-pdf").modal("open");
        } else {
            this.setState({errorText: "No selected text"});
            $("#error-modal-pdf").modal("open");
        }
    }

    private handleSaveNoteSubmitPdf(event) {
        event.preventDefault();
        $("#save-note-modal-pdf").modal("close");
        let selection = this.state.highlight.selection ? this.state.highlight.selection : { rects : this.state.highlight.rects };
        this.store.dispatch(
            addBookHighlight(this.book, {
                id: this.state.highlight.id,
                note: this.state.note,
                page: this.state.currentPage,
                selection
            })
        );
    }

    private handleSaveNoteClick() {
        // trigger highlight check
        // if check shows highlight open dialog else error dialog
        this.handleTriggerClick({msg: "getSelection", rtn: "openNoteDialog"});
    }

    private handleSaveNoteChange(event) {
        this.setState({noteId: undefined});
        this.setState({note: event.target.value});
    }

    private handleSaveNoteSubmit(event) {
        event.preventDefault();
        $("#save-note-modal").modal("close");
        let file = this.state.highlight.file ? this.state.highlight.file : this.getSpineFromBrickId(this.state.highlight.start);
        let selection = this.state.highlight.selection ? this.state.highlight.selection : { word_ids : this.state.highlight.selectionArray };

        this.store.dispatch(
            addBookHighlight(this.book, {
                id: this.state.highlight.id,
                note: this.state.note,
                file,
                selection,
            })
        );
    }

    private handleHighlightNoteEditClick(highlightId: number) {
        let selectedNoteList = this.state.highlights.filter((h) => h.id === highlightId);
        if (selectedNoteList.length) {
            let selectedNote = selectedNoteList[0];
            this.setState({highlight: selectedNote, note: selectedNote.note});
            if (this.book.bookType === BookType.Epub) {
                $("#save-note-modal").modal("open");
            } else {
                $("#save-note-modal-pdf").modal("open");
            }
        }
    }

    private handleBookmarkClickEpub(brickId) {
        let spineRef = this.getSpineFromBrickId(brickId);
        this.reader.send(
            "page-nav",
            {
                detail: {
                    brickId: String(brickId),
                    msg: "gotoBookmark",
                    spineRef,
                }
            }
        );
    }

    private handleBookmarkClickPdf(pageId) {
        this.store.dispatch(setCurrentPage(this.book, pageId));
    }

    private handleBookmarkDeleteClick(bookmarkId: number) {
        this.store.dispatch(deleteBookmark(this.book, bookmarkId));
    }

    private handleHighlightDeleteClick(highlightId: number) {
        this.store.dispatch(deleteBookHighlight(this.book, highlightId));
    }

    private handleTriggerClick(data) {
        // event.preventDefault();
        switch (data.msg) {
        case "getLastPage":
            let book = this.getBookFromState();
            this.store.dispatch(lastPage(book));
            break;
        case "gotoId":
            let lastWordId = this.getBookFromState().lastWordId;
            let spineRef = this.getSpineFromBrickId(lastWordId);
            this.reader.send("page-nav", {detail: {msg: data.msg, spineRef, brickId: String(lastWordId)}});
            break;
        case "gotoBookmark":
            this.reader.send(
                "page-nav",
                {
                    detail: {
                        brickId: String(data.brickId),
                        msg: "gotoBookmark",
                        spineRef,
                    }
                }
            );
            break;
        case "showSelection":
            this.reader.send(
                "page-nav",
                {
                    detail: {
                        highlights: this.state.highlights,
                        msg: "showSelection",
                    }
                }
            );
            break;
        case "showSettings":
            this.reader.send(
                "page-nav",
                {
                    detail: {
                        msg: "showSettings",
                    }
                }
            );
            break;
        case "showTableOfContents":
            this.reader.send(
                "page-nav",
                {
                    detail: {
                        msg: "showTableOfContents",
                    }
                }
            );
            break;
        case "getSelection":
            this.reader.send(
                "page-nav",
                {
                    detail: {
                        msg: "getSelection",
                        rtn: data.rtn,
                    }
                }
            );
            break;
        default:
            this.reader.send("page-nav", {detail: {msg: data.msg}});
        }
    }

    private getSpineFromBrickId(brickId: number) {
        let book = this.getBookFromState();
        for (let spineBrickMap of book.spineBrickMap) {
            if (brickId >= spineBrickMap.first && brickId <= spineBrickMap.last) {
                return spineBrickMap.idref;
            }
        }
    }

    private getBookFromState(): Book {
        return this.store.getState().bookshelf.books[this.props.match.params.bookshelfPk];
    }

    private getSelection(readerAction) {
        if (typeof readerAction.start === "undefined") {
            return undefined;
        }
        const selectionStart = Number(readerAction.start);
        const selectionEnd = Number(readerAction.end);
        let selectionArray = [];
        for (let i = selectionStart; i <= selectionEnd; i++) {
            selectionArray.push(String(i));
        }
        return {selectionArray, start: selectionStart};
    }
}
