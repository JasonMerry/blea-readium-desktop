import * as React from "react";
import { Store } from "redux";

import { lazyInject } from "blea/renderer/di";

import { IAppState } from "blea/renderer/reducers";

import { Bookshelf } from "blea/entities/bookshelf";

import BookCard from "blea/components/BookCard";

import { getServer } from "blea/resources/server";

import * as $ from "materialize-css/node_modules/jquery/dist/jquery.js";

interface BooklistState {
    bookshelf: Bookshelf;
    layout: string;
}

export default class Booklist extends React.Component<undefined, BooklistState> {
    public state: BooklistState;

    private unSubscriber;

    @lazyInject("store")
    private store: Store<IAppState>;

    constructor() {
        super();
        this.state = {
            bookshelf: this.store.getState().bookshelf,
            layout: this.store.getState().layout,
        };
    }

    public componentDidUpdate() {
        $(".tooltipped").tooltip();
    }

    public componentDidMount() {
        this.unSubscriber = this.store.subscribe(() => {
            this.setState({
                bookshelf: this.store.getState().bookshelf,
                layout: this.store.getState().layout,
            });
        });
        $(".tooltipped").tooltip();
    }

    public componentWillUnmount() {
        this.unSubscriber();
    }

    public render(): React.ReactElement<{}>  {

        if (!this.state.bookshelf.books) {
            return (
                <div className="card">
                    <div className="card-content">
                        <h2 className="card-title">Your bookshelf is empty</h2>
                        <p>You havent purchased or been assigned any books yet.</p>
                        <p>Log in to your web account to purchase some books
                            or speak to your lecturer for them to add books for you.
                        </p>
                    </div>
                    <div className="card-action">
                        <a href="https://learn.blackwell.co.uk" className="btn">Web account</a>
                    </div>
                </div>
            );
        } else {
            let serverPath = getServer().SERVER_PATH;
            let shelfType = "bookshelf";
            let pageClass1 = "bookshelf-item";
            let pageClass2 = "card small";
            let coverClass = "col l2 m2 s3";
            let titleClass = "item-title";
            let insightsClass = "insights";

            if (this.state.layout === "grid") {
                shelfType += " grid";
                pageClass1 += " col l3 m4 s6";
                coverClass = "";
                titleClass += " card-title";
                insightsClass += " card-content";
            } else {
                shelfType += " collection list";
                pageClass1 += " collection-item";
                pageClass2 = "";
                titleClass += " col l10 m10 s9";
                insightsClass += " col l4 m4 s12";
            }

            return (
                <ul className={shelfType}>
                    {this.sortedBooks().map((bookshelfitem) => (
                        <BookCard bookshelfPk={bookshelfitem.bookshelfPk}
                            isbn13={bookshelfitem.isbn13}
                            key={bookshelfitem.bookshelfPk}
                            title={bookshelfitem.title}
                            author={bookshelfitem.author}
                            coverUrl={serverPath + bookshelfitem.cover}
                            downloading={bookshelfitem.download.status}
                            progress={bookshelfitem.download.progress}
                            bookType={bookshelfitem.bookType}
                            pageClass1={pageClass1}
                            pageClass2={pageClass2}
                            coverClass={coverClass}
                            titleClass={titleClass}
                            insightsClass={insightsClass}
                        />
                    ))}
                </ul>
            );
        }
    }

    private sortedBooks() {
        return Object.keys(
            this.state.bookshelf.books).map(
                (key, index) => { return this.state.bookshelf.books[key]; }
            ).sort((a, b) => {
                return a.order - b.order;
            });
    }

}
