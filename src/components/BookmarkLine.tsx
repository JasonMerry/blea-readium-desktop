import * as React from "react";

import { Bookmark, BookType } from "blea/entities/book";

interface IBookmarkProps {
    key: number;
    bookmark: Bookmark;
    bookType: BookType;
    onClickBookmark: any;
    onClickBookmarkDelete: any;
};

export class BookmarkLine extends React.Component<IBookmarkProps, undefined> {

    private onClickBookmark;
    private onClickBookmarkDelete;

    constructor(props) {
        super(props);
        this.onClickBookmark = props.onClickBookmark;
        this.onClickBookmarkDelete = props.onClickBookmarkDelete;
    }

    public render(): React.ReactElement<{}>  {
        return (
                <li className="collection-item"><span onClick={ () => this.onClickBookmark(this.props.bookType === BookType.Epub ? this.props.bookmark.wordId : this.props.bookmark.page)}>{this.props.bookmark.words}</span>{this.props.bookmark.id ? <span onClick={ () => this.onClickBookmarkDelete(this.props.bookmark.id)}> | Delete</span> : null }</li>
        );
    }
}
