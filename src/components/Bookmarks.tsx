import * as React from "react";

import { Bookmark, BookType } from "blea/entities/book";

import { BookmarkLine } from "blea/components/BookmarkLine";

interface IBookmarksProps {
    bookmarks: Bookmark[];
    bookType: BookType;
    onClickBookmark: any;
    onClickBookmarkDelete: any;
};

export class Bookmarks extends React.Component<IBookmarksProps, undefined> {

    private onClickBookmark;
    private onClickBookmarkDelete;

    constructor(props) {
        super(props);
        this.onClickBookmark = props.onClickBookmark;
        this.onClickBookmarkDelete = props.onClickBookmarkDelete;
    }

    public render(): React.ReactElement<{}>  {
        if (this.props.bookmarks) {
            return (
                <div className="">
                <ul className="collection">

                {this.props.bookmarks.map((bookmark, index) => (
                    <BookmarkLine key={index} onClickBookmark={this.onClickBookmark} onClickBookmarkDelete={this.onClickBookmarkDelete} bookmark={bookmark} bookType={this.props.bookType} />
            ))}
                </ul>
                </div>
        );
        } else {
            return (<p>no bookmarks</p>);
        }
    }

}
