import * as React from "react";

import { Store } from "redux";

import { clearError } from "blea/actions/auth";
import { lazyInject } from "blea/renderer/di";
import { IAppState } from "blea/renderer/reducers";

import * as $ from "materialize-css/node_modules/jquery/dist/jquery.js";

interface IErrorProps {
    errorMessage: string;
    errorStatus: number;
}

export default class ErrorBox extends React.Component<IErrorProps, undefined> {

    @lazyInject("store")
    private store: Store<IAppState>;

    constructor(props) {
        super(props);

        this.handleButtonClick = this.handleButtonClick.bind(this);
    }

    public componentDidMount() {
        $(".modal").modal();
        $("#errorModal").modal("open");
    }

    public componentWillUnomount() {
        $("#errorModal").remove();
    }

    public render(): React.ReactElement<{}>  {

        return (
            <form id="errorModal" className="modal" ref="errorModal" onSubmit={this.handleButtonClick}>
                <div className="modal-content">
                    <h4>Ooops</h4>
                    <p>{this.props.errorMessage}</p>
                </div>
                <div className="modal-footer">
                    <button type="submit" ref="close" className="modal-action modal-close waves-effect red btn">
                        Try again
                    </button>
                </div>
            </form>
        );
    }

    private handleButtonClick(event) {
        event.preventDefault();
        this.store.dispatch(clearError());
    }
}
