import * as React from "react";

import { BookType } from "blea/entities/book";

interface IHighlightProps {
    bookType: BookType;
    key: number;
    highlight: any;
    onClickHighlight: any;
    onClickHighlightDelete: any;
    onClickNoteEdit: any;
};

export class HighlightLine extends React.Component<IHighlightProps, undefined> {

    constructor(props) {
        super(props);
    }

    public render(): React.ReactElement<{}>  {
        return (
                <li className="collection-item" key={this.props.key}><span onClick={ () => this.props.onClickHighlight(Number( this.props.bookType === BookType.Epub ? this.props.highlight.selection.word_ids[0] : this.props.highlight.page ))}>{this.props.highlight.note ? this.props.highlight.note : this.props.highlight.selection.text}</span>{this.props.highlight.id ? <span onClick={ () => this.props.onClickHighlightDelete(this.props.highlight.id)}> | Delete</span> : null } {this.props.highlight.note ? <span onClick={ () => this.props.onClickNoteEdit(this.props.highlight.id)}> | Edit</span> : null }</li>
        );
    }
}
