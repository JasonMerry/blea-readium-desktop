import * as React from "react";

import { BookType } from "blea/entities/book";

import { HighlightLine } from "blea/components/HighlightLine";

interface IHighlightsProps {
    bookType: BookType;
    highlights: any[];
    onClickHighlight: any;
    onClickHighlightDelete: any;
    onClickNoteEdit: any;
};

export class Highlights extends React.Component<IHighlightsProps, undefined> {

    constructor(props) {
        super(props);
    }

    public render(): React.ReactElement<{}>  {
        if (this.props.highlights) {

            return (
                    <div className="">
                    <h4>Notes</h4>
                <ul className="collection">
                    {this.props.highlights.filter(this.isNote).map((highlight, index) => (
                        <HighlightLine key={index} onClickHighlight={this.props.onClickHighlight} onClickHighlightDelete={this.props.onClickHighlightDelete} onClickNoteEdit={this.props.onClickNoteEdit} highlight={highlight} bookType={this.props.bookType} />
                ))}
                </ul>
                    <h4>Highlights</h4>
                <ul className="collection">
                    {this.props.highlights.filter((e) => !this.isNote(e)).map((highlight, index) => (
                            <HighlightLine key={index} onClickHighlight={this.props.onClickHighlight} onClickHighlightDelete={this.props.onClickHighlightDelete} onClickNoteEdit={this.props.onClickNoteEdit} highlight={highlight} bookType={this.props.bookType} />
                ))}
                </ul>
                </div>
        );
        } else {
            return (<p>no highlights</p>);
        }
    }

    private isNote(element) {
        if (element.note) {
            return true;
        }
        return false;
    }
}
