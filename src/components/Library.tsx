import * as React from "react";
import { Store } from "redux";

import { loginRequestDebug } from "blea/actions/auth";
import { fetchBookshelf, syncBookshelf } from "blea/actions/bookshelf";

import { setLocale } from "blea/actions/i18n";
import { Translator } from "blea/i18n/translator";
import { lazyInject } from "blea/renderer/di";
import { IAppState } from "blea/renderer/reducers";

import AppToolbar from "blea/components/AppToolbar";
import Booklist from "blea/components/Booklist";
import SwitchGridList from "blea/components/SwitchGridList";

interface ILibraryState {
    locale: string;
}

export default class Library extends React.Component<undefined, ILibraryState> {
    public state: ILibraryState;

    private unSubscriber;

    @lazyInject(Translator)
    private translator: Translator;

    @lazyInject("store")
    private store: Store<IAppState>;

    constructor() {
        document.body.id = "bookshelf";
        super();
        this.state = {
            locale: this.store.getState().i18n.locale,
        };

        this.handleLocaleChange = this.handleLocaleChange.bind(this);
        this.handleBookshelfFetch = this.handleBookshelfFetch.bind(this);
        this.handleBookshelfSync = this.handleBookshelfSync.bind(this);
        this.handleTestClick = this.handleTestClick.bind(this);
    }

    public componentDidMount() {
        this.unSubscriber = this.store.subscribe(() => {
            this.setState({
                locale: this.store.getState().i18n.locale,
            });
        });
        this.handleBookshelfSync();
        console.log("location", location);
    }

    public componentWillUnmount() {
        this.unSubscriber();
    }

    public render(): React.ReactElement<{}>  {
        const __ = this.translator.translate;
        return (
            <div>
                <AppToolbar />
                <div className="container">
                    <h1>{__("library.heading")}</h1>
                    <SwitchGridList />
                    <div className="row">
                        <Booklist />
                    </div>
                </div>
            </div>
        );
    }

    private handleLocaleChange(event: any, index: any, locale: string) {
        this.store.dispatch(setLocale(locale));
    }

    private handleBookshelfFetch() {
        this.store.dispatch(fetchBookshelf());
    }

    private handleBookshelfSync() {
        this.store.dispatch(syncBookshelf());
    }

    private handleTestClick() {
        this.store.dispatch(loginRequestDebug());
    }
}
