import * as React from "react";
import { Store } from "redux";

import { changeForm, clearError, loginRequest } from "blea/actions/auth";
import { AuthLogin } from "blea/entities/auth";
import { lazyInject } from "blea/renderer/di";
import { IAppState } from "blea/renderer/reducers";

import * as $ from "materialize-css/node_modules/jquery/dist/jquery.js";

import Materialize from "materialize-css/dist/js/materialize.min.js";

import ErrorBox from "blea/components/ErrorBox";

interface LoginFormState {
    currentlySending: boolean;
    errorMessage: string;
    errorStatus: number;
    loggedIn: boolean;
    password: string;
    username: string;
}

export default class Login extends React.Component<undefined, LoginFormState> {
    public state: LoginFormState;

    private unSubscriber;

    @lazyInject("store")
    private store: Store<IAppState>;

    constructor() {
        document.body.id = "login";
        super();
        this.state = {
            currentlySending: this.store.getState().auth.currentlySending,
            errorMessage: this.store.getState().auth.errorMessage,
            errorStatus: this.store.getState().auth.errorStatus,
            loggedIn: this.store.getState().auth.userState.loggedIn,
            password: this.store.getState().auth.formState.password,
            username: this.store.getState().auth.formState.username,
        };
        this.handleUsernameChange = this.handleUsernameChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);

        this.emitFormChange = this.emitFormChange.bind(this);
        this.handleLoginClick = this.handleLoginClick.bind(this);
    }

    public componentDidMount() {
        this.unSubscriber = this.store.subscribe(() => {
            this.setState({
                currentlySending: this.store.getState().auth.currentlySending,
                errorMessage: this.store.getState().auth.errorMessage,
                errorStatus: this.store.getState().auth.errorStatus,
                loggedIn: this.store.getState().auth.userState.loggedIn,
                password: this.store.getState().auth.formState.password,
                username: this.store.getState().auth.formState.username,
            });
        });
    }

    public componentWillUnmount() {
        this.unSubscriber();
    }

    public render(): React.ReactElement<{}> {
        const texts = {
            button: "Start reading",
            email: "Email",
            forgot: "Forgot password?",
            password: "Password",
        };

        const isError = this.state.errorStatus;
        $(() => {
            Materialize.updateTextFields();
        });

        return (
            <div className="rs">
                <header>
                    <img src="img/logo.svg" alt="Blackwell's - For Learning : For Life"/>
                    <small className="version">Version 0.0.0</small>
                </header>
                <form className="card-panel" onSubmit={this.handleLoginClick}>
                    <h1>Login</h1>
                    9s83utWz8MGBg98Y6JErQy46v2FQ8M
                    <div className="input-field">
                        <input
                            id="email"
                            type="email"
                            className="validate"
                            onChange={this.handleUsernameChange}
                            value={this.state.username}
                        />
                        <label
                            htmlFor="email"
                            data-error="Incorrect email format"
                            data-success="Correct email format"
                            className={this.state.username ? "active" : ""}
                        >
                            {texts.email}
                        </label>
                    </div>
                    <div className="input-field">
                        <input
                            id="password"
                            type="password"
                            className="validate"
                            onChange={this.handlePasswordChange}
                            value={this.state.password}
                        />
                        <label
                            htmlFor="password"
                            className={this.state.password ? "active" : ""}
                        >
                            {texts.password}
                        </label>
                    </div>
                    <button className="btn waves-effect btn-full" type="submit">
                        {texts.button}
                    </button>
                    <a href="#forgotPass" className="forgotPassword">{texts.forgot}</a>
                </form>
                <footer>
                    <small>&copy; Blackwell&#39;s Ltd. <em>Version 0.0.0</em></small>
                </footer>
                { isError >= 400 ? (
                    <ErrorBox errorStatus={this.state.errorStatus} errorMessage={this.state.errorMessage}/>
                ) : (
                    ""
                    // Need to show when offline, when the offline feature is set up and ready
                    // <ErrorBox errorStatus={0} errorMessage={"Offline"}/>
                )}
            </div>
        );
    }

    private handleLoginClick(event) {
        event.preventDefault();
        this.store.dispatch(clearError());
        let loginData = {
            password: this.state.password,
            username: this.state.username,
        };
        this.store.dispatch(loginRequest(loginData));
    }

    private emitFormChange(newFormState: AuthLogin) {
        this.store.dispatch(changeForm(newFormState));
    }

    private handleUsernameChange(event: any) {
        const newFormState = { password: this.state.password, username: event.target.value };
        this.emitFormChange(newFormState);
    }

    private handlePasswordChange(event: any) {
        const newFormState = { username: this.state.username, password: event.target.value };
        this.emitFormChange(newFormState);
    }

}
