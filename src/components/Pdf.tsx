import * as React from "react";

import * as $ from "materialize-css/node_modules/jquery/dist/jquery.js";

const pdfjs = require("pdfjs-dist");
const pdfjsViewer = require("pdfjs-dist/web/pdf_viewer");

interface IPdfProps {
    content?: string;
    documentInitParameters?: any;
    file?: any; // Could be File object or URL string.
    loading?: any;
    page?: number;
    scale?: number;
    rotate?: number;
    onDocumentComplete?: any;
    onDocumentError?: any;
    onPageComplete?: any;
    className?: string;
    style?: any;
    highlights?: any;
    onOutlineItemClick?: any;
}

interface IPdfState {
    pdf: any;
}

import { PdfOutline } from "blea/components/PdfOutline";

// borrowed from https://github.com/mikecousins/react-pdf-js

export class Pdf extends React.Component<IPdfProps, any> {

    public static defaultProps = {
        page: 1,
        scale: 1.0,
    };

    public state = {
        canvas: undefined,
        page: undefined,
        pdf: undefined,
    };

    public canvas;

    public componentDidMount() {
        this.renderPdf();
    }

    public componentWillReceiveProps(newProps) {
        const { pdf } = this.state;
        const { page } = this.state;

        if (pdf && ((newProps.page && newProps.page !== this.props.page) ||
                    (newProps.scale && newProps.scale !== this.props.scale) ||
                    (newProps.rotate && newProps.rotate !== this.props.rotate))) {
            this.setState({ page: null });
            pdf.getPage(newProps.page).then(this.onPageComplete);
        }
        if (page && ((newProps.highlights && newProps.highlights !== this.props.highlights))) {
            this.showHighlights();
        }
    }

    public componentWillUnmount() {
        const { pdf } = this.state;
        if (pdf) {
            pdf.destroy();
        }
    }

    public onDocumentComplete = (pdf) => {
        this.setState({ pdf });
        const { onDocumentComplete } = this.props;
        if (typeof onDocumentComplete === "function") {
            onDocumentComplete(pdf.numPages);
        }
        pdf.getPage(this.props.page).then(this.onPageComplete);
    }

    public onDocumentError = (err) => {
        if (err.isCanceled && err.pdf) {
            err.pdf.destroy();
        }
        if (typeof this.props.onDocumentError === "function") {
            this.props.onDocumentError(err);
        }
    }

    public onPageComplete = (page) => {
        this.setState({ page });
        this.renderPdf();
        const { onPageComplete } = this.props;
        if (typeof onPageComplete === "function") {
            onPageComplete(page.pageIndex + 1);
        }
    }

    public getDocument = () => {
        pdfjs.PDFJS.workerSrc = "./pdf.worker.js";
        let outerThis = this;

        pdfjsViewer.PDFJS.getDocument(this.props.file).then((pdfDocument) => {
            // Document loaded, retrieving the page.
            outerThis.onDocumentComplete(pdfDocument);
        });
    }

    public renderPdf = () => {
        const { page } = this.state;
        const { pdf } = this.state;

        let container: HTMLElement = document.getElementById("pageContainer");
        const currentPage = this.props.page;

        if (pdf) {
            return pdf.getPage(currentPage).then((pdfPage) =>  {
                // Creating the page view with default parameters.
                let pdfPageView = new pdfjsViewer.PDFJS.PDFPageView({
                    annotationLayerFactory: new pdfjsViewer.PDFJS.DefaultAnnotationLayerFactory(),
                    container,
                    defaultViewport: pdfPage.getViewport(1.0),
                    id: currentPage,
                    scale: this.props.scale,
                    // We can enable text/annotations layers, if needed
                    textLayerFactory: new pdfjsViewer.PDFJS.DefaultTextLayerFactory(),
                });
                // Associates the actual page with the view, and drawing it
                pdfPageView.setPdfPage(pdfPage);
                pdfPageView.draw();
                this.showHighlights();
            });
        } else {
            this.getDocument();
        }
    }

    public render() {
        const { loading } = this.props;
        const { page } = this.state;
        const { pdf } = this.state;
        $(".collapsible1").collapsible();

        return page ?
            <div><PdfOutline pdf={pdf} onItemClick={this.props.onOutlineItemClick}/><div id="pageContainer" className="pdfViewer singlePageView"></div></div> :
        loading || <div>Loading PDF...</div>;
    }

    public getHightlightCoords() {
        const { page } = this.state;
        let container: HTMLElement = document.getElementById("pageContainer");
        let innerContainer: Node = container.firstChild;
        let pageRect = (innerContainer as HTMLElement).getClientRects()[0];
        let selectionRects = window.getSelection().getRangeAt(0).getClientRects();
        let viewport = page.getViewport(this.props.scale);
        // only handle 1 selection
        let xfiddle = viewport.width / pageRect.width;
        let yfiddle = viewport.height / pageRect.height;
        let r;
        let selectedPdf;
        let selectedPdfDj = [];
        let shrink = 1.0;
        for (let i = 0; i < selectionRects.length; i++) {
            r = selectionRects.item(i);

            selectedPdf = viewport.convertToPdfPoint((r.left - pageRect.left) * xfiddle, (r.top - pageRect.top) * yfiddle).concat(
            viewport.convertToPdfPoint((r.right - pageRect.left) * xfiddle, (r.bottom - pageRect.top) * yfiddle));

            selectedPdfDj.push([
                {x: selectedPdf[0] * shrink, y: selectedPdf[1] * shrink},
                {x: selectedPdf[2] * shrink, y: selectedPdf[3] * shrink},
            ]);
        }
        return selectedPdfDj;
    }

    public showHighlights() {
        let bounds;
        const { page } = this.state;
        if (!page || !this.props.highlights) {
            return;
        }
        let viewport = page.getViewport(this.props.scale);
        let container: HTMLElement = document.getElementById("pageContainer");
        let innerContainer: Node = container.firstChild;
        let innerPage: HTMLElement = (innerContainer as HTMLElement);
        if (!innerPage) {
            return;
        }
        let innerPageRects = innerPage.getClientRects()[0];
        let xfiddle = viewport.width / innerPageRects.width;
        let yfiddle = viewport.height / innerPageRects.height;

        // remove highlights
        this.props.highlights.forEach(
            (highlight, i) => {
                if (highlight.page === this.props.page) {
                    highlight.selection.rects.forEach((rect, j) => {
                        let highlightId = "highlight_" + highlight.id + "_" + j;
                        let el = document.getElementById(highlightId);
                        if (el) {
                            el.remove();
                        }
                    });
                }
            }
        );

        this.props.highlights.forEach(
            (highlight, i) => {
                if (highlight.page === this.props.page) {
                    highlight.selection.rects.forEach((rect, j) => {
                        let highlightId = "highlight_" + highlight.id + "_" + j;
                        // // only add highlight if not already visible
                        if (!document.getElementById(highlightId)) {
                            bounds = viewport.convertToViewportRectangle([rect[0].x, rect[0].y, rect[1].x, rect[1].y]);
                            let el = document.createElement("div");
                            el.setAttribute(
                                "style",
                                "position: absolute; background-color: rgba(255, 255, 51, 0.3);" +
                                    "left:" + Math.min(bounds[0], bounds[2]) / xfiddle + "px; top:" + Math.min(bounds[1], bounds[3]) / yfiddle + "px;" +
                                    "width:" + Math.abs(bounds[0] - bounds[2]) / xfiddle + "px; height:" + Math.abs(bounds[1] - bounds[3]) / yfiddle + "px;");
                            el.setAttribute("id", highlightId);
                            innerPage.appendChild(el);
                        }
                    });
                }
            }
        );
    }
}
