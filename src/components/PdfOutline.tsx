import * as React from "react";

import * as $ from "materialize-css/node_modules/jquery/dist/jquery.js";

import {
    isDefined,
    makeCancellable,
    Ref,
} from "blea/shared/util";

interface IPdfOutlineProps {
    pdf: any;
    onItemClick?: any;
}

export class PdfOutline extends React.Component<IPdfOutlineProps, any> {

    public state = {
        outline: undefined,
    };

    private runningTask;

    constructor() {
        super();
        this.onLoadSuccess = this.onLoadSuccess.bind(this);
        this.onLoadError = this.onLoadError.bind(this);
        this.onParseSuccess = this.onParseSuccess.bind(this);
        this.onParseError = this.onParseError.bind(this);
    }

    public componentDidMount() {
        this.loadOutline();
    }

    public componentWillUnmount() {
        if (this.runningTask && this.runningTask.cancel) {
            this.runningTask.cancel();
        }
    }

    public componentDidUpdate() {
        $(".collapsible").collapsible();
    }

    public render() {
        const { pdf } = this.props;
        const { outline } = this.state;

        if (!pdf || !outline) {
            return null;
        }

        return (
                <ul className="collapsible" data-collapsible="accordion">
                    <li>
                        <div className="collapsible-header"><i className="material-icons">filter_drama</i>Contents</div>
                        <div className="collapsible-body">
                            {this.renderOutline()}
                        </div>
                    </li>
                </ul>
        );
    }

    public renderOutline(outline = this.state.outline) {
        return (
                <ul>
                {
                    outline.map((item, itemIndex) => (
                            <li
                        key={
                            typeof item.destination === "string" ?
                                item.destination :
                                itemIndex
                        }
                            >
                            <a
                        href="#"
                        onClick={(event) => {
                            event.preventDefault();
                            this.onItemClick(item);
                        }}
                            >
                            {item.title}
                        </a>
                            {item.items && this.renderOutline(item.items)}
                        </li>
                    ))
                }
                </ul>
        );
    }

    private loadOutline(props = this.props) {
        const { pdf } = props;

        if (!pdf) {
            throw new Error("Attempted to load an outline, but no document was specified.");
        }

        if (this.state.outline !== null) {
            this.setState({ outline: null });
        }

        this.runningTask = makeCancellable(pdf.getOutline());

        return this.runningTask.promise
            .then(this.onLoadSuccess)
            .catch(this.onLoadError);
    }

    private onLoadSuccess(outline) {
        this.runningTask = makeCancellable(this.parseOutline(outline));

        return this.runningTask.promise
            .then(this.onParseSuccess)
            .catch(this.onParseError);
    }

    private onLoadError(error) {
        if (error === "cancelled") {
            return;
        }

        this.setState({ outline: false });
    }

    private onParseSuccess(outline) {
        this.setState({ outline });
    }

    private onParseError(error) {

        this.setState({ outline: false });
    }

    private async mapOutlineItem(item) {

        const { pdf } = this.props;

        const mappedItem = {
            destination: item.dest,
            items: [],
            title: item.title,
            async getDestination() {
                if (typeof this.destination === "string") {
                    return pdf.getDestination(this.destination);
                }
                return this.destination;
            },
            async getPageIndex() {
                if (!isDefined(this.pageIndex)) {
                    const destination = await this.getDestination();
                    if (destination) {
                        const [ref] = destination;
                        this.pageIndex = pdf.getPageIndex(new Ref(ref));
                    }
                }
                return this.pageIndex;
            },
            async getPageNumber() {
                if (!isDefined(this.pageNumber)) {
                    this.pageNumber = await this.getPageIndex() + 1;
                }
                return this.pageNumber;
            },
        };

        if (item.items && item.items.length) {
            mappedItem.items = await Promise.all(item.items.map((subitem) => this.mapOutlineItem(subitem)));
        }

        return mappedItem;
    }

    private async parseOutline(outline) {
        if (!outline) {
            return null;
        }
        return Promise.all(outline.map((item) => this.mapOutlineItem(item)));
    }

    private async onItemClick(item) {
        const pageNumber = await item.getPageNumber();
        this.props.onItemClick(pageNumber);
    }
}
