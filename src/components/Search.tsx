import * as React from "react";

import { BookType } from "blea/entities/book";

import { SearchDialog } from "blea/components/SearchDialog";
import { SearchResultLine } from "blea/components/SearchResultLine";

interface SearchProps {
    bookshelfPk: number;
    bookType: BookType;
    results: any;
    onClickSearchResult: any;
};

export class Search extends React.Component<SearchProps, undefined> {

    constructor(props) {
        super(props);
    }

    public render(): React.ReactElement<{}>  {
        let searchDialog = <SearchDialog bookshelfPk={this.props.bookshelfPk}/>;
        let resultsDisp = <p>no results</p>;
        // merge results entries into one list
        let combinedResults = [];
        let searchTerm = "";
        let index = 0;
        if (this.props.results) {
            this.props.results.forEach((group) => {
                searchTerm = group.value;
                group.entries.forEach((entry) => {
                    // highlight search term
                    let regexp = new RegExp(searchTerm, "gi");
                    entry.context = entry.context.replace(regexp, "<em>" + searchTerm + "</em>");
                    entry.index = ++index;
                    combinedResults.push(entry);
                });
            });
        }
        if (combinedResults) {
            resultsDisp = (
                <div>
                    <ul className="collection" style={{background: "#fff", width: "300px", float: "left", margin: "0.3em"}}>
                {combinedResults.map((result) => (
                        <SearchResultLine key={result.index} onClickSearchResult={this.props.onClickSearchResult} result={result} bookType={this.props.bookType} />
                ))}
                </ul>
                </div>
            );
        }
        return (
                <div className="">
                {searchDialog}
                {resultsDisp}
                </div>
        );
    }

}
