import * as React from "react";

import { lazyInject } from "blea/renderer/di";
import { IAppState } from "blea/renderer/reducers";
import { Store } from "redux";

import { getSearchResult } from "blea/actions/book";

interface SearchDialogProps {
    bookshelfPk: number;
}

interface SearchFormState {
    query: string;
}

export class SearchDialog extends React.Component<SearchDialogProps, SearchFormState> {

    @lazyInject("store")
    private store: Store<IAppState>;

    constructor(props) {
        super(props);
        this.state = {query: ""};
        this.handleChange = this.handleChange.bind(this);
        this.handleSearchClick = this.handleSearchClick.bind(this);
    }

    public render(): React.ReactElement<{}>  {
        const texts = {
            button: "Search",
            query: "query",
        };

        return (
                <div>
                <form className="card-panel" onSubmit={this.handleSearchClick}>
                    <div className="input-field">
                        <input
                            id="search"
                            type="text"
                            value={this.state.query}
                            onChange={this.handleChange}
                        />
                    </div>
                    <button className="btn waves-effect btn-full" type="submit">
                        {texts.button}
                    </button>
                </form>

                </div>
        );
    }

    private handleChange(event) {
        this.setState({query: event.target.value});
    }

    private handleSearchClick(event) {
        event.preventDefault();
        this.store.dispatch(getSearchResult(
            this.props.bookshelfPk,
            this.state.query,
        ));
    }

}
