import * as React from "react";

import { BookType } from "blea/entities/book";

interface ISearchResultProps {
    bookType: BookType;
    key: number;
    result: any;
    onClickSearchResult: any;
};

export class SearchResultLine extends React.Component<ISearchResultProps, undefined> {

    constructor(props) {
        super(props);
    }

    public render(): React.ReactElement<{}>  {
        return (
                <li className="collection-item" key={this.props.key}><span onClick={ () => this.props.onClickSearchResult(Number( this.props.bookType === BookType.Epub ? this.props.result.first_word_id : this.props.result.page ))} dangerouslySetInnerHTML={{ __html: this.props.result.context}}></span></li>
        );
    }
}
