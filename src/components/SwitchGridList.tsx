import * as React from "react";
import { Store } from "redux";

import { lazyInject } from "blea/renderer/di";

import { IAppState } from "blea/renderer/reducers";

import { Bookshelf } from "blea/entities/bookshelf";

import { setLayout } from "blea/actions/layout";

interface ISwitchGridListState {
    bookshelf: Bookshelf;
    layout: string;
}

export default class SwitchGridList extends React.Component<undefined, ISwitchGridListState> {
    public state: ISwitchGridListState;

    private unSubscriber;

    @lazyInject("store")
    private store: Store<IAppState>;

    constructor() {
        super();
        this.state = {
            bookshelf: this.store.getState().bookshelf,
            layout: this.store.getState().layout,
        };

        this.handleLayoutChange = this.handleLayoutChange.bind(this);
    }

    public componentDidMount() {
        this.unSubscriber = this.store.subscribe(() => {
            this.setState({
                bookshelf: this.store.getState().bookshelf,
                layout: this.store.getState().layout,
            });
        });
    }

    public componentWillUnmount() {
        this.unSubscriber();
    }

    public render(): React.ReactElement<{}>  {

        if (!this.state.bookshelf.books) {
            return (
                null
            );
        } else {
            return (
                <div className="switch">
                    <label>
                        List
                        <input
                            type="checkbox"
                            defaultChecked={this.state.layout === "grid"}
                            onChange={this.handleLayoutChange}
                        />
                        <span className="lever"></span>
                        Grid
                    </label>
                </div>
            );
        }
    }

    private handleLayoutChange(event: any) {
        this.store.dispatch(setLayout(event.target.checked));
    }
}
