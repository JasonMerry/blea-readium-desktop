import { injectable} from "inversify";
import * as path from "path";
import { Store } from "redux";
import { tmpNameSync } from "tmp";
import { URL } from "url";
import * as uuid from "uuid";

import { Download, DownloadParams } from "blea/entities/download";
import { DownloadStatus } from "blea/entities/download";
import * as downloadActions from "blea/main/actions/downloader";

@injectable()
export class Downloader {
    // Path where files are downloaded
    private dstRepositoryPath: string;

    // Redux store
    private store: Store<any>;

    public constructor(dstRepositoryPath: string, store: Store<any>) {
        this.dstRepositoryPath = dstRepositoryPath;
        this.store = store;
    }

    public download(downloadParams: DownloadParams): Download {
        // Get extension from url
        const urlObj = new URL(downloadParams.url);
        const ext = path.extname(urlObj.pathname);

        // Create temporary file as destination file
        const dstPath = tmpNameSync(
            {
                dir: this.dstRepositoryPath,
                postfix: `${ext}.part`,
                prefix: "blea-",
            }
        );

        // Create download
        let download: Download = {
            dstPath,
            identifier: uuid.v4(),
            progress: 0,
            downloadParams,
            status: DownloadStatus.Init,
        };

        // Append to download queue
        this.store.dispatch(downloadActions.add(download));

        return download;
    }
}
