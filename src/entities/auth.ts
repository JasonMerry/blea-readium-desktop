export interface Auth {
    loggedIn: boolean;
}

export interface AuthLogin {
    password: string;
    username: string;
};

export interface UserAuthState {
    key?: string;
    loggedIn: boolean;
    token?: string;
    user?: number;
};

export interface AuthState {
    currentlySending: boolean;
    errorStatus: number;
    errorMessage: string;
    formState: AuthLogin;
    userState: UserAuthState;
};
