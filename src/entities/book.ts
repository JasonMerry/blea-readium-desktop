import { Downloadable } from "blea/entities/download";

export enum BookType {
    Epub,
    Pdf
}

export interface BookContent {
    url: string;
    ready: boolean;
    processed: boolean;
    size: number;
}

export interface SpineBrickMap {
    idref: string;
    first: number;
    last: number;
}

export interface Bookmark {
    id?: number;
    wordId?: number;
    words?: string;
    page?: number;
}

export interface Book {
    bookshelfPk: number;
    isbn13: string;
    download: Downloadable;
    currentPage?: number;
    order?: number;
    author?: string;
    cover?: string;
    title?: string;
    content?: BookContent;
    bookType?: BookType;
    spineBrickMap?: SpineBrickMap[];
    lastWordId?: number;
    bookmarks?: Bookmark[];
    pdfpages?: number;
}

export function mapContentType(contentType: string): BookType {
    switch (contentType) {
    case "application/epub+glb":
        return BookType.Epub;
    case "application/pdf+glb":
        return BookType.Pdf;
    case "application/epub+zip":
        return BookType.Epub;
    case "application/pdf+zip":
        return BookType.Pdf;
    default:
        return null;
    }
}
