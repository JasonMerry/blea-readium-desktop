
export interface BookStats {
    bookmarks: number;
    highlights: number;
    notes: number;
    shares: number;
}

export interface Bookshelf {
    id?: number;
    name?: string;
    books?: any;
    user?: number;
}
