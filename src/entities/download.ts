export enum DownloadStatus {
    Init, // Initial state
    Downloading, // File is downloading
    Canceled, // File download has been cancelled
    Failed, // File download has failed
    Downloaded, // File has been downloaded
}

export interface Downloadable {
    status: DownloadStatus;
    progress: number;
    contentType?: string;
}

export interface Identifiable {
    identifier: string;
}

export interface DownloadParams {
    url: string;
    headers: any;
    qs: any;
}

export interface Download extends Downloadable, Identifiable {
    // parameters of the source file to download
    downloadParams: DownloadParams;
    // Url of the downloaded
    dstPath: string;
    error?: string;
    contentType?: string;
}
