import { AuthLogin } from "blea/entities/auth";

export interface AuthLoginMessage {
    login: AuthLogin;
};
