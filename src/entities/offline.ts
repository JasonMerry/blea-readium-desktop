
export interface ApiItem {
    method: string;
    data: any;
    url: string;
    params?: any;
}
