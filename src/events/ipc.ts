// The renderer process sends a LoginMessage to the main process
// The main process requests login from the server
export const AUTH_LOGIN_REQUEST = "auth.login.request";

export const AUTH_LOGOUT_REQUEST = "auth.logout.request";

// The main process sends a login response to the renderer process
export const AUTH_LOGIN_RESPONSE = "auth.login.response";

// sub-types of the LOGIN_RESPONSE
export const AUTH_SENDING_REQUEST = "auth.sending.request";
export const AUTH_REQUEST_FINISHED = "auth.request.finished";

export const AUTH_REQUEST_ERROR = "auth.request.error";
export const AUTH_COMPLETE = "auth.complete";
export const AUTH_LOGOUT_COMPLETE = "auth.logout.complete";

export const BOOKSHELF_FETCH_REQUEST = "bookshelf.fetch.request";

export const SYNC_BOOKSHELF_RESPONSE = "sync.bookshelf.response";
export const SYNC_BOOKSHELF_REQUEST = "sync.bookshelf.request";

export const BOOK_DOWNLOAD_REQUEST = "book.download.request";
export const BOOK_DOWNLOAD_RESPONSE = "book.download.response";

export const BOOK_INDEX_REQUEST = "book.index.request";
export const BOOK_LAST_PAGE_REQUEST = "book.last.page.request";
export const BOOK_LAST_PAGE_RESPONSE = "book.last.page.response";
export const BOOK_HIGHLIGHT_REQUEST = "book.highlight.request";
export const BOOK_HIGHLIGHT_RESPONSE = "book.highlight.response";
export const BOOK_BOOKMARK_ADD_REQUEST = "book.bookmark.add.request";
export const BOOK_BOOKMARK_ADD_RESPONSE = "book.bookmark.add.response";
export const BOOK_BOOKMARK_DELETE_REQUEST = "book.bookmark.delete.request";
export const BOOK_BOOKMARKS_GET_REQUEST = "book.bookmarks.get.request";
export const BOOK_BOOKMARKS_GET_RESPONSE = "book.bookmarks.get.response";
export const BOOK_HIGHLIGHT_ADD_REQUEST = "book.highlight.add.request";
export const BOOK_HIGHLIGHT_DELETE_REQUEST = "book.highlight.delete.request";
export const BOOK_SEARCH_GET_REQUEST = "book.search.get.request";
export const BOOK_SEARCH_GET_RESPONSE = "book.search.get.response";
