import * as fs from "fs";
import * as mime from "mime";
import * as path from "path";

import { Store } from "redux";

import { app, BrowserWindow, ipcMain, protocol } from "electron";

const pjson = require("../package.json");

import { container } from "blea/main/di";
import { AppState } from "blea/main/reducers";
import { IDecrypt } from "blea/reader/epubdecipher";
import { IDecryptPdf } from "blea/reader/pdfdecipher";

import * as queueActions from "blea/main/actions/offline";

import TYPES from "blea/types";

import installExtension, { REACT_DEVELOPER_TOOLS, REDUX_DEVTOOLS } from 'electron-devtools-installer';

import { getAppFolderMain } from "blea/resources/device";

// Preprocessing directive
declare var __RENDERER_BASE_URL__: string;

// Global reference to the main window,
// so the garbage collector doesn't close it.
let mainWindow: Electron.BrowserWindow = null;

app.setName(pjson.productName || "bwl");
const appDataPath = app.getPath("appData");

// Opens the main window, with a native menu bar.
function createWindow() {

    ipcMain.on("asynchronous-message", (event, arg) => {
        console.log(arg);
        event.sender.send("asynchronous-reply", arg + "__PONG");
    });

    ipcMain.on("get_server_url", (event, arg) => {
        event.returnValue = pjson.config.serverUrl;
    });

    ipcMain.on("get_app_folder", (event, arg) => {
        event.returnValue = appDataPath;
    });

    mainWindow = new BrowserWindow({
        height: 1000,
        icon: path.join(__dirname, "/src/assets/img/icon.png"),
        title: pjson.productName || "bwl",
        width: 1220
    });
    let rendererBaseUrl = __RENDERER_BASE_URL__;

    if (rendererBaseUrl === "file://") {
        // This is a local url
        rendererBaseUrl += path.normalize(path.join(__dirname, "index.html"));
    } else {
        // This is a remote url
        rendererBaseUrl += "index.html";
    }

    mainWindow.loadURL(rendererBaseUrl);
    mainWindow.webContents.openDevTools();

    installExtension(REDUX_DEVTOOLS);
    installExtension(REACT_DEVELOPER_TOOLS);

    mainWindow.on("closed", () => {
        mainWindow = null;
    });
}

function startOfflinePolling() {
    const store: Store<AppState> = container.get("store") as Store<AppState>;
    store.dispatch(queueActions.clear());
}

function registerBufferIntercept() {
    const epubDecryptInst = container.get<IDecrypt>(TYPES.IDecrypt);
    const pdfDecryptPdfInst = container.get<IDecryptPdf>(TYPES.IDecryptPdf);

    const store: Store<AppState> = container.get("store") as Store<AppState>;
    const appDataSpilt = appDataPath.split(path.sep).concat("bwl");

    protocol.interceptBufferProtocol(
        "file",
        (req: any, cb: any) => {
            let reqPath = path.normalize(req.url.substr(7));
            reqPath = reqPath.replace("%20", " ");
            try {
                let file;
                const pathSplit = reqPath.split(path.sep);
                let pathMatches: boolean = appDataSpilt.every((e, i) => e === pathSplit[i]);
                // are we in the users epub/pdf directory
                if (pathMatches) {
                    const secret = store.getState().auth.userState.key;
                    // is it a pdf
                    if (pathSplit[pathSplit.length - 1] === "content.pdf")
                    {
                            pdfDecryptPdfInst.decryptFile(reqPath, secret).then(
                            (result) => {
                                cb({ mimeType: mime.lookup(reqPath), data: new Buffer(result) });
                            },
                            (error) => { console.log("error", error); }
                        );
                        return;

                    }
                    if (epubDecryptInst.shouldDecrypt(reqPath)) {
                        epubDecryptInst.decryptFile(reqPath, secret).then(
                            (result) => {
                                cb({ mimeType: mime.lookup(reqPath), data: new Buffer(result) });
                            },
                            (error) => { console.log("error", error); }
                        );
                        return;
                    }
                }
                file = fs.readFileSync(reqPath);

                cb({ mimeType: mime.lookup(reqPath), data: file });
            } catch (err) {
                console.log(err);
                cb({ mimeType: "", data: null });
            }
        },
        (err) => {
            if (err) {
                console.error("Failed to register epub decrypt protocol");
            } else {
                console.log("Successfully registered epub decrypt protocol");
            }
        }
    );

}

// Quit when all windows are closed.
app.on(
    "window-all-closed", () => {
        if (process.platform !== "darwin") {
            app.quit();
        }
    },
);

// Call 'createWindow()' on startup.
app.on("ready", () => {
    registerBufferIntercept();
    createWindow();
    startOfflinePolling();
});

// On OS X it's common to re-create a window in the app when the dock icon is clicked and there are no other
// windows open.
app.on("activate", () => {
    if (mainWindow === null) {
        createWindow();
    }
});
