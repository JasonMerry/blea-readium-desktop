import { Action } from "redux";

import { Download } from "blea/entities/download";

export const DOWNLOAD_ADD = "DOWNLOAD_ADD";
export const DOWNLOAD_REMOVE = "DOWNLOAD_REMOVE";
export const DOWNLOAD_START = "DOWNLOAD_START";
export const DOWNLOAD_PROGRESS = "DOWNLOAD_PROGRESS";
export const DOWNLOAD_CANCEL = "DOWNLOAD_CANCEL";
export const DOWNLOAD_FAIL = "DOWNLOAD_FAIL";
export const DOWNLOAD_FINISH = "DOWNLOAD_FINISH";

export interface DownloadAction extends Action {
    download: Download;
}

export function add(download: Download): DownloadAction {
    return {
        type: DOWNLOAD_ADD,
        download,
    };
}

export function remove(download: Download): DownloadAction {
    return {
        type: DOWNLOAD_REMOVE,
        download,
    };
}

export function cancel(download: Download): DownloadAction {
    return {
        type: DOWNLOAD_CANCEL,
        download,
    };
}

export function start(download: Download): DownloadAction {
    return {
        type: DOWNLOAD_START,
        download,
    };
}

export function progress(download: Download, progress: number): DownloadAction {
    download.progress = progress;
    return {
        type: DOWNLOAD_PROGRESS,
        download,
    };
}

export function finish(download: Download): DownloadAction {
    return {
        type: DOWNLOAD_FINISH,
        download,
    };
}

export function fail(download: Download, error: string): DownloadAction {
    download.error = error;
    return {
        type: DOWNLOAD_FAIL,
        download,
    };
}
