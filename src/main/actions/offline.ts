import { Action } from "redux";

import { ApiItem } from "blea/entities/offline";

export const QUEUE_ADD = "QUEUE_ADD";
export const QUEUE_CLEAR = "QUEUE_CLEAR";
export const QUEUE_REMOVE_FIRST_ITEM = "QUEUE_REMOVE_FIRST_ITEM";

export interface QueueAction extends Action {
    queueItem?: ApiItem;
}

export function add(queueItem: ApiItem): QueueAction {
    return {
        type: QUEUE_ADD,
        queueItem: queueItem,
    };
}

export function removeFirst(): QueueAction {
    return {
        type: QUEUE_REMOVE_FIRST_ITEM,
    };
}

export function clear(): Action {
    return {
        type: QUEUE_CLEAR,
    };
}
