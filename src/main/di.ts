import "reflect-metadata";

import { Store } from "redux";

import { app } from "electron";

import { Container } from "inversify";
import getDecorators from "inversify-inject-decorators";

import { store } from "blea/main/store/memory";

import { AppState } from "blea/main/reducers";

import { IDecrypt } from  "blea/reader/epubdecipher";
import { BlackwellEpubDecrypt } from  "blea/reader/epubdecipher_blackwell";

import { IDecryptPdf } from  "blea/reader/pdfdecipher";
import { BlackwellPdfDecrypt } from  "blea/reader/pdfdecipher_blackwell";

import { IDecompress } from "blea/reader/decompress";
import { BlackwellEpubDecompress } from "blea/reader/decompress_blackwell";

import { IAuthApi } from "blea/api/auth";
import { BlackwellAuthApi } from "blea/api/auth_blackwell";

import { BookshelfApi } from "blea/api/bookshelf";
import { BlackwellBookshelfApi } from "blea/api/bookshelf_blackwell";

import { Spine } from "blea/reader/spine";
import { BlackwellSpine } from "blea/reader/spine_blackwell";

import { Downloader } from "blea/downloader/downloader";

import TYPES from "blea/types";

let container = new Container();

let appTempPath = app.getPath("temp");

container.bind<IDecrypt>(TYPES.IDecrypt).to(BlackwellEpubDecrypt);
container.bind<IDecryptPdf>(TYPES.IDecryptPdf).to(BlackwellPdfDecrypt);
container.bind<IAuthApi>(TYPES.IAuthApi).to(BlackwellAuthApi);
container.bind<BookshelfApi>(TYPES.BookshelfApi).to(BlackwellBookshelfApi);
container.bind<IDecompress>(TYPES.IDecompress).to(BlackwellEpubDecompress);
container.bind<Store<AppState>>("store").toConstantValue(store);
container.bind<Downloader>("downloader").toConstantValue(
    new Downloader(appTempPath, store),
);
container.bind<Spine>(TYPES.Spine).to(BlackwellSpine);

let {
    lazyInject,
    lazyInjectNamed,
    lazyInjectTagged,
    lazyMultiInject,
} = getDecorators(container);

export {
    container,
    lazyInject,
    lazyInjectNamed,
    lazyInjectTagged,
    lazyMultiInject,
};
