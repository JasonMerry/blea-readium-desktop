import * as update from 'immutability-helper';

import {
    BOOK_DOWNLOAD_ADD,
    BOOK_DOWNLOAD_START,
    BookDownloadAction,
} from "blea/actions/book-download";

import { Download } from "blea/entities/download";
import { Book } from "blea/entities/book";

export interface BookDownloadState {
    // Download identifier => Book identifier
    downloadIdentifierToBook: { [identifier: string]: Book };
    // Book identifiers => Download identifiers
    bookIdentifierToDownloads: { [identifier: string]: Download[] };
}

const initialState: BookDownloadState = {
    downloadIdentifierToBook: {},
    bookIdentifierToDownloads: {},
};

export function bookDownloadReducer(
    state: BookDownloadState = initialState,
    action: BookDownloadAction,
    ): BookDownloadState {

    switch (action.type) {
    case BOOK_DOWNLOAD_ADD:
        return state;
    case BOOK_DOWNLOAD_START:
        const book = action.book;
        const downloads = action.downloads;
        let downloadObj = {};
        for (const download of downloads) {
            downloadObj[download.identifier] = book;
        }
        const newState = update(state, {downloadIdentifierToBook: {$merge: downloadObj}});
        const newState1 = update(newState, {bookIdentifierToDownloads: {[book.bookshelfPk]: {$set: downloads}}});
        return newState1;
    default:
        return state;
    }
}
