import * as update from 'immutability-helper';

import { DownloadAction } from "blea/main/actions/downloader";
import {
    DOWNLOAD_ADD,
    DOWNLOAD_CANCEL,
    DOWNLOAD_FAIL,
    DOWNLOAD_FINISH,
    DOWNLOAD_PROGRESS,
    DOWNLOAD_REMOVE,
    DOWNLOAD_START,
} from "blea/main/actions/downloader";
import { Download } from "blea/entities/download";
import { DownloadStatus } from "blea/entities/download";

export interface DownloaderState {
    downloads: { [identifier: string]: Download };
}

const initialState: DownloaderState = {
    downloads: {},
};

export function downloader(
    state: DownloaderState = initialState,
    action: DownloadAction,
    ): DownloaderState {
    if (action.download === undefined) {
        return state;
    }
    let identifier = action.download.identifier;

        switch (action.type) {
        case DOWNLOAD_ADD:
            const newState = update(state, {downloads: {[identifier]: {$set: action.download}}});
            return newState;
        case DOWNLOAD_REMOVE:
            const newStateRem = update(state, {downloads: {$unset: [identifier]}});
            return newStateRem;
        case DOWNLOAD_CANCEL:
            let newDownload = {...action.download, status: DownloadStatus.Canceled};
            const newStateCancel = update(state, {downloads: {[identifier]: {$set: newDownload}}});
            return newStateCancel;
        case DOWNLOAD_START:
            let newDownloadStart = {...action.download, status: DownloadStatus.Downloading};
            const newStateStart = update(state, {downloads: {[identifier]: {$set: newDownloadStart}}});
            return newStateStart;
        case DOWNLOAD_PROGRESS:
            const newStateProgress = update(state, {downloads: {[identifier]: {$set: action.download}}});
            return newStateProgress;
        case DOWNLOAD_FINISH:
            let newDownloadFinished = {...action.download, status: DownloadStatus.Downloaded, progress: 100};
            const newStateFinished = update(state, {downloads: {[identifier]: {$set: newDownloadFinished}}});
            return newStateFinished;
        case DOWNLOAD_FAIL:
            let newDownloadFailed = {...action.download, status: DownloadStatus.Failed, progress: 0};
            const newStateFailed = update(state, {downloads: {[identifier]: {$set: newDownloadFailed}}});
            return newStateFailed;
        default:
            return state;
        }
}
