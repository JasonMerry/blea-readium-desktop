import { combineReducers } from "redux";

import { AuthState } from "blea/entities/auth";
import { auth } from "blea/main/reducers/auth";
import { bookshelf } from "blea/reducers/bookshelf";
import { Bookshelf } from "blea/entities/bookshelf";
import { downloader } from "blea/main/reducers/downloader";
import { DownloaderState } from "blea/main/reducers/downloader";
import { queue, QueueState } from "blea/main/reducers/offline";

import {
    BookDownloadState,
    bookDownloadReducer,
} from "blea/main/reducers/book-download";

export interface AppState {
    auth: AuthState;
    bookshelf: Bookshelf;
    downloader: DownloaderState;
    bookDownloads: BookDownloadState;
    queue: QueueState;
}

export const rootReducer = combineReducers({
    auth,
    bookshelf,
    downloader,
    bookDownloads: bookDownloadReducer,
    queue,
});
