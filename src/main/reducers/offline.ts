import * as queueActions from "blea/main/actions/offline";

import { ApiItem } from "blea/entities/offline";

import * as update from 'immutability-helper';

export interface QueueState {
    items: ApiItem[];
}

const initialState: QueueState = {
    items: []
};

export function queue(
    state: QueueState = initialState,
    action: queueActions.QueueAction,
): QueueState {
    switch (action.type) {
    case queueActions.QUEUE_ADD:
        const newState = update(state, {items: {$push: [action.queueItem]}});
        return newState
    case queueActions.QUEUE_REMOVE_FIRST_ITEM:
        const newRemoveState = update(state, {items: {$splice: [[0, 1]]}});
        return newRemoveState;
    default:
        return state;
    }
};
