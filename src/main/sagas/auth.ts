import { BrowserWindow, ipcMain } from "electron";
import { Buffer, buffers, channel, Channel, SagaIterator } from "redux-saga";
import { actionChannel, call, fork, put, race, select, take } from "redux-saga/effects";

import { IAuthApi } from "blea/api/auth";

import { container } from "blea/main/di";
import TYPES from "blea/types";

import {
    changeForm,
    LOGIN_REQUEST,
    LOGOUT,
    loginRequest,
    logoutRequest,
    REQUEST_ERROR,
    sendingRequest,
    setAuthState,
} from "blea/actions/auth";

import { fetchBookshelf, removeBookshelf, updatedBookshelf } from "blea/actions/bookshelf";

import {
    AUTH_COMPLETE,
    AUTH_LOGIN_REQUEST,
    AUTH_LOGIN_RESPONSE,
    AUTH_LOGOUT_REQUEST,
    AUTH_LOGOUT_COMPLETE,
    AUTH_REQUEST_ERROR,
    AUTH_REQUEST_FINISHED,
    AUTH_SENDING_REQUEST,
} from "blea/events/ipc";

import { AuthLogin } from "blea/entities/auth";
import { Error } from "blea/entities/error";

enum AuthResponseType {
    Error, // Login error
    Start, // Start Login
    Cancel, // Cancel Login
    Logout, // Logout
}

interface AuthResponse {
    type: AuthResponseType;
    error?: Error;
    login?: AuthLogin;
}

export const getUserAuthState: any = (state: any ) => state.auth.userState;

export function * registerDevice (token: string) {
    // We send an action that tells Redux we're sending a request
    yield put(sendingRequest(true));

    try {
        const authInst = container.get<IAuthApi>(TYPES.IAuthApi);
        // TODO: get the device name
        const response = yield call(authInst.registerDevice, token, "some device name");
        return response;
    } catch (error) {
        // If we get an error we send Redux the appropiate action and return
        console.log("- - - - - ERROR to send to redux:", error);
        yield put({type: REQUEST_ERROR, status: error.response.status, message: error.message});
        yield put(logoutRequest());

        return false;
    } finally {
        // When done, we tell Redux we're not in the middle of a request any more
        yield put(sendingRequest(false));
    }
}

export function * authorize (data: AuthLogin) {
    // TODO: We send an action that tells Redux we're sending a request - send to renderers
    yield put(sendingRequest(true));
    yield call(sendRenderersAuthMessage, {type: AUTH_SENDING_REQUEST});

    console.log("in authorize",  data);
    // We then try to register or log in the user, depending on the request
    try {
        const authInst = container.get<IAuthApi>(TYPES.IAuthApi);
        let response;
        // For either log in or registering, we call the proper function in the `authInst`
        // module, which is asynchronous. Because we're using generators, we can work
        // as if it's synchronous because we pause execution until the call is done
        // with `yield`!
        response = yield call(authInst.login, data.username, data.password);
        console.log("response from server", response);

        let token: string = response.token;
        yield put(setAuthState({loggedIn: false, token}));

        let regRes = yield call(registerDevice, token);
        if (regRes) {
            yield put(
                setAuthState(
                    {
                        key: regRes.key,
                        loggedIn: regRes.loggedIn,
                        token,
                        user: regRes.user,
                    }
                )
            ); // User is logged in (authorized)
            yield put(changeForm({username: "", password: ""})); // Clear form
            yield call(sendRenderersAuthMessage, {type: AUTH_COMPLETE, content: {loggedIn: true}});
            yield put(fetchBookshelf());
        }

        return response;
    } catch (error) {
        console.log("error from server", error.response.status);
        // If we get an error we send Redux the appropiate action and return
        yield put({type: REQUEST_ERROR, status: error.response.status, message: error.message});
        yield call(sendRenderersAuthMessage, {type: AUTH_REQUEST_ERROR, status: error.response.status, message: error.message});
        yield call(sendRenderersAuthMessage, {type: AUTH_REQUEST_FINISHED});
        yield put(sendingRequest(false));
        return false;
    } finally {
        // When done, we tell Redux we're not in the middle of a request any more
        yield call(sendRenderersAuthMessage, {type: AUTH_REQUEST_FINISHED});
        yield put(sendingRequest(false));
    }
}

function * logout () {
    // We tell Redux we're in the middle of a request
    yield put(sendingRequest(true));
    yield call(sendRenderersAuthMessage, {type: AUTH_SENDING_REQUEST});

    try {
        const authInst = container.get<IAuthApi>(TYPES.IAuthApi);
        const userAuthState = yield select(getUserAuthState);
        let response = yield call(authInst.logout, userAuthState.token);
        yield put(
            setAuthState(
                {
                    key: null,
                    loggedIn: response.loggedIn,
                    token: null,
                    user: null
                }
            )
        );
        yield call(sendRenderersAuthMessage, {type: AUTH_LOGOUT_COMPLETE, content: {loggedIn: response.loggedIn}});
        return response;
    } catch (error) {
        yield put({type: REQUEST_ERROR, error: error.message});

    } finally {
        yield put(sendingRequest(false));
        yield call(sendRenderersAuthMessage, {type: AUTH_REQUEST_FINISHED});
        yield put(removeBookshelf());
        yield put(updatedBookshelf());
    }
}


export function* watchAuthLoginUpdate(): SagaIterator {
    let buffer: Buffer<any> = buffers.expanding(20);
    let chan = yield actionChannel([
        LOGIN_REQUEST,
        LOGOUT
        ], buffer);
    while (true) {
        const action: any = yield take(chan);

        switch (action.type) {
        case LOGIN_REQUEST:
            console.log("### login request", action);
            yield fork(authorize, {username: action.data.username, password: action.data.password});
            break;
        case LOGOUT:
            console.log("### logout request", action);
            yield fork(logout);
            break;
        default:
            break;
        }

        // yield put(catalogActions.updatePublication(
        //     publication,
        // ));
    }
}

// watch for ipc AUTH_LOGIN_REQUEST event from a renderer
// add action to AuthResponse Channel set up in watchRendererAuthLoginRequest
function waitForAuthLoginRequest(
    chan: Channel<AuthResponse>,
) {
    // Wait for catalog request from a renderer process
    ipcMain.on(
        AUTH_LOGIN_REQUEST,
        (event: any, msg: any) => {
            chan.put({
                login: msg.login,
                type: AuthResponseType.Start
            });
        },
    );
}

function waitForAuthLogoutRequest(
    chan: Channel<AuthResponse>,
) {
    // Wait for catalog request from a renderer process
    ipcMain.on(
        AUTH_LOGOUT_REQUEST,
        (event: any, msg: any) => {
            chan.put({
                type: AuthResponseType.Logout
            });
        },
    );
}

// create AuthResponse channel
// fork waitForAuthLoginRequest to wait for ipc renderer event
export function* watchRendererAuthLoginRequest(): SagaIterator {
    const chan = yield call(channel);

    yield fork(waitForAuthLoginRequest, chan);
    yield fork(waitForAuthLogoutRequest, chan);

    while (true) {
        const response: AuthResponse = yield take(chan);

        switch (response.type) {
        case AuthResponseType.Start:
            yield put(loginRequest(response.login));
            break;
        case AuthResponseType.Logout:
            yield put(logoutRequest());
            break;
        default:
            break;
        }
    }
}

function sendRenderersAuthMessage(authMessage) {
    let windows = BrowserWindow.getAllWindows();
    for (let window of windows) {
        window.webContents.send(AUTH_LOGIN_RESPONSE, authMessage);
    }
}
