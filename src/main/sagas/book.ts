import "reflect-metadata";

import * as path from "path";

import { AppState } from "blea/main/reducers";
import { Store } from "redux";
import { IDecrypt } from "blea/reader/epubdecipher";
import { IDecryptPdf } from "blea/reader/pdfdecipher";

import { BrowserWindow, ipcMain } from "electron";
import { Buffer, buffers, channel, delay, Channel, SagaIterator } from "redux-saga";
import { actionChannel, apply, call, fork, put, race, select, take } from "redux-saga/effects";

import { getServerMain } from "blea/resources/server";

import { Downloader } from "blea/downloader/downloader";

import {
    BOOK_BOOKMARK_ADD_REQUEST,
    BOOK_BOOKMARK_DELETE_REQUEST,
    BOOK_BOOKMARKS_GET_REQUEST,
    BOOK_BOOKMARKS_GET_RESPONSE,
    BOOK_DOWNLOAD_REQUEST,
    BOOK_DOWNLOAD_RESPONSE,
    BOOK_INDEX_REQUEST,
    BOOK_LAST_PAGE_REQUEST,
    BOOK_LAST_PAGE_RESPONSE,
    BOOK_HIGHLIGHT_ADD_REQUEST,
    BOOK_HIGHLIGHT_DELETE_REQUEST,
    BOOK_HIGHLIGHT_REQUEST,
    BOOK_HIGHLIGHT_RESPONSE,
    BOOK_SEARCH_GET_REQUEST,
    BOOK_SEARCH_GET_RESPONSE,
} from "blea/events/ipc";

import * as bookDownloadActions from "blea/actions/book-download";

import * as bookshelfActions from "blea/actions/bookshelf";
import * as bookActions from "blea/actions/book";
import * as queueActions from "blea/main/actions/offline";

import {
    DOWNLOAD_FINISH,
    DOWNLOAD_PROGRESS,
    DownloadAction,
} from "blea/main/actions/downloader";

import { BookshelfApi } from "blea/api/bookshelf";
import { IDecompress } from "blea/reader/decompress";

import { Spine } from "blea/reader/spine";

import { container } from "blea/main/di";
import { Download, DownloadParams, DownloadStatus } from "blea/entities/download";
import { Book, Bookmark, BookType, mapContentType, SpineBrickMap } from "blea/entities/book";

import TYPES from "blea/types";

import { bookDownloadPath } from "blea/resources/device";

import { Error } from "blea/entities/error";
import { ApiItem } from "blea/entities/offline";

const SERVER_POST_TIMEOUT = 10000; // 10 seconds

export const getUserAuthState: any = (state: any ) => state.auth.userState;
const getBookDownloads: any = (state: any, bookshelfPk: number) => state.bookDownloads.bookIdentifierToDownloads[bookshelfPk];

interface BookResponse {
    type: BookResponseType;
    book: Book;
    error?: Error;
    renderer?: any;
    wordId?: number;
    pageNumber?: number;
    highlight?: any;
    bookmarkId?: number;
    highlightId?: number;
}

enum BookResponseType {
    Error,   // Error in download
    Start,   // Start Download
    Cancel,  // Cancel Download
    Index,   // Index book
    Sync,    // get last page read
    Highlights,    // get book highlights and notes
    AddHighlight,  // add a highlight
    AddBookmark,   // add a bookmark
    DeleteBookmark, // delete a bookmark
    DeleteHighlight,  // delete highlight
    GetBookmarks,  // get bookmarks
}

interface BookSearch {
    bookshelfPk: number;
    query: string;
    renderer?: any;
}

function *startBookDownload(book: Book): SagaIterator {
    const bookshelfInst = container.get<BookshelfApi>(TYPES.BookshelfApi);
    const downloader: Downloader = container.get("downloader") as Downloader;

    let downloads: Download[] = [];
    const userAuthState = yield select(getUserAuthState);
    const token = userAuthState.token;
    const downloadParams: DownloadParams = bookshelfInst.getDownloadBookParams(token, book);
    let downloadInst: Download = downloader.download(downloadParams);
    downloads.push(downloadInst);
    yield put(bookDownloadActions.start(book, downloads));
}

function *startBookDecompress(book: Book): SagaIterator {
    const decompressInst = container.get<IDecompress>(TYPES.IDecompress);
    // get book path
    const bookDownloads = yield select(getBookDownloads, book.bookshelfPk);

    // seems to be required for some books
    yield call(delay, 1000);

    // there is only one file per Book at the moment - always need decompressing
    for (let bd of bookDownloads) {
        try {
            // if pdf will need to decrypt first
            yield call(decompressInst.decompressFile, bd.dstPath, bookDownloadPath(book.bookshelfPk));
        } catch (error) {
            console.log(error);
        }
    }
}

function * movePdf(book: Book): SagaIterator {
    const decryptPdfInst = container.get<IDecryptPdf>(TYPES.IDecryptPdf);
    const dstPath = path.join(bookDownloadPath(book.bookshelfPk), "content.pdf");
    const bookDownloads = yield select(getBookDownloads, book.bookshelfPk);
    // only one file for pdfs
    yield call(decryptPdfInst.moveFile, bookDownloads[0].dstPath, dstPath);
}

function *startBookIndex(book: Book): SagaIterator {
    // for each text file for the epub find the first and last
    // brickId - store with book data so we can navigate to sync. locations
    const spineInst = container.get<Spine>(TYPES.Spine);
    const epubDecryptInst = container.get<IDecrypt>(TYPES.IDecrypt);
    const store: Store<AppState> = container.get("store") as Store<AppState>;

    const bookRootPath = bookDownloadPath(book.bookshelfPk);
    const secret = store.getState().auth.userState.key;
    const brickIdRe = /<span class="bwlBrick" id="(\d*?)">/g;

    // get book path
    const bookDownloads = yield select(getBookDownloads, book.bookshelfPk);
    try {
        let idToFileMapObj = yield call(spineInst.getEpubSpine, book.bookshelfPk)
        let firstLastIds = [];
        for (let entry of idToFileMapObj.spineItems) {
            let reqPath = path.join(bookRootPath, idToFileMapObj.fileDir, entry.file);
            let res = yield call([epubDecryptInst, "decryptFile"], reqPath, secret);
            let match = brickIdRe.exec(res);
            let ids = [];
            while (match != null) {
                ids.push(match[1]);
                match = brickIdRe.exec(res);
            }
            firstLastIds.push(
                {idref: entry.id, first: ids[0], last: ids.slice(-1)[0]}
            );
        }
        yield put(bookshelfActions.updateBookSpineMap(book, firstLastIds));
    } catch (err) {
        console.log("error", err);
    }
}

function *startBookSyncRequest(book: Book, renderer: any): SagaIterator {
    const bookshelfInst = container.get<BookshelfApi>(TYPES.BookshelfApi);
    const userAuthState = yield select(getUserAuthState);
    const token = userAuthState.token;
    try {
        let response = yield call(bookshelfInst.getLastPage, book.bookshelfPk, token);
        yield fork(sendLastPageResponse, renderer, book, Number(response.word_id));
    } catch (err) {
        console.log(err);
    }
}

function sendLastPageResponse(
    renderer: any,
    book: Book,
    wordId: number,
) {
    console.log('BOOK_LAST_PAGE_RESPONSE', wordId);
    renderer.send(
        BOOK_LAST_PAGE_RESPONSE,
        { book, wordId },
    );
}

function *startBookHighlightsRequest(book: Book, renderer: any): SagaIterator {
    const bookshelfInst = container.get<BookshelfApi>(TYPES.BookshelfApi);
    const userAuthState = yield select(getUserAuthState);
    const token = userAuthState.token;
    try {
        let response = yield call(bookshelfInst.getBookHighlights, book.bookshelfPk, token);
        yield fork(sendBookHighlightsResponse, renderer, book, response);
    } catch (err) {
        console.log(err);
    }
}

function sendBookHighlightsResponse(
    renderer: any,
    book: Book,
    highlights: any,
) {
    renderer.send(
        BOOK_HIGHLIGHT_RESPONSE,
        { book, highlights },
    );
}

function * startBookAddBookmarkRequest(book: Book, wordId: number, pageNumber: number, renderer: any): SagaIterator {
    console.log("startBookAddBookmarkRequest", book);
    const bookshelfInst = container.get<BookshelfApi>(TYPES.BookshelfApi);
    const userAuthState = yield select(getUserAuthState);
    const token = userAuthState.token;
    let sentToServer = false;
    try {
        const {response, timeout} = yield race({
            response: call(bookshelfInst.addBookmark, book.bookshelfPk, wordId, pageNumber, token),
            timeout: call(delay, SERVER_POST_TIMEOUT)
        })
        if (response) {
            // send all bookmarks from the server
            yield call(startBookGetBookmarksRequest, book, renderer);
            sentToServer = true;
        } else {
            console.log("timeout");
        }
    } catch (err) {
        console.log("server error", err);
    } finally {
        if (!sentToServer) {
            // add request to offline queue
            let queueItem: ApiItem = bookshelfInst.getApiCallAddBookmark(book.bookshelfPk, wordId, pageNumber);
            yield put(queueActions.add(queueItem));
        }
    }
}

function * startBookDeleteBookmarkRequest(book: Book, bookmarkId: number, renderer: any): SagaIterator {
    console.log("remove bookmark", bookmarkId);
    const bookshelfInst = container.get<BookshelfApi>(TYPES.BookshelfApi);
    const userAuthState = yield select(getUserAuthState);
    const token = userAuthState.token;
    let sentToServer = false;
    try {
        const {response, timeout} = yield race({
            response: call(bookshelfInst.deleteBookmark, bookmarkId, token),
            timeout: call(delay, SERVER_POST_TIMEOUT)
        })
        if (response) {
            // send all bookmarks from the server
            yield call(startBookGetBookmarksRequest, book, renderer);
            sentToServer = true;
        } else {
            console.log("timeout");
        }
    } catch (err) {
        console.log("server error", err);
    } finally {
        if (!sentToServer) {
            // add request to offline queue
            let queueItem: ApiItem = bookshelfInst.getApiCallDeleteBookmark(bookmarkId);
            yield put(queueActions.add(queueItem));
        }
    }
}

function * startBookAddHighlightRequest(book: Book, highlight: any, renderer: any): SagaIterator {
    const bookshelfInst = container.get<BookshelfApi>(TYPES.BookshelfApi);
    const userAuthState = yield select(getUserAuthState);
    const token = userAuthState.token;
    let sentToServer = false;
    try {
        const {response, timeout} = yield race({
            response: call(bookshelfInst.addBookHighlight, book.bookshelfPk, highlight, token),
            timeout: call(delay, SERVER_POST_TIMEOUT)
        })
        if (response) {
            console.log(response);
            // send all bookmarks from the server
            yield call(startBookHighlightsRequest, book, renderer);
            sentToServer = true;
        } else {
            console.log("timeout");
        }
    } catch (err) {
        console.log("server error", err);
    } finally {
        if (!sentToServer) {
            // add request to offline queue
            let queueItem: ApiItem = bookshelfInst.getApiCallAddBookHighlight(book.bookshelfPk, highlight);
            yield put(queueActions.add(queueItem));
        }
    }
}

function * startBookDeleteHighlightRequest(book: Book, highlightId: any, renderer: any): SagaIterator {
    console.log("remove highlight", highlightId);
    const bookshelfInst = container.get<BookshelfApi>(TYPES.BookshelfApi);
    const userAuthState = yield select(getUserAuthState);
    const token = userAuthState.token;
    let sentToServer = false;
    try {
        const {response, timeout} = yield race({
            response: call(bookshelfInst.deleteHighlight, highlightId, token),
            timeout: call(delay, SERVER_POST_TIMEOUT)
        })
        if (response) {
            // send all bookmarks from the server
            yield call(startBookHighlightsRequest, book, renderer);
            sentToServer = true;
        } else {
            console.log("timeout");
        }
    } catch (err) {
        console.log("server error", err);
    } finally {
        if (!sentToServer) {
            // add request to offline queue
            let queueItem: ApiItem = bookshelfInst.getApiCallDeleteHighlight(highlightId);
            yield put(queueActions.add(queueItem));
        }
    }

}

function sendBookBookmarkResponse(
    renderer: any,
    book: Book,
    bookmarks: Bookmark[],
) {
    renderer.send(
        BOOK_BOOKMARKS_GET_RESPONSE,
        { book, bookmarks },
    );
}

function * startBookGetBookmarksRequest(book: Book, renderer: any): SagaIterator {
    const bookshelfInst = container.get<BookshelfApi>(TYPES.BookshelfApi);
    const userAuthState = yield select(getUserAuthState);
    const token = userAuthState.token;
    try {
        const {response, timeout} = yield race({
            response: call(bookshelfInst.getBookmarks, book.bookshelfPk, token),
            timeout: call(delay, SERVER_POST_TIMEOUT)
        })
        if (response) {
            let allBookmarks = response.map(
                (bookmark) => {
                    return {
                        id: bookmark.id,
                        wordId: bookmark.word_id,
                        words: bookmark.words,
                        page: bookmark.page,
                    }
                }
            );
            // send response to renderer
            yield fork(sendBookBookmarkResponse, renderer, book, allBookmarks);
        } else {
            console.log("got timeout:", timeout);
        }
    } catch (err) {
        console.log(err);
    }
}

function sendBookSearchResponse(
    renderer: any,
    bookshelfPk: number,
    result: any,
) {
    renderer.send(
        BOOK_SEARCH_GET_RESPONSE,
        { bookshelfPk, result },
    );
}

function * startBookSearchRequest(bookshelfPk: number, query: string, renderer: any): SagaIterator {
    const bookshelfInst = container.get<BookshelfApi>(TYPES.BookshelfApi);
    const userAuthState = yield select(getUserAuthState);
    const token = userAuthState.token;
    try {
        const {response, timeout} = yield race({
            response: call(bookshelfInst.searchBook, bookshelfPk, query, token),
            timeout: call(delay, SERVER_POST_TIMEOUT)
        })
        if (response) {
            // send results to renderer
            yield fork(sendBookSearchResponse, renderer, bookshelfPk, response.result);
        } else {
            console.log("got timeout:", timeout);
        }
    } catch (err) {
        console.log(err);
    }
}

export function * watchBookDownloadUpdate(): SagaIterator {
    let buffer: Buffer<any> = buffers.expanding(20);
    let chan = yield actionChannel([
        bookDownloadActions.BOOK_DOWNLOAD_ADD,
        bookDownloadActions.BOOK_DOWNLOAD_PROGRESS,
        bookDownloadActions.BOOK_DOWNLOAD_FINISH,
        bookDownloadActions.BOOK_DOWNLOAD_INDEX,
        ], buffer);
    while (true) {
        const action: any = yield take(chan);
        const book = action.book;
        switch (action.type) {
        case bookDownloadActions.BOOK_DOWNLOAD_ADD:
            yield fork(startBookDownload, action.book);
            break;
        case bookDownloadActions.BOOK_DOWNLOAD_START:
            book.download = {
                progress: 0,
                status: DownloadStatus.Downloading,
            };
            break;
        case bookDownloadActions.BOOK_DOWNLOAD_PROGRESS:
            book.download = {
                progress: action.progress,
                status: DownloadStatus.Downloading,
            };
            break;
        case bookDownloadActions.BOOK_DOWNLOAD_FINISH:
            book.bookType = mapContentType(action.contentType);
            book.download = {
                progress: action.progress,
                status: DownloadStatus.Downloaded,
            };
            if (book.bookType === BookType.Epub) {
                yield fork(startBookDecompress, book);
                yield fork(startBookIndex, book);
            } else {
                yield fork(movePdf, book);
            }
            break;
        case bookDownloadActions.BOOK_DOWNLOAD_INDEX:
            yield call(startBookIndex, book);
            break;
        default:
            break;
        }
        yield put(bookshelfActions.updateBook(
            book,
        ));
    }
}

function waitForBookDownloadRequest(
    chan: Channel<BookResponse>,
) {
    // Wait for catalog request from a renderer process
    ipcMain.on(
        BOOK_DOWNLOAD_REQUEST,
        (event: any, msg: Book) => {
            // TODO: alter to take Cancel
            chan.put({
                type: BookResponseType.Start,
                book: msg
            });
        },
    );
}

export function * watchBookDownloadRequest(): SagaIterator {
    const chan = yield call(channel);

    yield fork(waitForBookDownloadRequest, chan);
    yield fork(waitForBookIndexRequest, chan);
    yield fork(waitForBookLastPageRequest, chan);
    yield fork(waitForBookHighlightsRequest, chan);
    yield fork(waitForBookAddBookmarkRequest, chan);
    yield fork(waitForBookDeleteBookmarkRequest, chan);
    yield fork(waitForBookBookmarksGetRequest, chan);
    yield fork(waitForBookAddHighlightRequest, chan);
    yield fork(waitForBookDeleteHighlightRequest, chan);
    while (true) {
        const response: BookResponse = yield take(chan);
        const renderer = response.renderer;
        switch (response.type) {
        case BookResponseType.Start:
            yield put(bookDownloadActions.add(response.book));
            break;
        case BookResponseType.Index:
            yield put(bookDownloadActions.index(response.book));
            break;
        case BookResponseType.Sync:
            yield call(startBookSyncRequest, response.book, renderer);
            break;
        case BookResponseType.Highlights:
            yield call(startBookHighlightsRequest, response.book, renderer);
            break;
        case BookResponseType.AddHighlight:
            yield call(startBookAddHighlightRequest, response.book, response.highlight, renderer);
            break;
        case BookResponseType.DeleteHighlight:
            yield call(startBookDeleteHighlightRequest, response.book, response.highlightId, renderer);
            break;
        case BookResponseType.AddBookmark:
            yield call(startBookAddBookmarkRequest, response.book, response.wordId, response.pageNumber, renderer);
            break;
        case BookResponseType.DeleteBookmark:
            yield call(startBookDeleteBookmarkRequest, response.book, response.bookmarkId, renderer);
            break;
        case BookResponseType.GetBookmarks:
            yield call(startBookGetBookmarksRequest, response.book, renderer);
            break;
        default:
            break;
        }
    }
}

function waitForBookAddBookmarkRequest(
    chan: Channel<BookResponse>,
) {
    // Wait for catalog request from a renderer process
    ipcMain.on(
        BOOK_BOOKMARK_ADD_REQUEST,
        (event: any, msg: any) => {
            chan.put({
                type: BookResponseType.AddBookmark,
                book: msg.book,
                wordId: msg.wordId,
                pageNumber: msg.page,
                renderer: event.sender,
            });
        },
    );
}

function waitForBookDeleteBookmarkRequest(
    chan: Channel<BookResponse>,
) {
    // Wait for catalog request from a renderer process
    ipcMain.on(
        BOOK_BOOKMARK_DELETE_REQUEST,
        (event: any, msg: any) => {
            chan.put({
                type: BookResponseType.DeleteBookmark,
                book: msg.book,
                bookmarkId: msg.bookmarkId,
                renderer: event.sender,
            });
        },
    );
}

function waitForBookBookmarksGetRequest(
    chan: Channel<BookResponse>,
) {
    // Wait for catalog request from a renderer process
    ipcMain.on(
        BOOK_BOOKMARKS_GET_REQUEST,
        (event: any, msg: any) => {
            chan.put({
                type: BookResponseType.GetBookmarks,
                book: msg.book,
                renderer: event.sender,
            });
        },
    );
}

function waitForBookIndexRequest(
    chan: Channel<BookResponse>,
) {
    // Wait for catalog request from a renderer process
    ipcMain.on(
        BOOK_INDEX_REQUEST,
        (event: any, msg: Book) => {
            chan.put({
                type: BookResponseType.Index,
                book: msg,
            });
        },
    );
}

function waitForBookLastPageRequest(
    chan: Channel<BookResponse>,
) {
    // Wait for catalog request from a renderer process
    ipcMain.on(
        BOOK_LAST_PAGE_REQUEST,
        (event: any, msg: Book) => {
            chan.put({
                type: BookResponseType.Sync,
                book: msg,
                renderer: event.sender,
            });
        },
    );
}

function waitForBookHighlightsRequest(
    chan: Channel<BookResponse>,
) {
    // Wait for catalog request from a renderer process
    ipcMain.on(
        BOOK_HIGHLIGHT_REQUEST,
        (event: any, msg: Book) => {
            chan.put({
                type: BookResponseType.Highlights,
                book: msg,
                renderer: event.sender,
            });
        },
    );
}

function waitForBookAddHighlightRequest(
    chan: Channel<BookResponse>,
) {
    // Wait for catalog request from a renderer process
    ipcMain.on(
        BOOK_HIGHLIGHT_ADD_REQUEST,
        (event: any, msg: any) => {
            chan.put({
                type: BookResponseType.AddHighlight,
                book: msg.book,
                highlight: msg.highlight,
                renderer: event.sender,
            });
        },
    );
}

function waitForBookDeleteHighlightRequest(
    chan: Channel<BookResponse>,
) {
    // Wait for catalog request from a renderer process
    ipcMain.on(
        BOOK_HIGHLIGHT_DELETE_REQUEST,
        (event: any, msg: any) => {
            chan.put({
                type: BookResponseType.DeleteHighlight,
                book: msg.book,
                highlightId: msg.highlightId,
                renderer: event.sender,
            });
        },
    );
}

export function * watchDownloadUpdate(): SagaIterator {
    while (true) {
        const action: DownloadAction = yield take([
            DOWNLOAD_FINISH,
            DOWNLOAD_PROGRESS,
        ]);
        const state: AppState = yield select();
        // Find publication linked to this download
        const download = action.download;
        const book = state.bookDownloads
            .downloadIdentifierToBook[download.identifier];

        const progress = download.progress;
        switch (action.type) {
        case DOWNLOAD_PROGRESS:
            yield put(bookDownloadActions.progress(
                book,
                progress,
            ));
            break;
        case DOWNLOAD_FINISH:
            yield put(bookDownloadActions.finish(
                book,
                download.contentType
            ));
            break;
        default:
            break;
        }
    }
}

function waitForBookSearchRequest(
    chan: Channel<BookSearch>,
) {
    ipcMain.on(
        BOOK_SEARCH_GET_REQUEST,
        (event: any, msg: any) => {
            chan.put({
                bookshelfPk: msg.bookshelfPk,
                query: msg.query,
                renderer: event.sender,
            });
        },
    );
}

export function * watchBookSearchRequest(): SagaIterator {
    const chan = yield call(channel);

    yield fork(waitForBookSearchRequest, chan);
    while (true) {
        const response: BookSearch = yield take(chan);
        const renderer = response.renderer;
        yield call(startBookSearchRequest, response.bookshelfPk, response.query, renderer);
    }
}
