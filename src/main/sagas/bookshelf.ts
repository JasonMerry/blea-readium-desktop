import "reflect-metadata";

import { AppState } from "blea/main/reducers";
import { Store } from "redux";

import { BrowserWindow, ipcMain } from "electron";
import { Buffer, buffers, channel, Channel, delay, SagaIterator } from "redux-saga";
import { actionChannel, call, fork, put, race, select, take } from "redux-saga/effects";

import { getServerMain } from "blea/resources/server";

import { bookDownloadMimetype } from "blea/resources/device";

import {
    fetchBookshelf,
    FETCH_BOOKSHELF,
    FETCH_BOOKSHELF_SUCCESS,
    REMOVE_BOOKSHELF,
    setBookshelf,
    updatedBookshelf,
    UPDATED_BOOKSHELF,
    BOOK_UPDATE,
} from "blea/actions/bookshelf";

import {
    BOOKSHELF_FETCH_REQUEST,
    SYNC_BOOKSHELF_RESPONSE,
    SYNC_BOOKSHELF_REQUEST,
    BOOK_DOWNLOAD_REQUEST,
    BOOK_DOWNLOAD_RESPONSE,
} from "blea/events/ipc";

import { BookshelfApi } from "blea/api/bookshelf";
import { container } from "blea/main/di";

import TYPES from "blea/types";

import { Error } from "blea/entities/error";

import {
    sendingRequest,
    REQUEST_ERROR,
} from "blea/actions/auth";

import { DownloadStatus } from "blea/entities/download";
import { mapContentType } from "blea/entities/book";

export const getUserAuthState: any = (state: any ) => state.auth.userState;

enum BookshelfResponseType {
    Error, // Bookshelf error
    Fetch, // Fetch
    Sync,  // Sync.
}

interface BookshelfResponse {
    type: BookshelfResponseType;
    error?: Error;
}

export function * downloadBookshelf () {
    // get a users bookshelf
    yield put(sendingRequest(true));

    try {
        const userAuthState = yield select(getUserAuthState);
        console.log("userAuthState: ", userAuthState);
        const token = userAuthState.token;
        console.log("container",container);
        const bookshelfInst = container.get<BookshelfApi>(TYPES.BookshelfApi);
        console.log("bookshelfInst", bookshelfInst);

        let response = yield call(bookshelfInst.fetchBookshelf, token);
        if (response && response.items) {

            // transform bookshelf
            let books = {};
            response.items.forEach((item, i) => {
                // merge items we already have
                // check if book is on the system - guess type from
                let mimetype = bookDownloadMimetype(item.pk);
                let download = {status:null};
                let bookType = null;
                if (mimetype !== "") {
                    download = {
                        status: DownloadStatus.Downloaded,
                    };
                    bookType = mapContentType(mimetype);
                }
                books[item.pk] = {
                    content: item.book.content,
                    author: item.book.author,
                    cover: item.book.cover,
                    isbn13: item.book.isbn13,
                    title: item.book.title,
                    order: i,
                    currentPage: item.current_page,
                    bookshelfPk: item.pk,
                    download: download,
                    bookType: bookType,
                };
            });
            response.books = books;
            delete response.items;
        }
        yield put(setBookshelf(response));
        yield put(updatedBookshelf());
    } catch (error) {
        // If we get an error we send Redux the appropiate action and return
        yield put({type: REQUEST_ERROR, errorStatus: error.response.status, errorMessage: error.message});
        return false;
    } finally {
        // When done, we tell Redux we're not in the middle of a request any more
        yield put(sendingRequest(false));
    }
}

function sendBookshelfResponse(renderer: any) {
    // Get catalog
    const store: Store<AppState> = container.get("store") as Store<AppState>;
    console.log("sendBookshelfResponse");
    renderer.send(
        SYNC_BOOKSHELF_RESPONSE,
        {bookshelf: store.getState().bookshelf},
    );
}

function * syncBookshelf() {
    let windows = BrowserWindow.getAllWindows();
    for (let window of windows) {
        sendBookshelfResponse(window.webContents);
    }
}

export function* watchBookshelfUpdate(): SagaIterator {
    let buffer: Buffer<any> = buffers.expanding(20);
    let chan = yield actionChannel([
        FETCH_BOOKSHELF,
        UPDATED_BOOKSHELF,
        BOOK_UPDATE,
        ], buffer);
    while (true) {
        const action: any = yield take(chan);

        switch (action.type) {
        case FETCH_BOOKSHELF:
            yield fork(downloadBookshelf);
            break;
        case UPDATED_BOOKSHELF:
            yield fork(syncBookshelf);
            break;
        case BOOK_UPDATE:
            yield fork(syncBookshelf);
            break;
        default:
            break;
        }

        // Limit the catalog refresh to 1s
        yield call(delay, 1000);

    }
}

function waitForBookshelfFetchRequest(
    chan: Channel<BookshelfResponse>,
) {
    // Wait for catalog request from a renderer process
    ipcMain.on(
        BOOKSHELF_FETCH_REQUEST,
        (event: any, msg: any) => {
            console.log("got ipc req. BOOKSHELF_FETCH_REQUEST");
            chan.put({
                type: BookshelfResponseType.Fetch
            });
        },
    );
}

function waitForBookshelfSyncRequest() {
    // Wait for catalog request from a renderer process
    ipcMain.on(
        SYNC_BOOKSHELF_REQUEST,
        (event: any) => {
            console.log("got ipc req. SYNC_BOOKSHELF_REQUEST");
            sendBookshelfResponse(event.sender);
        },
    );
}

export function * watchRendererBookshelfRequest(): SagaIterator {
    yield fork(waitForBookshelfSyncRequest);
}

// create BookshelfResponse channel
// fork waitForBookshelfFetchRequest to wait for ipc renderer event
export function* watchBookshelfRequest(): SagaIterator {
    const chan = yield call(channel);

    yield fork(waitForBookshelfFetchRequest, chan);

    while (true) {
        const response: BookshelfResponse = yield take(chan);

        switch (response.type) {
        case BookshelfResponseType.Fetch:
            console.log("got BookshelfResponseType.Fetch");
            yield put(fetchBookshelf());
            break;
        default:
            break;
        }
    }
}
