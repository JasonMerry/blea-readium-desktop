import {
    watchDownloadFinish,
    watchDownloadStart,
} from "blea/main/sagas/downloader";

import {
    watchAuthLoginUpdate,
    watchRendererAuthLoginRequest,
} from "blea/main/sagas/auth";

import {
    watchBookshelfUpdate,
    watchBookshelfRequest,
    watchRendererBookshelfRequest,
} from "blea/main/sagas/bookshelf";

import {
    watchBookDownloadRequest,
    watchBookDownloadUpdate,
    watchDownloadUpdate,
    watchBookSearchRequest,
} from "blea/main/sagas/book";

import { watchQueueData } from "blea/main/sagas/offline";

export function * rootSaga() {
    yield [
        watchAuthLoginUpdate(),
        watchRendererAuthLoginRequest(),
        watchBookshelfUpdate(),
        watchBookshelfRequest(),
        watchRendererBookshelfRequest(),
        watchBookDownloadRequest(),
        watchBookDownloadUpdate(),
        watchDownloadFinish(),
        watchDownloadStart(),
        watchDownloadUpdate(),
        watchQueueData(),
        watchBookSearchRequest(),
    ];
}
