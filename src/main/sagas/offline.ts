import { Store } from "redux";

import { AppState } from "blea/main/reducers";

import { container } from "blea/main/di";

import { Buffer, buffers, channel, delay, SagaIterator } from "redux-saga";
import { actionChannel, call, cancel, fork, put, race, take } from "redux-saga/effects";

import * as queueActions from "blea/main/actions/offline";

import { LOGOUT } from "blea/actions/auth";

import { BookshelfApi } from "blea/api/bookshelf";

import TYPES from "blea/types";

// every 30s
const CHECK_DELAY = 30000;

function * checkAndClearQueue() {
    const store: Store<AppState> = container.get("store") as Store<AppState>;

    // check logged in
    if (!store.getState().auth.userState.loggedIn) {
        console.log("not logged in");
        return;
    }
    // check queue not empty
    if (store.getState().queue.items.length == 0) {
        console.log("no items in queue");
        return;
    }
    // check-instance on server
    const bookshelfInst = container.get<BookshelfApi>(TYPES.BookshelfApi);
    try {
        const serverResponseStatus = yield call(bookshelfInst.checkServerAvailable);
        console.log("server available", serverResponseStatus);
        if (serverResponseStatus === 200) {
            // act on queue items
            const token = store.getState().auth.userState.token;
            for (let queueItem of store.getState().queue.items) {
                try {
                    yield call(bookshelfInst.genericApiCall, queueItem, token);
                } catch (err) {
                    console.log(err);
                } finally {
                    // have to remove whatever happens
                    yield put(queueActions.removeFirst());
                }
            }
        }
    } catch (err) {
        console.log("no server");
    }
}

// every CHECK_DELAY ms check offline state
function* pollOffline() {
    try {
        yield call(delay, CHECK_DELAY);
        // yield put(queueActions.clear());
        yield call(checkAndClearQueue);
        yield put(queueActions.clear());
    } catch (error) {
        // cancellation error
        return;
    }
}

export function * watchQueueData(): SagaIterator {
    while (true) {
        yield take(queueActions.QUEUE_CLEAR);
        const {poll, logout} = yield race({
            poll: fork(pollOffline),
            logout: take(LOGOUT)
        });
        // need to get logout cancel working
        // LOGOUT todo a QUEUE_POLL_STOP
    }
}
