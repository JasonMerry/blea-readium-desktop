import {resolve} from "path";
import { app } from "electron";

import NodeStorage from "redux-persist-storage-node";

import logger from "redux-logger";

import { applyMiddleware, compose, createStore, Store } from "redux";
import createSagaMiddleware from "redux-saga";
import { autoRehydrate, persistStore } from "redux-persist";

import { AppState, rootReducer } from "blea/main/reducers";

import { rootSaga } from "blea/main/sagas";

const sagaMiddleware = createSagaMiddleware();
let appDataPath = app.getPath("appData");

export const store: Store<AppState> = createStore(
    rootReducer,
        compose(
            applyMiddleware(sagaMiddleware),
            // applyMiddleware(logger),
            autoRehydrate(),
        )
) as (Store<AppState>);

sagaMiddleware.run(rootSaga);
let persist = persistStore(store, { storage: new NodeStorage(appDataPath + "/bwl/store.json") }, () => {console.log("stored stuff")});
