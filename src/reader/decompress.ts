
export interface IDecompress {
    decompressFile(pathToCompressedFile: String, pathToOutput: string): any;
}
