import { injectable } from "inversify";

import { IDecompress } from "blea/reader/decompress";

const DecompressZip = require("decompress-zip");

@injectable()
export class BlackwellEpubDecompress implements IDecompress {

    public decompressFile(pathToCompressedFile: string, pathToOutput: string): any {
        return new Promise((resolve, reject) => {
            const filePaths = [];

            let unzipper = new DecompressZip(pathToCompressedFile);

            unzipper.on("error", (err) => {
                reject(Error(err));
            });

            unzipper.on("extract", (log) => {
                Object.keys(log).forEach((key) => {
                    filePaths.push(`${pathToOutput}${log[key].deflated}`);
                });
                resolve(filePaths);
            });

            unzipper.extract({
                filter: (file) => {
                    return file.type !== "SymbolicLink";
                },
                path: pathToOutput,
            });

        });
    }
}
