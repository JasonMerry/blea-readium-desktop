export interface IDecrypt {
    shouldDecrypt(filename: string): boolean;
    decryptFile(path: string, key: string): any;
    decryptData(path: string, key: string): any;
}
