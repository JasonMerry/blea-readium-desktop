import { injectable } from "inversify";

import { IDecrypt } from "blea/reader/epubdecipher";

const crypto = require("crypto");
const fs = require("fs-extra");

const ENCRYPTED_EXTENSIONS = ["jpg", "png", "html", "xhtml", "xml", "jpeg", "gif"];
const ENCRYPTED_IMAGE_EXTENSIONS = ["jpg", "png", "jpeg", "gif"];
const ENCRYPTED_TEXT_NEEDS_FIX_EXTENSIONS = ["html", "xhtml"];

function file_checker(filename: string, extensions: any): boolean {
    return (
        filename.indexOf(".") !== -1 &&
        extensions.includes(filename.substr((filename.lastIndexOf(".") + 1)))
    );
}

function file_is_image(filename: string): boolean {
    return file_checker(filename, ENCRYPTED_IMAGE_EXTENSIONS);
}

function file_is_text_needs_fix(filename: string): boolean {
    return file_checker(filename, ENCRYPTED_TEXT_NEEDS_FIX_EXTENSIONS);
}

function should_decrypt_file(filename: string): boolean {
    return (
        filename.indexOf("META-INF") === -1 &&
            filename.indexOf(".") !== -1 &&
            ENCRYPTED_EXTENSIONS.includes(filename.substr((filename.lastIndexOf(".") + 1)))
    );
}

@injectable()
export class BlackwellEpubDecrypt implements IDecrypt {

    private static readonly LENGTH_FILE = 8;
    private static readonly LENGTH_IV = 16;

    public shouldDecrypt(filename: string): boolean {
        return should_decrypt_file(filename);
    }

    // decrypt a file - return decrypted version in promise
    public decryptFile(path: string, key: string): any {
        return new Promise((resolve, reject) => {
            this.decryptData(path, key).then(
                (result) => {
                    try {
                        if (file_is_image(path)) {
                            resolve(result);
                        }
                        const raw = result.toString();
                        let fixed: string;
                        // encryption/decryption leaves extra space at end of files
                        // fix to get rid of them (only matters for xhtml/html files)
                        if (file_is_text_needs_fix(path)) {
                            const endTag = "</html>";
                            const lastIndexHtml = raw.lastIndexOf(endTag);
                            if (lastIndexHtml !== -1) {
                                const closingTagIndex = raw.lastIndexOf(endTag) + endTag.length;
                                fixed = raw.slice(0, closingTagIndex);
                            } else {
                                fixed = raw;
                            }
                        } else {
                            fixed = raw;
                        }
                        resolve(fixed);
                    } catch (err) {
                        reject(Error(err));
                    }
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }

    public decryptData(path: string, key: string): any {
        return new Promise((resolve, reject) => {
            fs.readFile(path, (err, fileBuffer) => {

                if (err) { reject(Error(err)); };

                if (should_decrypt_file(path)) {
                    const dataSize = fileBuffer.readUInt32LE(0, BlackwellEpubDecrypt.LENGTH_FILE);
                    const dataIVBytes = fileBuffer.slice(
                        BlackwellEpubDecrypt.LENGTH_FILE,
                        BlackwellEpubDecrypt.LENGTH_FILE + BlackwellEpubDecrypt.LENGTH_IV
                    );
                    const encryptedBytes =  fileBuffer.slice(
                        BlackwellEpubDecrypt.LENGTH_FILE + BlackwellEpubDecrypt.LENGTH_IV
                    );
                    const decipher = crypto.createDecipheriv(
                        "aes-128-cbc",
                        new Buffer(key),
                        dataIVBytes
                    );
                    decipher.setAutoPadding(false);
                    const decryptedBuffer = Buffer.concat([decipher.update(encryptedBytes), decipher.final()]);
                    fileBuffer = decryptedBuffer.slice(0, dataSize);
                }
                resolve(fileBuffer);
            });
        });
    }

}
