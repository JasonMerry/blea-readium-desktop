
export interface IDecryptPdf {
    moveFile(originalFilePath: string, pathToOutput: string): any;
    decryptFile(path: string, key: string): any;
    decryptData(path: string, key: string): any;
}
