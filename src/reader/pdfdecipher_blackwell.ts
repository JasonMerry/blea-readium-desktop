import { injectable } from "inversify";

import { IDecryptPdf } from "blea/reader/pdfdecipher";

const crypto = require("crypto");
const fs = require("fs-extra");

@injectable()
export class BlackwellPdfDecrypt implements IDecryptPdf {

    public moveFile(originalFilePath: string, pathToOutput: string): any {
        // move the downloaded file to the users directory
        // create bookshelf directory
        fs.move(originalFilePath, pathToOutput, { overwrite: true }, (err) => { console.log(err); });
    }

    // decrypt a file - return decrypted version in promise
    public decryptFile(path: string, key: string): any {
        return new Promise((resolve, reject) => {
            this.decryptData(path, key).then(
                (result) => {
                    try {
                        resolve(result);
                    } catch (err) {
                        reject(Error(err));
                    }
                },
                (error) => {
                    reject(error);
                }
            );
        });
    }

    public decryptData(path: string, key: string): any {
        const LENGTH_IV = 16;
        const INT_LENGTH = 4;
        const ENCRYPTION_TYPE_CODE_LENGTH = 8;

        return new Promise((resolve, reject) => {
            fs.readFile(path, (err, fileBuffer) => {
                if (err) { reject(Error(err)); };
                const fileBytes = fileBuffer.slice(0, 8);
                const fileEncryption = fileBytes.toString("ascii", 0, fileBytes.length);
                const rdaesCodes = [82, 68, 65, 69, 83, 0, 0, 0]; // RDAES

                if (!rdaesCodes.every((e, i) => fileEncryption.charCodeAt(i) === e)) {
                    reject(Error("Not a globe encoded file"));
                }

                const chunksCount = fileBuffer.readUIntBE(ENCRYPTION_TYPE_CODE_LENGTH, INT_LENGTH);
                const ivStart = ENCRYPTION_TYPE_CODE_LENGTH + INT_LENGTH * 2;
                const iv = fileBuffer.slice(ivStart, ivStart + LENGTH_IV);

                const startChunkInfo = ENCRYPTION_TYPE_CODE_LENGTH + INT_LENGTH * 2 + LENGTH_IV;
                let chunks = [];
                let currentOffset: number;
                let offset: number;
                let length: number;
                for (let i = 0; i < chunksCount; i++) {
                    currentOffset = startChunkInfo + (INT_LENGTH * 2 * i);
                    offset = fileBuffer.readUIntBE(currentOffset, INT_LENGTH);
                    length = fileBuffer.readUIntBE(currentOffset + INT_LENGTH, INT_LENGTH);
                    chunks.push(
                        {
                            length,
                            offset,
                        }
                    );
                }
                let decipher;
                let keyBuffer = new Buffer(key);
                let decryptedBuffer;
                let encryptedBuffer;
                let completeDecryptedBuffer = Buffer.alloc(4096 * chunksCount);
                let cursorPos = 0;
                let decryptedBufferLength;
                for (let chunkInfo of chunks) {
                    // re-init. as this is what Android did
                    decipher = crypto.createDecipheriv(
                        "aes-128-cbc",
                        keyBuffer,
                        iv
                    );
                    encryptedBuffer = fileBuffer.slice(chunkInfo.offset, chunkInfo.offset + chunkInfo.length);
                    decryptedBuffer = Buffer.concat([decipher.update(encryptedBuffer), decipher.final()]);
                    decryptedBufferLength = decryptedBuffer.length;
                    decryptedBuffer.copy(completeDecryptedBuffer, cursorPos);
                    cursorPos += decryptedBufferLength;
                }
                // trim off end
                let rtnBuffer = completeDecryptedBuffer.slice(0, cursorPos);
                resolve(rtnBuffer);
            });
        });
    }

}
