import test from "ava";

import { IDecrypt } from "blea/reader/epubdecipher";
import { IDecryptPdf } from "blea/reader/pdfdecipher";

import { container } from "blea/renderer/di";
import TYPES from "blea/types";

import { IDecompress } from "blea/reader/decompress";

test.skip(
    "get epub decrypter instance - data", (t) => {
        let path = "../resources/test_data/Copyright.html";
        const epubDecryptInst = container.get<IDecrypt>(TYPES.IDecrypt);
        return epubDecryptInst.decryptData(path, "key").then(
            (result) => { t.is(result.length, 13104); },
            (error) => { console.log("error", error); }
        );
    }
);

test.skip(
    "get epub decrypter instance - text", (t) => {
        let path = "../resources/test_data/Copyright.html";
        const epubDecryptInst = container.get<IDecrypt>(TYPES.IDecrypt);
        return epubDecryptInst.decryptFile(path, "key").then(
            (result) => { t.is(result.length, 13099); },
            (error) => { console.log("error", error); }
        );
    }
);

test.skip(
    "get epub unzipper instance", (t) => {
        let path = "../resources/test_data/9781781575161.glb";
        const epubDecomressInst = container.get<IDecompress>(TYPES.IDecompress);
        return epubDecomressInst.decompressFile(path, "/tmp/decom")
            .then(
                (result) => { t.is(result.length, 348); },
                (error) => { console.log("error", error); }
            );
    }
);

test.skip(
    "get pdf decryption", (t) => {
        let path = "/tmp/recursion_enc.glb";
        const pdfDecryptPdfInst = container.get<IDecryptPdf>(TYPES.IDecryptPdf);
        return pdfDecryptPdfInst.decryptFile(path, "yoIpnQCyjqAfsko4")
            .then(
                (result) => {
                    console.log("finsished");
                    t.is(result.length, 348); },
                (error) => { console.log("error", error); }
            );
    }
);
