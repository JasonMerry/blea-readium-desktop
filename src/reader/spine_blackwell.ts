import * as fs from "fs";
import * as path from "path";
import * as xml2js from "xml2js";

import { injectable } from "inversify";

import { Spine } from "blea/reader/spine";
import { bookDownloadPath } from "blea/resources/device";

const containerPath = "META-INF/container.xml";

@injectable()
export class BlackwellSpine implements Spine {

    public getEpubSpine(bookshelfPk: number) {

        const parser = new xml2js.Parser();
        const rootPath = bookDownloadPath(bookshelfPk);

        function getContainerXML() {
            return new Promise(
                (resolve, reject) => {
                    try {
                        fs.readFile(
                            path.join(rootPath, containerPath), (err, data) => {
                                if (err) {
                                    reject(Error(String(err)));
                                }
                                resolve(data);
                            }
                        );
                    } catch (err) {
                        reject(Error(String(err)));
                    }
                }
            );
        }

        function getOPFPath(xmlString: string) {
            return new Promise(
                (resolve, reject) => {
                    try {
                        parser.parseString(
                            xmlString,
                            (err, result) => {
                                const opfFile = result.container.rootfiles[0].rootfile[0].$["full-path"];
                                if (err) {
                                    reject(Error(String(err)));
                                }
                                resolve(opfFile);
                            }
                        );
                    } catch (err) {
                        reject(Error(String(err)));
                    }
                }
            );
        }

        function getOPFXML(opfPath: string) {
            return new Promise(
                (resolve, reject) => {
                    try {
                        fs.readFile(
                            path.join(rootPath, opfPath),
                            (err, data) => {
                                if (err) {
                                    reject(Error(String(err)));
                                }
                                resolve({data, opfPath});
                            }
                        );
                    } catch (err) {
                        reject(Error(String(err)));
                    }
                }
            );
        }

        function getSpineItems(xmlStringAndPath: any) {
            const xmlString =  xmlStringAndPath.data;
            const opfPath = xmlStringAndPath.opfPath;
            const fileDirSplit = opfPath.split("/");
            let fileDir = "";
            if (fileDirSplit.length > 1) {
                fileDir = opfPath.split("/")[0];
            }
            return new Promise(
                (resolve, reject) => {
                    try {
                        parser.parseString(xmlString, (err, result) => {
                            let manifest = result.package.manifest;
                            let idToFileMapping = manifest[0].item.reduce(
                                (mapping, item) => {
                                    mapping[item.$.id] = item.$.href;
                                    return mapping;
                                },
                                {}
                            );
                            // get xhtml/html file from manifest for spines
                            let spineItems = result.package.spine[0].itemref.map(
                                (item: any) => {
                                    const itemId = item.$.idref;
                                    return {id: itemId, file: idToFileMapping[itemId]};
                                }
                            );
                            resolve({fileDir, spineItems});
                        });
                    } catch (err) {
                        reject(Error(String(err)));
                    }
                }
            );
        }
        return getContainerXML().then(getOPFPath).then(getOPFXML).then(getSpineItems);
    }
}
