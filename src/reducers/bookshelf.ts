import * as update from "immutability-helper";

import {
    BOOK_SET_SPINE_MAP,
    BOOK_UPDATE,
    BookshelfAction,
    FETCH_BOOKSHELF_SUCCESS,
    REMOVE_BOOKSHELF,
    SET_BOOKSHELF,
} from "blea/actions/bookshelf";

import {
    BOOK_ADD_BOOKMARK,
    BOOK_ADD_HIGHLIGHT,
    BOOK_DELETE_BOOKMARK,
    BOOK_DELETE_HIGHLIGHT,
    BOOK_LAST_WORD_ID,
    BOOK_SEARCH_UPDATE,
    BOOK_SET_CURRENT_PAGE,
    BOOK_SET_HIGHLIGHTS,
    BOOK_SET_NUMBER_PAGES,
    BOOK_UPDATE_BOOKMARKS,
    BookAction,
    BookSearchAction,
} from "blea/actions/book";

import { Bookshelf } from "blea/entities/bookshelf";

const initialState = {
};

export function bookshelf(
    state: Bookshelf = initialState,
    action: BookshelfAction|BookAction,
    ): Bookshelf {
    switch (action.type) {
    case SET_BOOKSHELF:
        state = (action as BookshelfAction).bookshelf;
        return state;
    case FETCH_BOOKSHELF_SUCCESS:
        state = (action as BookshelfAction).bookshelf;
        return state;
    case REMOVE_BOOKSHELF:
        return {};
    case BOOK_UPDATE:
        const bookToUpdate = (action as BookAction).book;
        const newBookshelfState = update(
            state, {books: {[bookToUpdate.bookshelfPk]: {download: {$set: bookToUpdate.download}}}}
        );
        const newBookshelfState1 = update(
            newBookshelfState, {books: {[bookToUpdate.bookshelfPk]: {bookType: {$set: bookToUpdate.bookType}}}}
        );
        return newBookshelfState1;
    case BOOK_SET_CURRENT_PAGE:
        const bookToUpdateCp = (action as BookAction).book;
        const newBookshelfStateCp = update(
            state, {books: {[bookToUpdateCp.bookshelfPk]: {currentPage: {$set: (action as BookAction).page}}}}
        );
        return newBookshelfStateCp;
    case BOOK_SET_NUMBER_PAGES:
        const bookToUpdateNp = (action as BookAction).book;
        const newBookshelfStateNp = update(
            state, {books: {[bookToUpdateNp.bookshelfPk]: {pdfpages: {$set: (action as BookAction).pdfpages}}}}
        );
        return newBookshelfStateNp;
    case BOOK_SET_SPINE_MAP:
        const bookshelfPk = (action as BookAction).book.bookshelfPk;
        const newSpineState = update(
            state, {books: {[bookshelfPk]: {spineBrickMap: {$set: (action as BookAction).spineBrickMap}}}}
        );
        return newSpineState;
    case BOOK_LAST_WORD_ID:
        const newLastWordState = update(
            state, {books: {[(action as BookAction).book.bookshelfPk]: {lastWordId: {$set: (action as BookAction).wordId}}}}
        );
        return newLastWordState;
    case BOOK_SET_HIGHLIGHTS:
        const newHighlightState = update(
            state, {books: {[(action as BookAction).book.bookshelfPk]: {highlights: {$set: (action as BookAction).highlights}}}}
        );
        return newHighlightState;
    case BOOK_ADD_BOOKMARK:
        let bookshelfPk1 = (action as BookAction).book.bookshelfPk;
        let bookOnShelf = state.books[bookshelfPk1];
        if (bookOnShelf.bookmarks) {
            const newBookmarkState = update(
                state, {books: {[bookshelfPk1]: {bookmarks: {$push: [(action as BookAction).bookmark]}}}}
            );
            return newBookmarkState;
        } else {
            const newBookmarkState1 = update(
                state, {books: {[bookshelfPk1]: {bookmarks: {$set: [(action as BookAction).bookmark]}}}}
            );
            return newBookmarkState1;
        }
    case BOOK_UPDATE_BOOKMARKS:
        const newBookmarkState2 = update(
            state, {books: {[(action as BookAction).book.bookshelfPk]: {bookmarks: {$set: (action as BookAction).bookmarks}}}}
        );
        return newBookmarkState2;
    case BOOK_DELETE_BOOKMARK:
        let bookshelfPk3 = (action as BookAction).book.bookshelfPk;
        let bookOnShelf3 = state.books[bookshelfPk3];
        let bookmarkId = (action as BookAction).bookmarkId;
        if (bookOnShelf3.bookmarks) {
            // filter out bookmark with bookmarkId
            const newBookmarkState = update(
                state,
                {
                    books: {
                        [bookshelfPk3]: {
                            bookmarks: {
                                $set: bookOnShelf3.bookmarks.filter((_) => (_.id !== bookmarkId))
                            }
                        }
                    }
                }
            );
            return newBookmarkState;
        } else {
            return state;
        }
    case BOOK_ADD_HIGHLIGHT:
        let bookshelfPk2 = (action as BookAction).book.bookshelfPk;
        let bookOnShelf2 = state.books[bookshelfPk2];
        if (bookOnShelf2.highlights) {
            const newHighlightState1 = update(
                state, {books: {[bookshelfPk2]: {highlights: {$push: [(action as BookAction).highlight]}}}}
            );
            return newHighlightState1;
        } else {
            const newHighlightState2 = update(
                state, {books: {[bookshelfPk2]: {highlights: {$set: [(action as BookAction).highlight]}}}}
            );
            return newHighlightState2;
        }
    case BOOK_DELETE_HIGHLIGHT:
        let bookshelfPk4 = (action as BookAction).book.bookshelfPk;
        let bookOnShelf4 = state.books[bookshelfPk4];
        let highlightId = (action as BookAction).highlightId;
        if (bookOnShelf4.highlights) {
            // filter out bookmark with bookmarkId
            const newHighlightState3 = update(
                state,
                {
                    books: {
                        [bookshelfPk4]: {
                            highlights: {
                                $set: bookOnShelf4.highlights.filter((_) => (_.id !== highlightId))
                            }
                        }
                    }
                }
            );
            return newHighlightState3;
        } else {
            return state;
        }
    case BOOK_SEARCH_UPDATE:
        const newBooKSearchState = update(
            state, {books: {[(action as BookSearchAction).bookshelfPk]: {result: {$set: (action as BookSearchAction).result}}}}
        );
        return newBooKSearchState;
    default:
        return state;
    }
}
