import { ILocaleAction, SET_LOCALE } from "blea/actions/i18n";

export interface I18NState {
    locale: string;
}

const initialState: I18NState = {
    locale: "en",
};

export function i18n(
    state: I18NState = initialState,
    action: ILocaleAction,
    ): I18NState {
    switch (action.type) {
    case SET_LOCALE:
        return {...state, locale: action.locale};
    default:
        return state;
    }
}
