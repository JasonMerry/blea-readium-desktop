import { ILayoutAction, SET_LAYOUT } from "blea/actions/layout";

const initialState: string = "grid";

export function layout(
    state: string = initialState,
    action: ILayoutAction,
    ): string {
    switch (action.type) {
    case SET_LAYOUT:
        return action.layout;
    default:
        return state;
    }
}
