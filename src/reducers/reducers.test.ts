import test from "ava";

import {reducerTest} from "redux-ava";

import {
    changeForm,
    requestError,
    sendingRequest,
    setAuthState,
} from "blea/actions/auth";

import { setLocale } from "blea/actions/i18n";
import { i18n } from "blea/reducers/i18n";

import { setLayout } from "blea/actions/layout";
import { layout } from "blea/reducers/layout";

import { auth } from "blea/renderer/reducers/auth";

import * as bookDownloadActions from "blea/actions/book-download";
import { bookDownloadReducer } from "blea/main/reducers/book-download";

import {
    addBookHighlight,
    addBookmark,
    deleteBookHighlight,
    deleteBookmark,
    setBookHighlights,
    setLastWordId,
    updateBookmarks,
} from "blea/actions/book";

import {
    removeBookshelf,
    setBookshelf,
    updateBook,
    updateBookSpineMap,
} from "blea/actions/bookshelf";
import { bookshelf } from "blea/reducers/bookshelf";

import { Download } from "blea/entities/download";

import * as downloaderActions from "blea/main/actions/downloader";
import { downloader } from "blea/main/reducers/downloader";

let stateBefore = {
    currentlySending: false,
    errorMessage: "",
    errorStatus: "",
    formState: {
        password: "",
        username: "",
    },
    userState: {
        loggedIn: false
    }
};

test(
    "reducer handles CHANGE_FORM action", reducerTest(
        auth,
        stateBefore,
        changeForm({username: "stephen", password: "password"}),
        {...stateBefore, formState: {username: "stephen", password: "password"}}
    )
);

test(
    "reducer handles SET_AUTH action", reducerTest(
        auth,
        stateBefore,
        setAuthState({loggedIn: true, token: "some token"}),
        {...stateBefore, userState: {loggedIn: true, token: "some token"}}
    )
);

test(
    "reducer handles SENDING_REQUEST action", reducerTest(
        auth,
        stateBefore,
        sendingRequest(true),
        {...stateBefore, currentlySending: true}
    )
);

test(
    "reducer handles REQUEST_ERROR action", reducerTest(
        auth,
        stateBefore,
        requestError(200, "some error happened"),
        {...stateBefore, errorStatus: 200, errorMessage: "some error happened"}
    )
);

let bookDownloadStateBefore = {
    bookIdentifierToDownloads: {},
    downloadIdentifierToBook: {},
};

let testBook = {bookshelfPk: 123, isbn13: "hello", download: {status: 1, progress: 0}};

test(
    "reducer handles BOOK_DOWNLOAD_ADD action - does nothing", reducerTest(
        bookDownloadReducer,
        bookDownloadStateBefore,
        bookDownloadActions.add(testBook),
        {...bookDownloadStateBefore}
    )
);

let testDownload: Download = {
    downloadParams: {url: "/test/", headers: {}, qs: {}},
    dstPath: "/somewhere",
    identifier: "abc",
    progress: 0,
    status: 1,
};

let testDownloads = [
    testDownload
];

test(
    "reducer handles BOOK_DOWNLOAD_START action", reducerTest(
        bookDownloadReducer,
        bookDownloadStateBefore,
        bookDownloadActions.start(testBook, testDownloads),
        {
            bookIdentifierToDownloads: { 123: testDownloads },
            downloadIdentifierToBook: { abc: { bookshelfPk: 123, isbn13: "hello", download: {status: 1, progress: 0} } },
        }
    )
);

test(
    "reducer downloader add download", reducerTest(
        downloader,
        {downloads: {}},
        downloaderActions.add(testDownload),
        {downloads: { abc: testDownload }}
    )
);

test(
    "reducer downloader remove download", reducerTest(
        downloader,
        {downloads: { abc: testDownload }},
        downloaderActions.remove(testDownload),
        {downloads: {}},
    )
);

let cancelledDownload: Download = {
    downloadParams: {url: "/test/", headers: {}, qs: {}},
    dstPath: "/somewhere",
    identifier: "abc",
    progress: 0,
    status: 2,
};

test(
    "reducer downloader cancel download", reducerTest(
        downloader,
        {downloads: {abc: testDownload }},
        downloaderActions.cancel(testDownload),
        {downloads: {abc: cancelledDownload}},
    )
);

let startedDownload: Download = {
    downloadParams: {url: "/test/", headers: {}, qs: {}},
    dstPath: "/somewhere",
    identifier: "abc",
    progress: 0,
    status: 1,
};

test(
    "reducer downloader start download", reducerTest(
        downloader,
        {downloads: {abc: testDownload }},
        downloaderActions.start(testDownload),
        {downloads: {abc: startedDownload}},
    )
);

let testDownload1: Download = {
    downloadParams: {url: "/test/", headers: {}, qs: {}},
    dstPath: "/somewhere",
    identifier: "abc",
    progress: 0,
    status: 1,
};

let progressedDownload: Download = {
    downloadParams: {url: "/test/", headers: {}, qs: {}},
    dstPath: "/somewhere",
    identifier: "abc",
    progress: 50,
    status: 1,
};

test(
    "reducer downloader progress download", reducerTest(
        downloader,
        {downloads: {abc: testDownload1 }},
        downloaderActions.progress(testDownload1, 50),
        {downloads: {abc: progressedDownload}},
    )
);

let testDownload2: Download = {
    downloadParams: {url: "/test/", headers: {}, qs: {}},
    dstPath: "/somewhere",
    identifier: "abc",
    progress: 50,
    status: 1,
};

let finishedDownload: Download = {
    downloadParams: {url: "/test/", headers: {}, qs: {}},
    dstPath: "/somewhere",
    identifier: "abc",
    progress: 100,
    status: 4,
};

test(
    "reducer downloader progress download", reducerTest(
        downloader,
        {downloads: {abc: testDownload2 }},
        downloaderActions.finish(testDownload2),
        {downloads: {abc: finishedDownload}},
    )
);

let testDownload3: Download = {
    downloadParams: {url: "/test/", headers: {}, qs: {}},
    dstPath: "/somewhere",
    identifier: "abc",
    progress: 50,
    status: 1,
};

let failedDownload: Download = {
    downloadParams: {url: "/test/", headers: {}, qs: {}},
    dstPath: "/somewhere",
    error: "some error",
    identifier: "abc",
    progress: 0,
    status: 3,
};

test(
    "reducer downloader progress download", reducerTest(
        downloader,
        {downloads: {abc: testDownload3 }},
        downloaderActions.fail(testDownload3, "some error"),
        {downloads: {abc: failedDownload}},
    )
);

test(
    "reducer i18n", reducerTest(
        i18n,
        {locale: "en"},
        setLocale("fr"),
        {locale: "fr"},
    )
);

test(
    "reducer layout", reducerTest(
        layout,
        "grid",
        setLayout(false),
        "list",
    )
);

let testBookshelf = {
    books: {
        123: testBook
    }
};

test(
    "reducer bookshelf set bookshelf", reducerTest(
        bookshelf,
        {},
        setBookshelf(testBookshelf),
        testBookshelf,
    )
);

test(
    "reducer bookshelf remove bookshelf", reducerTest(
        bookshelf,
        {books: {123: testBook}},
        removeBookshelf(),
        {},
    )
);

let testUpdateBook = {...testBook, download: {status: 1, progress: 50}, bookType: 0};
test(
    "reducer bookshelf update book", reducerTest(
        bookshelf,
        {books: {123: testBook}},
        updateBook(testUpdateBook),
        {books: {123: testUpdateBook}},
    )
);

let spineMap = [{idref: "abc", first: 1, last: 10}];
test(
    "reducer bookshelf update book spine brick map", reducerTest(
        bookshelf,
        {books: {123: testBook}},
        updateBookSpineMap(testBook, spineMap),
        {books: {123: {...testBook, spineBrickMap: spineMap}}},
    )
);

test(
    "reducer bookshelf update book lastWordId", reducerTest(
        bookshelf,
        {books: {123: testBook}},
        setLastWordId(testBook, 15),
        {books: {123: {...testBook, lastWordId: 15}}},
    )
);

test(
    "reducer bookshelf update book highlights", reducerTest(
        bookshelf,
        {books: {123: testBook}},
        setBookHighlights(testBook, [{highlight: 1}]),
        {books: {123: {...testBook, highlights: [{highlight: 1}]}}},
    )
);

test(
    "reducer bookshelf add first bookmark", reducerTest(
        bookshelf,
        {books: {123: testBook}},
        addBookmark(testBook, {id: 1234, wordId: 123, words: "blah"}),
        {books: {123: {...testBook, bookmarks: [{id: 1234, wordId: 123, words: "blah"}]}}},
    )
);

test(
    "reducer bookshelf add another bookmark", reducerTest(
        bookshelf,
        {books: {123: {...testBook, bookmarks: [{id: 1234, wordId: 123, words: "blah"}]}}},
        addBookmark(testBook, {id: 12345, wordId: 1234, words: "blah blah"}),
        {books: {123: {...testBook, bookmarks: [
            {id: 1234, wordId: 123, words: "blah"},
            {id: 12345, wordId: 1234, words: "blah blah"}
        ]}}},
    )
);

test(
    "reducer bookshelf update bookmarks", reducerTest(
        bookshelf,
        {books: {123: testBook}},
        updateBookmarks(testBook, [{id: 1234, wordId: 123, words: "blah"}]),
        {books: {123: {...testBook, bookmarks: [{id: 1234, wordId: 123, words: "blah"}]}}},
    )
);

test(
    "reducer bookshelf delete bookmark", reducerTest(
        bookshelf,
        {books: {123: {...testBook, bookmarks: [{id: 1234, wordId: 123, words: "blah"}]}}},
        deleteBookmark(testBook, 1234),
        {books: {123: {...testBook, bookmarks: []}}},
    )
);

test(
    "reducer bookshelf delete bookmark multiple", reducerTest(
        bookshelf,
        {books: {123: {...testBook, bookmarks: [{id: 1234, wordId: 123, words: "blah"}, {id: 1235, wordId: 125, words: "blah"}]}}},
        deleteBookmark(testBook, 1235),
        {books: {123: {...testBook, bookmarks: [{id: 1234, wordId: 123, words: "blah"}]}}},
    )
);

test(
    "reducer book add highlight", reducerTest(
        bookshelf,
        {books: {123: testBook}},
        addBookHighlight(testBook, {selection: {word_ids: [1, 2, 3]}}),
        {books: {123: {...testBook, highlights: [{selection: {word_ids: [1, 2, 3]}}]}}},
    )
);

test(
    "reducer bookshelf add another highlight", reducerTest(
        bookshelf,
        {books: {123: {...testBook, highlights: [{selection: {word_ids: [1, 2, 3]}}]}}},
        addBookHighlight(testBook, {selection: {word_ids: [4, 5, 6, 7]}}),
        {books: {123: {...testBook, highlights: [
            {selection: {word_ids: [1, 2, 3]}},
            {selection: {word_ids: [4, 5, 6, 7]}}
        ]}}},
    )
);

test(
    "reducer bookshelf delete highlight", reducerTest(
        bookshelf,
        {books: {123: {...testBook, highlights: [{id: 1234, selection: {word_ids: [1, 2, 3]}}]}}},
        deleteBookHighlight(testBook, 1234),
        {books: {123: {...testBook, highlights: []}}},
    )
);
