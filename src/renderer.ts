import "material-design-icons-iconfont/dist/material-design-icons.css";
import "materialize-css/dist/css/materialize.css";
import "./assets/css/main.css";

import "materialize-css/dist/js/materialize.js";

import * as React from "react";
import * as ReactDOM from "react-dom";
// import * as injectTapEventPlugin from "react-tap-event-plugin";

let {ipcRenderer} = require("electron");

import App from "blea/components/App";

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
// injectTapEventPlugin();

// Render React App component
ReactDOM.render(
    React.createElement(App, {}, null),
    document.getElementById("app"),
);

// register service worker
// service worker needs to live in dist
navigator.serviceWorker.register("./sw.js")
    .then((reg: any) => {
        console.log("service worker registered");
    })
    .catch((error: any) => {
        console.log("error when registering service worker", error);
    });
