import { Container } from "inversify";

import "reflect-metadata";

import { Store } from "redux";

import getDecorators from "inversify-inject-decorators";

import { IAuthApi } from "blea/api/auth";
import { BlackwellAuthApi } from "blea/api/auth_blackwell";

import { Translator } from "blea/i18n/translator";
import { IAppState } from "blea/renderer/reducers";

import { store } from "blea/renderer/store/memory";

import TYPES from "blea/types";

let container = new Container();

container.bind<Translator>(Translator).toSelf();
container.bind<IAuthApi>(TYPES.IAuthApi).to(BlackwellAuthApi);
container.bind<Store<IAppState>>("store").toConstantValue(store);

let {
    lazyInject,
    lazyInjectNamed,
    lazyInjectTagged,
    lazyMultiInject,
} = getDecorators(container);

export {
    container,
    lazyInject,
    lazyInjectNamed,
    lazyInjectTagged,
    lazyMultiInject,
};
