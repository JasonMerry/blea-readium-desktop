
import {
    CHANGE_FORM,
    CLEAR_ERROR,
    REQUEST_ERROR,
    SENDING_REQUEST,
    SET_AUTH,
} from "blea/actions/auth";

import { AuthLogin, AuthState, UserAuthState } from "blea/entities/auth";

// The initial application state
let initialState = {
    currentlySending: false,
    errorMessage: "",
    errorStatus: null,
    formState: {
        password: "",
        username: "",
    },
    userState: {
        loggedIn: false
    }
};

// Takes care of changing the application state
export function auth (state: AuthState = initialState, action: any): AuthState {
    switch (action.type) {
    case CHANGE_FORM:
        return {...state, formState: action.newFormState};
    case SET_AUTH:
        return {...state, userState: action.newAuthState};
    case SENDING_REQUEST:
        return {...state, currentlySending: action.sending};
    case REQUEST_ERROR:
        return {...state, errorStatus: action.errorStatus, errorMessage: action.errorMessage};
    case CLEAR_ERROR:
        return {...state, errorStatus: null, errorMessage: ""};
    default:
        return state;
    }
};
