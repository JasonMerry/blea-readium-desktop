import { routerReducer } from "react-router-redux";
import { combineReducers } from "redux";

import { auth } from "blea/renderer/reducers/auth";
import { AuthState } from "blea/entities/auth";
import { i18n, I18NState } from "blea/reducers/i18n";

import { Bookshelf } from "blea/entities/bookshelf";
import { bookshelf } from "blea/reducers/bookshelf";

import { layout } from "blea/reducers/layout";

export interface IAppState {
    auth: AuthState;
    bookshelf: Bookshelf;
    i18n: I18NState;
    layout: string;
    router: any;
}

export const app = combineReducers({
    auth,
    bookshelf,
    i18n,
    layout,
    router: routerReducer
});
