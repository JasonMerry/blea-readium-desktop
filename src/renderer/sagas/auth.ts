import { ipcRenderer } from "electron";
import { channel, Channel, SagaIterator } from "redux-saga";
import { call, fork, put, take } from "redux-saga/effects";
import { forwardTo } from "blea/renderer/store/memory";

import {
    changeForm,
    clearError,
    LOGIN_REQUEST,
    LOGOUT,
    requestError,
    sendingRequest,
    setAuthState,
} from "blea/actions/auth";

import {
    AUTH_COMPLETE,
    AUTH_LOGIN_REQUEST,
    AUTH_LOGIN_RESPONSE,
    AUTH_LOGOUT_REQUEST,
    AUTH_LOGOUT_COMPLETE,
    AUTH_REQUEST_ERROR,
    AUTH_REQUEST_FINISHED,
    AUTH_SENDING_REQUEST,
} from "blea/events/ipc";

import { Auth } from "blea/entities/auth";
import { AuthLoginMessage } from "blea/entities/ipc";

function waitForAuthLoginResponse(chan: Channel<Auth>) {
    // alert("waitForAuthLoginResponse");
    // Wait for catalog response from main process
    if (typeof ipcRenderer !== "undefined") {
        ipcRenderer.on(AUTH_LOGIN_RESPONSE, (event: any, msg: any) => {
            console.log("AUTH_LOGIN_RESPONSE", msg);
            chan.put(msg);
        });
    }
}

export function * watchMainAuthLoginResponse(): SagaIterator {
    // alert("watchMainAuthLoginResponse");
    const chan = yield call(channel);
    yield fork(waitForAuthLoginResponse, chan);

    while (true) {
        // alert("watchMainAuthLoginResponse TRUE");
        // Wait for a response from the main process
        const auth = yield take(chan);
        // yield put(catalogActions.set(catalog));
        switch (auth.type) {
        case AUTH_SENDING_REQUEST:
            yield put(clearError());
            yield put(sendingRequest(true));
            console.log("sending request");
            break;
        case AUTH_REQUEST_FINISHED:
            yield put(sendingRequest(false));
            console.log("sending request finished");
            break;
        case AUTH_REQUEST_ERROR:
            yield put(requestError(auth.status, auth.message));
            console.log("login request error", auth);
            break;
        case AUTH_COMPLETE:
            console.log("auth complete", auth.content);
            yield put(setAuthState(auth.content));
            yield put(changeForm({username: "", password: ""})); // Clear form
            yield call(forwardTo, "/library");
        case AUTH_LOGOUT_COMPLETE:
            console.log("auth logout complete", auth.content);
            yield put(setAuthState(auth.content));
            yield call(forwardTo, "/");
        default:
            break;
        }
    }
}

function sendIPCAuthLogin(loginForm: AuthLoginMessage) {
    // alert("sendIPCAuthLogin");
    ipcRenderer.send(AUTH_LOGIN_REQUEST, loginForm);
}

export function * watchAuthLogin(): SagaIterator {
    // alert("watchAuthLogin");
    while (true) {
        // alert("watchAuthLogin TRUE");
        let resp = yield take(LOGIN_REQUEST);
        let loginForm = resp.data;
        // alert("watchAuthLogin " + loginForm);
        if (loginForm.username || loginForm.password) {
            sendIPCAuthLogin(
                {
                    login: {
                        password: loginForm.password,
                        username: loginForm.username
                    }
                }
            );
        } else {
            yield put(requestError(401,"You need to fill in both fields to Login."));
        }
    }
}

function sendIPCAuthLogout() {
    console.log("AUTH_LOGOUT_REQUEST");
    ipcRenderer.send(AUTH_LOGOUT_REQUEST, {});
}

export function * watchAuthLogout(): SagaIterator {
    while (true) {
        let resp = yield take(LOGOUT);
        sendIPCAuthLogout();
    }
}
