import { ipcRenderer } from "electron";
import { channel, Channel, SagaIterator } from "redux-saga";
import { call, fork, put, take } from "redux-saga/effects";

import {
    BOOK_DOWNLOAD_ADD,
} from "blea/actions/book-download";

import {
    BOOK_ADD_BOOKMARK,
    BOOK_ADD_HIGHLIGHT,
    BOOK_BOOKMARKS_GET,
    BOOK_DELETE_HIGHLIGHT,
    BOOK_DELETE_BOOKMARK,
    BOOK_GET_HIGHLIGHTS,
    BOOK_LAST_WORD_ID,
    BOOK_LAST_PAGE,
    BOOK_SEARCH_REQUEST,
    setBookHighlights,
    setLastWordId,
    updateBookmarks,
    updateSearchResults,
} from "blea/actions/book";


import { Book, Bookmark } from "blea/entities/book";

import {
    BOOK_BOOKMARKS_GET_REQUEST,
    BOOK_BOOKMARKS_GET_RESPONSE,
    BOOK_BOOKMARK_ADD_REQUEST,
    BOOK_BOOKMARK_DELETE_REQUEST,
    BOOK_DOWNLOAD_REQUEST,
    BOOK_DOWNLOAD_RESPONSE,
    BOOK_LAST_PAGE_REQUEST,
    BOOK_LAST_PAGE_RESPONSE,
    BOOK_HIGHLIGHT_ADD_REQUEST,
    BOOK_HIGHLIGHT_DELETE_REQUEST,
    BOOK_HIGHLIGHT_REQUEST,
    BOOK_HIGHLIGHT_RESPONSE,
    BOOK_SEARCH_GET_REQUEST,
    BOOK_SEARCH_GET_RESPONSE,
} from "blea/events/ipc";

function sendIPCBookDownloadStart(download: Book) {
    console.log('send ipc BOOK_DOWNLOAD_REQUEST', download);
    ipcRenderer.send(BOOK_DOWNLOAD_REQUEST, download);
}

export function * watchBookDownload(): SagaIterator {
    while (true) {
        const resp = yield take(BOOK_DOWNLOAD_ADD);
        const download: Book = resp.book;
        sendIPCBookDownloadStart(download);
    }
}

function sendIPCBookLastPageRequest(book: Book) {
    ipcRenderer.send(BOOK_LAST_PAGE_REQUEST, book);
}

export function * watchBookLastPage(): SagaIterator {
    while (true) {
        const resp = yield take(BOOK_LAST_PAGE);
        const book: Book = resp.book;
        sendIPCBookLastPageRequest(book);
    }
}

function waitForSendLastPageResponse(chan: Channel<any>) {
    if (ipcRenderer) {
        ipcRenderer.on(BOOK_LAST_PAGE_RESPONSE, (event: any, msg: any) => {
            console.log("BOOK_LAST_PAGE_RESPONSE", msg);
            chan.put({
                book: msg.book,
                wordId: msg.wordId,
            });
        });
    }
}

export function* watchSendLastPageResponse(): SagaIterator {
    const chan = yield call(channel);
    yield fork(waitForSendLastPageResponse, chan);

    while (true) {
        // Wait for a manifest url
        const response = yield take(chan);
        console.log('sagas', response);
        // Set last page
        yield put(setLastWordId(
            response.book,
            response.wordId,
        ));
    }
}

function waitForBookHighlightsResponse(chan: Channel<any>) {
    if (ipcRenderer) {
        ipcRenderer.on(BOOK_HIGHLIGHT_RESPONSE, (event: any, msg: any) => {
            console.log("BOOK_HIGHLIGHT_RESPONSE", msg);
            chan.put({
                book: msg.book,
                highlights: msg.highlights,
            });
        });
    }
}

export function* watchBookHighlightsResponse(): SagaIterator {
    const chan = yield call(channel);
    yield fork(waitForBookHighlightsResponse, chan);

    while (true) {
        // Wait for a manifest url
        const response = yield take(chan);
        console.log('sagas book highlights', response);
        // Set book highlihts
        yield put(setBookHighlights(
            response.book,
            response.highlights,
        ));
    }
}

function sendIPCBookHighlightRequest(book: Book) {
    console.log("send ipc BOOK_HIGHLIGHT_REQUEST");
    ipcRenderer.send(BOOK_HIGHLIGHT_REQUEST, book);
}

export function * watchBookHighlights(): SagaIterator {
    while (true) {
        const resp = yield take([BOOK_GET_HIGHLIGHTS, BOOK_ADD_HIGHLIGHT, BOOK_DELETE_HIGHLIGHT]);
        console.log("watchBookHighlights", resp);
        const book: Book = resp.book;
        switch (resp.type) {
        case BOOK_GET_HIGHLIGHTS:
            sendIPCBookHighlightRequest(book);
            break;
        case BOOK_ADD_HIGHLIGHT:
            const highlight: any = resp.highlight;
            sendIPCBookHighlightAdd(book, highlight);
            break;
        case BOOK_DELETE_HIGHLIGHT:
            sendIPCBookHighlightDelete(book, resp.highlightId);
            break;
        }
    }
}

function sendIPCBookHighlightAdd(book: Book, highlight: any) {
    console.log('send ipc BOOK_HIGHLIGHT_ADD_REQUEST', highlight);
    ipcRenderer.send(BOOK_HIGHLIGHT_ADD_REQUEST, {book, highlight});
}

function sendIPCBookHighlightDelete(book: Book, highlightId: number) {
    console.log('send ipc BOOK_HIGHLIGHT_DELETE_REQUEST', highlightId);
    ipcRenderer.send(BOOK_HIGHLIGHT_DELETE_REQUEST, {book, highlightId});
}

function sendIPCBookBookmarkAdd(book: Book, wordId: number, page: number) {
    console.log('send ipc BOOK_BOOKMARK_ADD_REQUEST', wordId);
    ipcRenderer.send(BOOK_BOOKMARK_ADD_REQUEST, {book, wordId, page});
}

function sendIPCBookBookmarkDelete(book: Book, bookmarkId: number) {
    console.log('send ipc BOOK_BOOKMARK_DELETE_REQUEST', bookmarkId);
    ipcRenderer.send(BOOK_BOOKMARK_DELETE_REQUEST, {book, bookmarkId});
}

export function* watchBookAddBookmarkResponse(): SagaIterator {
    const chan = yield call(channel);
    yield fork(waitForBookBookmarksGetResponse, chan);

    while (true) {
        // Wait for a manifest url
        const response = yield take(chan);
        console.log('sagas book bookmark', response);
        // Set book bookmark
        yield put(
            updateBookmarks(
                response.book,
                response.bookmarks,
            )
        )
    }
}

export function * watchBookBookmark(): SagaIterator {
    while (true) {
        const resp = yield take([BOOK_ADD_BOOKMARK, BOOK_DELETE_BOOKMARK, BOOK_BOOKMARKS_GET]);
        const book: Book = resp.book;
        console.log(resp);
        switch (resp.type) {
        case BOOK_ADD_BOOKMARK:
            const bookmark: Bookmark = resp.bookmark;
            sendIPCBookBookmarkAdd(book, bookmark.wordId, bookmark.page);
            break;
        case BOOK_DELETE_BOOKMARK:
            sendIPCBookBookmarkDelete(book, resp.bookmarkId);
            break;
        case BOOK_BOOKMARKS_GET:
            sendIPCBookBookmarksGet(book);
            break;
        }
    }
}

function sendIPCBookBookmarksGet(book: Book) {
    console.log('send ipc BOOK_BOOKMARKS_GET_REQUEST');
    ipcRenderer.send(BOOK_BOOKMARKS_GET_REQUEST, {book});
}

function waitForBookBookmarksGetResponse(chan: Channel<any>) {
    if (ipcRenderer) {
        ipcRenderer.on(BOOK_BOOKMARKS_GET_RESPONSE, (event: any, msg: any) => {
            console.log("BOOK_BOOKMARKS_GET_RESPONSE", msg);
            chan.put({
                book: msg.book,
                bookmarks: msg.bookmarks,
            });
        });
    }
}

function sendIPCBookSearchRequest(bookshelfPk: number, query: string) {
    ipcRenderer.send(BOOK_SEARCH_GET_REQUEST, {bookshelfPk, query});
}

export function * watchBookSearch(): SagaIterator {
    while (true) {
        const resp = yield take([BOOK_SEARCH_REQUEST]);
        const bookshelfPk: number = resp.bookshelfPk;
        switch (resp.type) {
        case BOOK_SEARCH_REQUEST:
            console.log("send IPC request for search");
            sendIPCBookSearchRequest(bookshelfPk, resp.query);
            break;
        }
    }
}

function waitForBookSearchGetResponse(chan: Channel<any>) {
    if (ipcRenderer) {
        ipcRenderer.on(BOOK_SEARCH_GET_RESPONSE, (event: any, msg: any) => {
            chan.put({
                bookshelfPk: msg.bookshelfPk,
                result: msg.result,
            });
        });
    }
}

export function * watchBookSearchResponse(): SagaIterator {
    const chan = yield call(channel);
    yield fork(waitForBookSearchGetResponse, chan);

    while (true) {
        const response = yield take(chan);
        // Set book search results
        yield put(
            updateSearchResults(
                response.bookshelfPk,
                response.result,
            )
        )
    }
}
