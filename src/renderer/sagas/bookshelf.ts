import { ipcRenderer } from "electron";
import { channel, Channel, SagaIterator } from "redux-saga";
import { call, fork, put, take } from "redux-saga/effects";
import { cacheAssets } from "blea/cache";
import { getServer } from "blea/resources/server";

import {
    FETCH_BOOKSHELF,
    FETCH_BOOKSHELF_SUCCESS,
    REMOVE_BOOKSHELF,
    setBookshelf,
    SYNC_BOOKSHELF
} from "blea/actions/bookshelf";

import * as bookDownloadActions from "blea/actions/book-download";

import { Bookshelf } from "blea/entities/bookshelf";
import { Book } from "blea/entities/book";

import {
    BOOKSHELF_FETCH_REQUEST,
    SYNC_BOOKSHELF_RESPONSE,
    SYNC_BOOKSHELF_REQUEST,
    BOOK_DOWNLOAD_REQUEST,
    BOOK_DOWNLOAD_RESPONSE,
    BOOK_INDEX_REQUEST
} from "blea/events/ipc";

// fetch - loads bookshelf from server - online only
function sendIPCBookshelfFetch() {
    console.log('send ipc BOOKSHELF_FETCH_REQUEST');
    ipcRenderer.send(BOOKSHELF_FETCH_REQUEST, {});
}

export function * watchBookshelfFetch(): SagaIterator {
    while (true) {
        let resp = yield take(FETCH_BOOKSHELF);
        sendIPCBookshelfFetch();
    }
}

// syncs the bookshelf from main online or offline
function sendIPCBookshelfSync() {
    console.log('send ipc SYNC_BOOKSHELF_REQUEST');
    ipcRenderer.send(SYNC_BOOKSHELF_REQUEST, {});
}

export function * watchBookshelfSync(): SagaIterator {
    while (true) {
        let resp = yield take(SYNC_BOOKSHELF);
        sendIPCBookshelfSync();
    }
}

function waitForSyncResponse(chan: Channel<Bookshelf>) {
    // Wait for catalog response from main process
    if (ipcRenderer) {
        ipcRenderer.on(SYNC_BOOKSHELF_RESPONSE, (event: any, msg: any) => {
            console.log("waitForSyncResponse", msg);
            chan.put(msg.bookshelf);
        });
    }
}

// If bookshelf from main process has been updated
// Update renderer bookshelf
export function* watchMainBookshelfResponse(): SagaIterator {
    const chan = yield call(channel);
    yield fork(waitForSyncResponse, chan);

    while (true) {
        // Wait for a response from the main process
        const bookshelf = yield take(chan);
        console.log("watchMainBookshelfResponse", bookshelf)
        // add images to cache
        if (bookshelf.books) {
            let assets: string[] = [];
            Object.keys(bookshelf.books).forEach(
                (key) => {
                    assets.push(getServer().SERVER_PATH + bookshelf.books[key].cover);
                }
            );
            cacheAssets(assets);
        }
        yield put(setBookshelf(bookshelf));
    }
}

function sendIPCBookDownloadStart(download: Book) {
    ipcRenderer.send(BOOK_DOWNLOAD_REQUEST, download);
}

export function * watchBookDownload(): SagaIterator {
    while (true) {
        const resp = yield take(bookDownloadActions.BOOK_DOWNLOAD_ADD);
        const download: Book = resp.download;
        sendIPCBookDownloadStart(download);
    }
}

function sendIPCBookIndex(book: Book) {
    ipcRenderer.send(BOOK_INDEX_REQUEST, book);
}

export function * watchBookIndex(): SagaIterator {
    while (true) {
        const resp = yield take(bookDownloadActions.BOOK_DOWNLOAD_INDEX);
        const book: Book = resp.book;
        sendIPCBookIndex(book);
    }
}
