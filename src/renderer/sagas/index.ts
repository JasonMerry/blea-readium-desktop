
import {
    watchAuthLogin,
    watchAuthLogout,
    watchMainAuthLoginResponse,
} from "blea/renderer/sagas/auth";

import {
    watchBookshelfFetch,
    watchMainBookshelfResponse,
    watchBookIndex,
} from "blea/renderer/sagas/bookshelf";

import {
    watchBookDownload,
    watchBookLastPage,
    watchSendLastPageResponse,
    watchBookHighlights,
    watchBookHighlightsResponse,
    watchBookBookmark,
    watchBookAddBookmarkResponse,
    watchBookSearch,
    watchBookSearchResponse,
} from "blea/renderer/sagas/book";

export function * rootSaga() {
    yield [
        watchAuthLogin(),
        watchAuthLogout(),
        watchMainAuthLoginResponse(),
        watchBookshelfFetch(),
        watchMainBookshelfResponse(),
        watchBookDownload(),
        watchBookIndex(),
        watchBookLastPage(),
        watchSendLastPageResponse(),
        watchBookHighlights(),
        watchBookHighlightsResponse(),
        watchBookBookmark(),
        watchBookAddBookmarkResponse(),
        watchBookSearch(),
        watchBookSearchResponse(),
    ];
}
