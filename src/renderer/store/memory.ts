import { routerMiddleware } from "react-router-redux";
import { applyMiddleware, compose, createStore, Store } from "redux";
import logger from "redux-logger";
import { autoRehydrate, persistStore } from "redux-persist";

import createHistory from "history/createMemoryHistory";
import createSagaMiddleware from "redux-saga";

import { app, IAppState } from "blea/renderer/reducers";
import { rootSaga } from "blea/renderer/sagas";

let sagaMiddleware = createSagaMiddleware();

export const history = createHistory();
const rMiddleware = routerMiddleware(history);

export const store: Store<IAppState> = createStore(
    app,
    compose(
        applyMiddleware(sagaMiddleware),
        applyMiddleware(rMiddleware),
        applyMiddleware(logger),
        autoRehydrate(),
    ),
) as (Store<IAppState>);

persistStore(store, {}, () => {
    let initialLocation = store.getState().router.location;
    if (initialLocation) {
        forwardTo(initialLocation.pathname);
    } else {
        forwardTo("/");
    }
});

// rootSaga is undefined during testing
if (rootSaga) {
    sagaMiddleware.run(rootSaga);
}

export function forwardTo(location) {
    history.push(location);
}
