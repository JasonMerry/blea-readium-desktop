import { app, ipcRenderer } from "electron";
import { machineIdSync } from "electron-machine-id";
import * as fs from "fs";
import * as path from "path";

let deviceId: string = machineIdSync(true);
let appFolder = null;

export { deviceId };

export function getAppFolderMain(): any {
    if (appFolder !== null) {
        return appFolder;
    } else {
        appFolder = app.getPath("appData");
        return appFolder;
    }
}

export function getAppFolder(): any {
    if (appFolder !== null) {
        return appFolder;
    } else {
        if (typeof ipcRenderer !== "undefined") {
            appFolder = ipcRenderer.sendSync("get_app_folder", "");
        }
        return appFolder;
    }
}

export function bookDownloadPath(bookshelfPk: any): string {
    return path.join(getAppFolderMain(), "bwl", String(bookshelfPk));
}

export function bookDownloadMimetype(bookshelfPk: string): any {
    // check if book downloaded
    try {
        let data = fs.readFileSync(path.join(bookDownloadPath(bookshelfPk), "mimetype"), "utf8");
        return data;
    } catch (e) {
        // check for a pdf
        if (fs.existsSync(path.join(bookDownloadPath(bookshelfPk), "content.pdf"))) {
            return "application/pdf+glb";
        } else {
            return "";
        }
    }
}
