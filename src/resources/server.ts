import axios from "axios";

import {ipcRenderer} from "electron";
const pjson = require("../../package.json");

let SERVER_PATH = "";
let axiosInstance = null;

export function getServerMain(): any {
    if (axiosInstance !== null) {
        return {axiosInstance, SERVER_PATH};
    } else {
        let serverUrl: any = pjson.config.serverUrl;
        axiosInstance = axios.create({
            baseURL: serverUrl
        });
        SERVER_PATH = serverUrl;
        return {axiosInstance, SERVER_PATH};
    }
}

export function getServer(): any {
    if (axiosInstance !== null) {
        return {axiosInstance, SERVER_PATH};
    } else {
        if (typeof ipcRenderer !== "undefined") {
            let serverUrl: any = ipcRenderer.sendSync("get_server_url", "");
            axiosInstance = axios.create({
                baseURL: serverUrl
            });
            SERVER_PATH = serverUrl;
        }
        return {axiosInstance, SERVER_PATH};
    }
}
