import test from "ava";

// to remove no tests warning
test("one plus one is two", (t) => {
    t.deepEqual(1 + 1, 2);
});

// testing sagas is problematic with Inversify

// import { call, put, race, select, take } from "redux-saga/effects";

// import * as actions from "blea/actions/auth";

// import {
//     authorize,
//     bookshelfFlow,
//     downloadBook,
//     downloadBookFlow,
//     fetchBookshelf,
//     getUserAuthState,
//     loginFlow,
//     logout,
//     logoutFlow,
//     registerDevice,
// } from "blea/renderer/sagas";

// import {
//     LOGIN_REQUEST,
//     LOGOUT,
//     REQUEST_ERROR,
//     SENDING_REQUEST,
// } from "blea/actions/auth";

// import {
//     DOWNLOAD_BOOK,
//     FETCH_BOOKSHELF,
//     FETCH_BOOKSHELF_SUCCESS,
//     REMOVE_BOOKSHELF,
// } from "blea/actions/bookshelf";

// import { BookshelfApi } from "blea/api/bookshelf";
// import { container } from "blea/renderer/di";
// import TYPES from "blea/types";

// let user = {username: "stephen", password: "password"};
// let data = {data: user};
// let raceObject = {
//     auth: call(authorize, user),
//     logout: take(LOGOUT)
// };
// let postRegAuthState = {key: "some key", loggedIn: true, user: 12345, token: "some token"};
// let bookshelfResp = {id: 1, name: "empty bookshelf"};
// let bookshelfInst = container.get<BookshelfApi>(TYPES.BookshelfApi);

// test("loginFlow saga with success", (t) => {
//     let gen = loginFlow();
//     let loginRace = race(raceObject);
//     let authWinner = {auth: {loggedIn: false, token: "some token"}};

//     t.deepEqual(
//         gen.next().value,
//         take(LOGIN_REQUEST)
//     );

//     t.deepEqual(
//         gen.next(data).value,
//         loginRace
//     );

//     t.deepEqual(
//         gen.next(authWinner).value,
//         put(actions.setAuthState({loggedIn: false, token: "some token"}))
//     );

//     t.deepEqual(
//         gen.next().value,
//         call(registerDevice, "some token")
//     );

//     t.deepEqual(
//         gen.next(postRegAuthState).value,
//         put(actions.setAuthState(postRegAuthState))
//     );

//     t.deepEqual(
//         gen.next().value,
//         put(actions.changeForm({username: "", password: ""}))
//     );

// });

// test("logoutFlow saga", (t) => {
//     let gen = logoutFlow();

//     t.deepEqual(
//         gen.next().value,
//         take(LOGOUT)
//     );

//     t.deepEqual(
//         gen.next().value,
//         call(logout)
//     );

//     t.deepEqual(
//         gen.next().value,
//         put(actions.setAuthState({loggedIn: false}))
//     );

//     t.deepEqual(
//         gen.next().value,
//         put({type: REMOVE_BOOKSHELF})
//     );

// });

// test("bookshelfFlow saga", (t) => {
//     let gen = bookshelfFlow();
//     t.deepEqual(
//         gen.next().value,
//         take(FETCH_BOOKSHELF)
//     );

//     t.deepEqual(
//         gen.next().value,
//         select(getUserAuthState)
//     );

//     t.deepEqual(
//         gen.next(postRegAuthState).value,
//         call(fetchBookshelf, "some token")
//     );

//     t.deepEqual(
//         gen.next(bookshelfResp).value,
//         put({type: FETCH_BOOKSHELF_SUCCESS, bookshelf: bookshelfResp})
//     );
// });

// test("fetchBookshelf saga", (t) => {
//     let gen = fetchBookshelf("some token");

//     t.deepEqual(
//         gen.next().value,
//         put({type: SENDING_REQUEST, sending: true})
//     );

//     t.deepEqual(
//         gen.next().value,
//         call(bookshelfInst.fetchBookshelf, "some token")
//     );

// });

// test("fetchBookshelf saga error test", (t) => {
//     let gen = fetchBookshelf("some token");

//     t.deepEqual(
//         gen.next().value,
//         put({type: SENDING_REQUEST, sending: true})
//     );

//     t.deepEqual(
//         gen.next().value,
//         call(bookshelfInst.fetchBookshelf, "some token")
//     );
//     const error = new Error("No Bookshelf");

//     let result = gen.throw(error);

//     t.deepEqual(
//         result.value,
//         put({type: REQUEST_ERROR, error: "No Bookshelf"})
//     );

//     t.deepEqual(
//         gen.next().value,
//         put({type: SENDING_REQUEST, sending: false})
//     );

// });

// test("downloadBookFlow saga test", (t) => {

//     let gen = downloadBookFlow();
//     t.deepEqual(
//         gen.next().value,
//         take(DOWNLOAD_BOOK)
//     );

//     // pass in expected params to next call
//     t.deepEqual(
//         gen.next({download: {bookshelfPk: 1234, isbn13: "1234567890123"}}).value,
//         select(getUserAuthState)
//     );

//     t.deepEqual(
//         gen.next(postRegAuthState).value,
//         call(downloadBook, "some token", {bookshelfPk: 1234, isbn13: "1234567890123"})
//     );

// });

// // test("downloadBook saga test error", (t) => {
// //     let gen = downloadBook("some token", 1234);
// //     t.deepEqual(
// //         gen.next().value,
// //         put({type: SENDING_REQUEST, sending: true})
// //     );

// //     t.deepEqual(
// //         gen.next().value,
// //         call(bookshelfInst.downloadBook, "some token", 1234)
// //     );

// //     const error = new Error("No Book");

// //     let result = gen.throw(error);

// //     t.deepEqual(
// //         result.value,
// //         put({type: REQUEST_ERROR, error: "No Book"})
// //     );

// //     t.deepEqual(
// //         gen.next().value,
// //         put({type: SENDING_REQUEST, sending: false})
// //     );

// // });

// // test("downloadBook saga test success", (t) => {

// //     let gen = downloadBook("some token", 1234);
// //     t.deepEqual(
// //         gen.next().value,
// //         put({type: SENDING_REQUEST, sending: true})
// //     );

// //     t.deepEqual(
// //         gen.next().value,
// //         call(bookshelfInst.downloadBook, "some token", 1234)
// //     );

// //     t.deepEqual(
// //         gen.next().value,
// //         put({type: SENDING_REQUEST, sending: false})
// //     );

// // });
