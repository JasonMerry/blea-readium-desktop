/* tslint:disable */
// compiles to a service worker so needs to be valid javascript
const pjson = require("../../package.json");
self.addEventListener("fetch", function(event) {
    var request = event.request;
    // check if request
    if (request.url.indexOf(pjson.config.serverUrl) > -1) {
        // contentful asset detected
        event.respondWith(
            caches.match(event.request).then(function(response) {
                // return from cache, otherwise fetch from network
                return response || fetch(request);
            })
        );
    }
    // Otherwise: ignore event
});
/* tslint:enable */
