
export const isDefined = (variable) => typeof variable !== "undefined";

export const makeCancellable = (promise) => {
    let isCancelled = false;

    const wrappedPromise = new Promise((resolve, reject) => {
        promise.then(
            (args) => (isCancelled ? reject("cancelled") : resolve(args)),
            (error) => (isCancelled ? reject("cancelled") : reject(error)),
        );
    });

    return {
        promise: wrappedPromise,
        cancel() {
            isCancelled = true;
        },
    };
};

export class Ref {
    private num;
    private gen;

    constructor({ num, gen }) {
        this.num = num;
        this.gen = gen;
    }

    public toString() {
        let str = `${this.num}R`;
        if (this.gen !== 0) {
            str += this.gen;
        }
        return str;
    }
}
