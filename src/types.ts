export let TYPES = {
    IAuthApi: Symbol("IAuthApi"),
    BookshelfApi: Symbol("BookshelfApi"),
    IDecompress: Symbol("IDecompress"),
    IDecrypt: Symbol("IDecrypt"),
    IDecryptPdf: Symbol("IDecryptPdf"),
    Spine: Symbol("Spine"),
};

export default TYPES;
