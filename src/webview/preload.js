const {ipcRenderer} = require('electron');


ipcRenderer.on("load-book", function(event, data){
    console.log("load-book", event);
    document.getElementById('readiumbody').setAttribute('data-epub', data.path);
});

ipcRenderer.on("show-highlights", function(event, data){
});

global.pingHost = (blah) => {
    ipcRenderer.sendToHost(blah);
}

global.initPage = () => {
    ipcRenderer.sendToHost({type: "pageloaded"});
}


global.setupEventHandlers = () => {

    function isPositiveInteger(str) {
        return /^\d+$/.test(str);
    }

    // look for next element if no brickId present
    function ensureSiblingId(curNode) {
        var curNodeId = undefined;
        if (curNode && curNode.parentNode) {
            var curNodeId = curNode.parentNode.id;
            if (!isPositiveInteger(curNodeId)) {
                return curNode.previousSibling.id;
            }
        }
        return curNodeId;
    }

    function getSelection() {
        var selection = $("#epub-reader-frame iframe")[0].contentDocument.getSelection();

        // type should be "Range"
        if (selection.type === "Caret") {
            return {endId: undefined, startId: undefined};
        }

        var startNode = selection.anchorNode;
        var startId = ensureSiblingId(startNode);
        var endNode = selection.extentNode;
        var endId = ensureSiblingId(endNode);

        if (!(startId && endId)) {
            return {endId: undefined, startId: undefined};
        }
        if (startId > endId) {
            return {endId: startId, startId: endId};
        }
        return {startId, endId};
    }

    document.addEventListener("pagenav", function (e) {
        console.log("open page", e);
        switch (e.detail.msg) {
        case "back":
            ReadiumSDK.reader.openPagePrev();
            break;
        case "forward":
            ReadiumSDK.reader.openPageNext();
            break;
        case "gotoBookmark":
            ReadiumSDK.reader.openSpineItemElementId(e.detail.spineRef, e.detail.brickId);
            break;
        case "gotoId":
            console.log(e.detail.spineRef);
            ReadiumSDK.reader.openSpineItemElementId(e.detail.spineRef, e.detail.brickId);
            break;
        case "getSelection":
            var selection = getSelection();
            ipcRenderer.sendToHost({type: e.detail.rtn, start: selection.startId, end: selection.endId});
            break;
        case "getBookmark":
            var matchIdInCfiRegEx = /\[(\d+)\]/;
            var firstCfi = ReadiumSDK.reader.getFirstVisibleCfi();
            var match = matchIdInCfiRegEx.exec(firstCfi);
            if (match) {
                var firstId = match[1];
                ipcRenderer.sendToHost({type: "bookmark", id: firstId});
            } else {
                console.log("Cannot bookmark this page");
            }
            break;
        case "showSelection":
            var matchIdInCfiRegEx = /\[(\d+)\]/;

            var highlights = e.detail.highlights;
            var spineRef = ReadiumSDK.reader.getCurrentView().getLoadedSpineItems()[0].idref;
            // all brick elements on spine
            var spineElements = ReadiumSDK.reader.getCurrentView().getElement(
                spineRef,
                ".bwlBrick"
            );
            if (!spineElements) {
                break;
            }
            var firstSpineElementId = parseInt(spineElements[0].id);
            var lastSpineElementId = parseInt(spineElements[spineElements.length - 1].id);
            for (i=0; i < highlights.length; i++) {
                var wrapperId = "bwlHighlight_" + highlights[i].id;
                // check if already applied
                var wid = ReadiumSDK.reader.getCurrentView().getElement(spineRef, "#"+wrapperId);
                if (!wid) {
                    var word_ids = highlights[i].selection.word_ids;
                    var lowestId = parseInt(word_ids[0]);
                    var highestId = parseInt(word_ids[word_ids.length - 1]);
                    if ((lowestId > firstSpineElementId && lowestId < lastSpineElementId) || (highestId > firstSpineElementId && highestId < lastSpineElementId)) {
                        var wrapper = document.createElement("span");
                        wrapper.style.backgroundColor = "yellow";
                        wrapper.id = wrapperId;
                        var parent, sibling;
                        for (j=0; j < word_ids.length; j++) {
                            var spineElementToHighlight = spineElements[word_ids[j] - firstSpineElementId];
                            if (j === 0) {
                                parent  = spineElementToHighlight.parentNode;
                                sibling = spineElementToHighlight.nextSibling;
                            }
                            wrapper.append(spineElementToHighlight);
                            if (j !== (word_ids.length - 1)) {
                                wrapper.append(document.createTextNode(' '));
                            }
                        }
                        if (sibling) {
                            parent.insertBefore(wrapper, sibling);
                        } else {
                            parent.appendChild(wrapper);
                        }
                    }
                }
            }
            break;
        case "showSettings":
            $('#settings-dialog').modal("show");
            break;
        case "showTableOfContents":
            $appContainer = $('#app-container'),
            $appContainer.addClass('toc-visible');
            break;
        }
    }, false);
    // also need to listen for readium events
}

ipcRenderer.on("page-nav", function(event, data){
    console.log("on page-nav", data);
    var event = new CustomEvent("pagenav", {"detail": data.detail});
    document.dispatchEvent(event);
});
