// adapted from readium-js-viewer/src/js/ReadiumViewer.js
// global scope required otherwise doc is opened twice

var path = (window.location && window.location.pathname) ? window.location.pathname : '';

// extracts path to index.html (or more generally: /PATH/TO/*.[x]html)
path = path.replace(/(.*)\/.*\.[x]?html$/, "$1");

// removes trailing slash
path = path.charAt(path.length-1) == '/'
    ? path.substr(0, path.length-1)
    : path;

var HTTPServerRootFolder =
window.location ? (
    window.location.protocol
    + "//"
    + window.location.hostname
    + (window.location.port ? (':' + window.location.port) : '')
    + path
    ) : ''
;

var getURLQueryParams = function() {
    var params = {};

    var query = window.location.search;
    if (query && query.length) {
    query = query.substring(1);
    var keyParams = query.split('&');
    for (var x = 0; x < keyParams.length; x++)
        {
            var keyVal = keyParams[x].split('=');
            if (keyVal.length > 1) {
                params[keyVal[0]] = decodeURIComponent(keyVal[1]);
            }
        }
    }

    return params;
};

var urlParams = getURLQueryParams();

var fontsArray = [];
if (typeof getFontFaces != "undefined") { // defined externally
    fontsArray = getFontFaces(HTTPServerRootFolder + "/font-faces/");
}

// MUST BE *SINGLE* CALL TO require.config() FOR ALMOND (SINGLE BUNDLE) TO WORK CORRECTLY!!!
require.config({
    /* http://requirejs.org/docs/api.html#config-waitSeconds */
    waitSeconds: 0,
    config : {
        'readium_js_viewer/ModuleConfig' : {
            'mathJaxUrl': HTTPServerRootFolder + '/scripts/mathjax/MathJax.js',
            'fonts': fontsArray,
            'annotationCSSUrl': HTTPServerRootFolder + '/css/annotations.css',
            'jsLibRoot': HTTPServerRootFolder + '/scripts/zip/',
            'useSimpleLoader' : false, // cloud reader (strictly-speaking, this config option is false by default, but we prefer to have it explicitly set here).
            'epubLibraryPath': urlParams['epubs'] ? urlParams['epubs'] : "epub_content/epub_library.opds", // defaults to /epub_content/epub_library.json relative to the application's root index.html
            'imagePathPrefix': undefined,
            'canHandleUrl' : false,
            'canHandleDirectory' : false,
            'workerUrl': undefined,
            'epubReadingSystemUrl': undefined
        }
    }
});

require(["readium_shared_js/globalsSetup", "readium_shared_js/globals"], function (GlobalsSetup, Globals) {
    require(['readium_shared_js/views/reader_view', 'jquery', 'readium_shared_js/helpers', 'URIjs', 'readium_js_viewer/EpubReader', 'readium_js_viewer/ModuleConfig'], function (ReaderView, $, Helpers, URI, EpubReader, moduleConfig) {
        var readepub = function(eventPayload){
            if (!eventPayload || !eventPayload.epub) return;

            var ebookURL_filepath = Helpers.getEbookUrlFilePath(eventPayload.epub);

            var epub = eventPayload.epub;

            if (epub && (typeof epub !== "string")) {
                epub = ebookURL_filepath;
            };

            ebookURL_filepath = EpubReader.ensureUrlIsRelativeToApp(ebookURL_filepath);

            var epubs = eventPayload.epubs;

            epubs = EpubReader.ensureUrlIsRelativeToApp(epubs);

            var urlState = Helpers.buildUrlQueryParameters(undefined, {
                epub: ebookURL_filepath,
                epubs: (epubs ? epubs : undefined),
                embedded: (eventPayload.embedded ? eventPayload.embedded : undefined)
            });

            EpubReader.loadUI(eventPayload);
        };

        $(document).ready(function () {
            ReadiumSDK.reader = new ReaderView(
            {
                needsFixedLayoutScalerWorkAround: false,
                el:"#viewport",
                annotationCSSUrl: moduleConfig.annotationCSSUrl,
                mathJaxUrl : moduleConfig.mathJaxUrl,
                fonts: moduleConfig.fonts
            });
            console.log("the reader", ReadiumSDK.reader);
            readepub({epub: $('body').attr('data-epub')});
            setupEventHandlers(); // in preload.js
            ReadiumSDK.on(
                ReadiumSDK.Events.READER_INITIALIZED,
                function()  {
                    ReadiumSDK.reader.on(
                        ReadiumSDK.Events.CONTENT_DOCUMENT_LOADED,
                        function() {
                            initPage();
                        }
                    );
                }
            );
        });
    });
});
