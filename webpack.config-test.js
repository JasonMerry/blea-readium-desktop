var path = require('path');
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");

let rendererBaseUrl = "file://";

let definePlugin = new webpack.DefinePlugin({
    __RENDERER_BASE_URL__: JSON.stringify(rendererBaseUrl)
});

module.exports = {
    output: {
        filename: "test.js",
        path: path.join(__dirname, "dist_test"),
    },
    resolve: {
        alias: {
            "blea": path.resolve(__dirname, "src")
        },
        extensions: [".ts", ".tsx", ".js", ".jsx"]
    },
    devtool: 'eval',
    module: {
        loaders: [
            {
                test: /\.tsx?$/,
                loader: 'awesome-typescript-loader'
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/index.ejs",
        }),
        new webpack.ProvidePlugin({
            // Several different ways to enable jquery to work.
            // Have left all options here in case we need them later and to save searching again.
            // Uncomment as/if needed in the future.
            //
            // jQuery: 'jquery',
            // $: 'jquery',
            // jquery: 'jquery',
            "window.jQuery": "jquery", // Currently needed for Materialize-css to work
            // "root.jQuery": "jquery",
            // "window.$": "jquery"
        }),
        definePlugin,
    ],
};
