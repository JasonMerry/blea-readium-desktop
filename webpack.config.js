const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const nodeExternals = require('webpack-node-externals');

// Default values for DEV environment
let buildEnv = process.env.BUILD_ENV || "DEV";
let rendererBaseUrl = "file://";

let definePlugin = new webpack.DefinePlugin({
    __RENDERER_BASE_URL__: JSON.stringify(rendererBaseUrl)
});

let mainConfig = Object.assign({}, {
    entry: "./src/main.ts",
    name: "main",
    output: {
        filename: "main.js",
        path: path.join(__dirname, "dist"),
    },
    target: "electron-main",

    node: {
        __dirname: false,
        __filename: false,
    },

    resolve: {
        // Add '.ts' as resolvable extensions.
        extensions: [".ts", ".js", ".json"],
        alias: {
            "blea": path.resolve(__dirname, "src"),
        },
    },

    module: {
        loaders: [
            // All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
            { test: /\.tsx?$/, loaders: ["awesome-typescript-loader"] },
        ],
    },
    plugins: [
        definePlugin,
    ],
});

let rendererConfig = Object.assign({}, {
    entry: "./src/renderer.ts",
    name: "renderer",
    output: {
        filename: "renderer.js",
        path: path.join(__dirname, "dist"),
    },
    target: "electron-renderer",

    resolve: {
        extensions: [".ts", ".tsx", ".js", ".jsx", ".json"],
        alias: {
            "blea": path.resolve(__dirname, "src"),
        },
    },

    module: {
        loaders: [
            {
                test: /\.tsx?$/,
                loaders: ["react-hot-loader", "awesome-typescript-loader"],
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: "css-loader",
                }),
            },
            {
                test: /\.(png|jpe?g|gif|ico)$/,
                loader: "file-loader?name=img/[name].[hash].[ext]",
            },
            {
                test: /\.(woff|woff2|ttf|eot|svg)$/,
                loader: "file-loader?name=fonts/[name].[hash].[ext]",
            },
            {
                test: /\.(png|jpe?g|gif|ico|svg)$/,
                include: "/src/assets/",
                loader: "file-loader?name=img/[name].[hash].[ext]",
            },
            {
                test: /\.json$/,
                use: 'json-loader'
            },
        ],
    },
    devServer: {
        contentBase: __dirname,
        hot: true,
        watchContentBase: true
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/index.ejs",
        }),
        new ExtractTextPlugin("styles.css"),
        new CopyWebpackPlugin([
            {from: "./src/assets/img/logo.svg", to: "img/"},
            {
                from: "./readium-js-viewer/dist/cloud-reader",
                // from: "./readium-js-viewer/dist/cloud-reader-lite",
                to: "readium/",
                force: true,
                ignore: ["index.html"]
            },
            {from: "./src/webview/preload.js", to: "readium/"},
            {from: "./src/webview/reader.js", to: "readium/"},
            {from: "./src/webview/index.html", to: "readium/"},
            {from: "./src/webview/main.js", to: "readium/scripts"},
            {from: "./node_modules/pdfjs-dist/build/pdf.worker.js"},
            // {from: "./node_modules/materialize-css/dist/css/materialize.css", to : "readium/css"},
            // {from: "./node_modules/materialize-css/dist/js/materialize.min.js", to : "readium/scripts"},
            {from: "./src/assets/css/bleadium.css", to : "readium/css"},
            {from: "./node_modules/requirejs/require.js", to : "readium/scripts"},
            {from: "./node_modules/popper.js/dist/popper.js", to : "readium/scripts"},
            {from: "./node_modules/bootstrap-material-design/dist/css/bootstrap-material-design.css", to : "readium/css"},
            {from: "./node_modules/bootstrap-material-design/dist/js/bootstrap-material-design.js", to : "readium/scripts"},
            {from: "./node_modules/jquery/dist/jquery.js", to : "readium/scripts"},
            {from: "./node_modules/material-design-icons-iconfont/dist/material-design-icons.css", to : "readium/css"},
            {from: "./node_modules/material-design-icons-iconfont/dist/fonts", to : "readium/css/fonts"}
        ]),
        new webpack.ProvidePlugin({
            // Several different ways to enable jquery to work.
            // Have left all options here in case we need them later and to save searching again.
            // Uncomment as/if needed in the future.
            //
            // jQuery: 'jquery',
            // $: 'jquery',
            // jquery: 'jquery',
            "window.jQuery": "jquery", // Currently needed for Materialize-css to work
            // "root.jQuery": "jquery",
            // "window.$": "jquery"
        }),
        definePlugin,
    ],
});

let serviceWorkerConfig = Object.assign({}, {
    entry: "./src/service_workers/cache-images.ts",
    name: "service_worker",
    output: {
        filename: "sw.js",
        path: path.join(__dirname, "dist"),
    },
    target: "electron-renderer",

    resolve: {
        extensions: [".ts", ".js"],
        alias: {
            "blea": path.resolve(__dirname, "src"),
        },
    },

});

if (buildEnv == "DEV") {

    // Main config for DEV environment
    mainConfig.output.publicPath = "http://localhost:8080/dist/";

    // Renderer config for DEV environment
    rendererConfig = Object.assign({}, rendererConfig, {
        // Enable sourcemaps for debugging webpack's output.
        devtool: "source-map",

        devServer: {
            contentBase: __dirname,
            hot: true,
            watchContentBase: true
        }
    });

    rendererConfig.output.publicPath = "http://localhost:8080/dist/";
    rendererConfig.plugins.push(new webpack.HotModuleReplacementPlugin());
}

module.exports =  [
    mainConfig, rendererConfig, serviceWorkerConfig
];
